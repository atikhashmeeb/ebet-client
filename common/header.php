<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<!-- <meta name="viewport" content="user-scalable=no, minimum-scale=0.1, maximum-scale=1.0, initial-scale=1">
	<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black"> -->
	<meta name="format-detection" content="telephone=no" />
	<title><?php echo $config['site name']?></title>
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" media="all" href="<?php echo $baseurl?>/css/style.css" />
	<script type="text/javascript" src="<?php echo $baseurl?>/js/timer.js"></script>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
	<script src="<?php echo $baseurl?>/js/respond.min.js"></script>
</head>
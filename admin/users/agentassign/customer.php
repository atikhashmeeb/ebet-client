<?php
require_once("../../../include/config.php");
require_once($basedir . "/admin/include/functions.php");
include $basedir . '/admin/include/isadmin.php';
$agents = getAssignUser();
$usermenu='active';
$datatables='active';

?>
<!DOCTYPE html>
<html>
<head>
<?php include $basedir . '/admin/include/header.php'; ?>
</head>
<body class="skin-blue">
<header class="header">
  <?php include $basedir . '/admin/include/header_menu.php'; ?>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <aside class="left-side sidebar-offcanvas">
    <section class="sidebar">
      <?php include $basedir . '/admin/include/sidebar.php'; ?>
    </section>
  </aside>
  <aside class="right-side">
    <section class="content-header">
      <h1><?php echo $lang[17] ?><small><?php echo $lang[31] ?></small></h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo $baseurl ?>/"><i class="fa fa-dashboard"></i><?php echo $lang[27] ?></a></li>
        <li><a href="<?php echo $baseurl ?>/users"><?php echo $lang[14] ?></a></li>
        <li class="active"><?php echo $lang[17] ?></li>
      </ol>
    </section>
    <section class="content">
    
      <div class="tabs-sections" style="height:-webkit-fill-available">
     
        <div class="tabs-sections__section  tabs-sections__section--active  tab-js  tab-active-js">
       
          <div class="box-body box  no-padding">
           <br> <br>
           
            <table id="datatable" class="table table-hover">
              <thead>
                <tr>
                  <th>UserId</th>
                  <th>Name</th>
         
                 <th>Status</th>
                 <th>Credit</th>
                 <th>Int Limit</th>
                 <th>Pending</th>
                 <th>Balance</th>
                 <th>Last Wager</th>
                </tr>
              </thead>
              <tbody>
                <?php 
			$iThisParticularUserDetail=mysqli_query($con, "SELECT * FROM   assign_customer WHERE agent_id = '".$_SESSION['user_id']."' order by id desc");
					while($iTHisUser = mysqli_fetch_assoc($iThisParticularUserDetail)) {
 					 
			 $qrny ="SELECT * FROM  users WHERE user_id = '".$iTHisUser['customer_id']."'";
			 $ThisGetUsid=mysqli_query($con, $qrny);
			 $iTssUser = mysqli_fetch_assoc($ThisGetUsid);
			 
			 $iBettingDetail=mysqli_query($con, "SELECT * FROM  bettingdetail WHERE userid = '".$iTHisUser['customer_id']."' order by ID desc");
			 $iBetDet = mysqli_fetch_assoc($iBettingDetail);
	                ?>
                <tr>
                   <td><a href="index.php?action=edit&user_id=<?php echo $iTHisUser['customer_id']	?>">USER <?php echo $iTHisUser['customer_id']	?></a></td>
                   <td> <?php echo $iTssUser['user_name']	?></td>
                   <td> <?php if($iTssUser['user_status']=='1') { echo 'Active'; } else {  echo 'Pending'; }	?></td>
                    <td><?php echo $iBetDet['user_current_credit_limit']; ?></td>
                    <td><?php echo $iBetDet['user_current_max_limit']; ?></td>
                    <td><?php echo $iBetDet['user_current_pending']; ?></td>
                    <td><?php echo $iBetDet['user_current_balance']; ?></td>
                    <td><?php echo $iBetDet['create_at']; ?></td>
                </tr>
                <?php }  ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </section>
  </aside>
</div>
<?php include $basedir . '/include/javascript.php'; ?>
</body>
</html>

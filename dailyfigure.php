<?php 
require_once('include/config.php');
require_once($basedir . "/include/functions.php");
require_once($basedir . '/include/user_functions.php');
if ($_SESSION['user_id']=='') { 
	header('Location: ' . $baseurl . '/log.php');
	exit;
}

$settingsmenu = 'active';
$accountmenu='active';

$def_tz = ($_SESSION['user_timezone']) ? $_SESSION['user_timezone'] : 'Asia/Tokyo';
$languages = getLanguages();

$file = $basedir . '/temp/all_users.txt';
$data = json_decode(file_get_contents($file), true);
?>
<!DOCTYPE HTML>
<html>
<?php include $basedir . '/common/header.php'; ?>
<style>
#myaccount .box, #dashboard .box {
    margin: 0 !important;
}
.HeaderColumnsName td {
	border-right: 1px solid;
	text-align: center;
	padding: 10px;
}
.tdSpread, .tdMoneyLine, .tdTotal {
	border-right: 1px solid;
}
.tdHeaderTeamTotal {
	width: 28%;
}
.TableTeamTotal {
	width: 100%;
}
.TableTeamTotal td input {
	float: left;
}
.Submitbutton {
	background-color: #007bff!important;
	    padding: 8px 15px;
    color: white;
    border: 0;
    font-size: 17px;
}
input[type="text"] {
	padding: 10px 6px !important;
	width: 40px !important;
	box-shadow: inset 0 0 3px 0 black;
}
#tblcc tr > td {
	padding: 4px;
}
.td3 select {
	padding: 2px;
}
td.tdTeamName {
	font-weight: 600;
	border-right: 1px solid;
}
.submitwager {
	clear: right;
	float: right;
	padding-top: 5px;
	padding-bottom: 4px;
}
.HeaderTableLine {
	background: #0e2431;
	border-bottom: 1px solid #ffffff;
	line-height: 25px;
	padding: 0 17px;
	font-size: 14px;
	padding-right: 10px;
}
.HeaderColumnsName {
	background: #525252;
	font: bold 12px Arial;
	height: 24px;
	padding: 0 0 0 10px;
	text-align: left;
	color: #FFFFFF;
}
.BackgroundLineGray {
	background: #ffffff;
	height: 25px;
	color: #000000;
}
#tblcc tr > td:first-child {
	border-bottom: 0px;
}
.tdDate, .tdTelevised, .tdTeamName, .tdSpread, .tdMoneyLine, .tdTotal, .tdTeamTotal {
	text-align: left;
	font: normal 12px "Segoe UI", Arial, sans-serif;
}
.BackgroundLineGray {
	background: #ffffff;
	height: 25px;
	color: #000000;
}
.BackgroundLineAlternateGray {
	border: 1px solid;
	background: #66666630;
}
.lastRow {
	border-bottom: 1px solid gray;
}
.TableWager {
	border-collapse: collapse;
	width: 100%;
	font-size: 8px;
	font-family: Arial;
	height: 100%;
	border: 1px solid #000;
}
.tdContestTeamName, .tdTeamName {
	width: 16%;
	padding-left: 5px;
	vertical-align: inherit;
	white-space: nowrap;
}
.cellSportHeader {
	clear: left;
	float: left;
	padding-top: 8px;
	text-align: left;
	color: #ffffff;
}
.TableSpread {
	width: 100%;
	font-family: Arial;
	font-size: 12px;
}
#tblcc tr > td:first-child {
	border-bottom: 0px;
}
.TableSpread td.td1, .TableMoneyLine td.td1, .TableTotals td.td1 {
	width: 1%;
	text-align: right;
}
.TableSpread td.td2, .TableMoneyLine td.td2, .TableTotals td.td2 {
	text-align: right;
	width: 5%;
}
.TableSpread td.td3, .TableMoneyLine td.td3, .TableTotals td.td3 {
	text-align: left;
}
.textBox {
	cursor: text;
	font-family: Arial;
	font-size: 11px;
	width: 38px;
	height: 15px;
	color: Blue;
	background: #FFF;
	border: 1px solid #999;
	padding: 2px;
	outline: medium none;
}
input[type="text"], input[type="password"], input[type="date"], input[type="datetime"], input[type="datetime-local"], input[type="month"], input[type="week"], input[type="email"], input[type="number"], input[type="search"], input[type="tel"], input[type="time"], input[type="url"], textarea {
	padding: 15px 10px;
	color: black;
	font-size: 15px;
	width: 100px;
}
.tdTeamName p {
	font-weight: 400;
}
.mainapidata table thead tr th {
	text-align: center;
	background:#525252;
	color: #fff;
	padding: 15px 0px;
	font-size: 13px;
	    padding-left: 10px;
}

footer {
    padding-top: 20px;
}
.mainapidata table thead tr td {
	text-align: center;
}
.mainapidata table tr td {
	width: 250px;
    font-size: 13px;
    text-align: center;
	    padding: 10px;
}
table tbody tr td table.innertable tr {
	height: 30px;
}


.oddtble{background-color: #fff;}
.evetble{background-color: #f2f2f2;}

table tbody tr td table.innertable tr td input[type="text"] {
	float: left;
	box-shadow: inset 0 0 3px 0 black;
	margin-right: 10px;
	width: 50px;
	height: 0px;
}

.abrveiation {
	width: 50px!important;
}
</style>



<script type="text/javascript">
function submitthisform()
	{
		document.getElementById("myFormTes").submit();

	}
</script>
<body>
<?php include $basedir . '/common/head.php'; ?>
<div class="container row">
  <?php include $basedir . '/common/myheadmenu.php';?>
  <main role="main" class="row gutters mypage">
    <article id="myaccount" class="col span_12">
      <div class="box" style="margin: 0px 10px 0px 10px;">
        <div class="title_box"> 
          <!--<h4 class="title"><?php echo $lang[406];?></h4>
						<p class="desc"><?php echo $lang[486];?></p>--> 
        </div>
    
        <main class="bg-light clearfix">
          <div class="col-md-6 d-inline-block">
            <div class="table-responsive mainapidata">

 <form action="" method="post"  id="myFormTes">
 
 
 
 
 
 
 	<?php if($_GET['action']=='viewdetails') { ?>
  <table style="width:100%">
  <tr><td style="text-align: left;"><a class="Submitbutton mt-3 mr-4 " style="text-decoration:none;" href="dailyfigure.php" >Back</a>
  <b>Date : </b><?php echo $_GET['date']; ?></td></tr>
  </table>
   
    <table style="width:100%">
    
    
                <thead>
                  <tr class="HeaderColumnsName">
                    <th class="thHeaderPeriod" style="width:20px;">Date Time Accepted    </th>
                    
                    <th class="thHeaderPeriod" style="width:20px;">Ticket #   </th>
                    <th class="thHeaderPeriod" style="width:20px;">Status  </th>
                    <th class="tdHeaderSpread" style="    text-align: left;">Game</th>
                    <th class="thHeaderPeriod" style="width:20px;">Wager Type  </th>
                    <th class="tdHeaderMoneyLine">Risk</th>
                    <th class="tdHeaderTotal">Win</th>
                  </tr>
                </thead>
                <tbody>
                  <?php		$i=0;
				
				   $q = "SELECT * FROM bettingdetail WHERE userid = '".$_SESSION['user_id']."' and result!='0' and DATE(create_at) = '".$_GET['date']."'";
					$result = mysqli_query($con,$q);
					while ($iBetDet = mysqli_fetch_array($result)) {
						
						$i = $i+1	;
						 
							?>
                  <tr class="<?php if($i%2=='0') { echo 'oddtble'; } else { echo 'evetble'; } ?>">
                       
                        <td style="width:20px;"><?php echo date("m/d/Y", strtotime($iBetDet['create_at'])); ?> <?php echo date("h:i a", strtotime($iBetDet['create_at'])); ?> </td>
                          <td style="width:20px;"><?php echo $iBetDet['ticketid']; ?></td>
                         <td style="width:20px;"> (<?php if($iBetDet['result']=='1') { echo  'Win';  } if($iBetDet['result']=='2') { echo  'Loss';  } ?>)</td>
                         
                         
                        <td style="width:200px;    text-align: left;">
                        Baseball - MLB <Br>
                        <b><strong style="font-weight: bold;">[<?php echo $iBetDet['abrevation_name']; ?>] <?php echo $iBetDet['sport_name']; ?> </strong> <?php echo date("m/d/Y", strtotime($iBetDet['event_date'])); ?>
                         (<?php echo date("h:i a", strtotime($iBetDet['event_date'])); ?>) </b> <strong style="color:#e26c32">(<?php if($iBetDet['result']=='1') { echo  'Win';  } if($iBetDet['result']=='2') { echo  'Loss';  } ?>) </strong><Br>
                        <b><strong style="font-weight: bold;"><?php echo $iBetDet['bettingcondition_orginal']; ?> </strong> for the Game </b><?php echo $iBetDet['sport_name']; ?> </td>
                        
                        	 <td style="width: 30px;">		<?php if($iBetDet['wager']=='1') { echo  'Risk';  } if($iBetDet['wager']=='2') { echo  'Win';  }if($iBetDet['wager']=='3') { echo  'Base Amount';  } ?></td>
                        
                        <td style="width: 30px;">  <?php  echo  $iBetDet['riskamount'];    ?> </td>
                        <td style="width: 30px;">  <?php  echo  $iBetDet['bettingwin']; ?> </td>
                  </tr>
                  <?php  } ?>
                </tbody>
              </table>  
 
 
 	<?php } else { ?>
 
              <table style="width:100%">
                <thead>
                  <tr class="HeaderColumnsName">
                    <th class="thHeaderPeriod" style="width:20px;">Week Starting</th>
                     <?php 	for($ijk=0; $ijk<1; $ijk++) {	
								$previous_week = strtotime("-$ijk week +$ijk day");
								$start_week = strtotime("last sunday midnight",$previous_week);
								$end_week = strtotime("next saturday",$start_week);
								 $start_week  = date("Y-m-d",$start_week);
								 $end_week  = date("Y-m-d",$end_week);
								?>
								
				<?php	for ($iOms =	strtotime($start_week); $iOms<=	strtotime($end_week); $iOms+=86400) {    ?>
                    <th class="thHeaderPeriod" style="width:100px;"><?php  echo date("D", $iOms);  /// date("Y-m-d", $iOms); // This is Date  ?>   </th>
                 <?php } } ?>
                    <th class="tdHeaderSpread">Weekly</th>
                    <th class="tdHeaderMoneyLine">Payments</th>
                    <th class="tdHeaderTotal">Pending</th>
                    <th class="tdHeaderTotal">Balance</th>
                  </tr>
                </thead>
                <tbody>
                <?php 	
					
						for($i=0; $i<6; $i++) {
				
							$previous_week = strtotime("-$i week +$i day");
							
							$start_week = strtotime("last sunday midnight",$previous_week);
							$end_week = strtotime("next saturday",$start_week);
							
							$start_week = date("Y-m-d",$start_week);
							$end_week = date("Y-m-d",$end_week);
							
				
				
?>
                 
                  <tr class="<?php if($i%2=='0') { echo 'oddtble'; } else { echo 'evetble'; } ?>">
                       
                        <td style="width:20px;"><?php if($i=='0') { echo 'Current Week'; } else { echo date("m/d/Y", strtotime($start_week)); 	}	?> </td>
                       		<?php	for ($iOms=	strtotime($start_week); $iOms<=	strtotime($end_week); $iOms+=86400) {    ?>
                                    		<td style="width:100px;"><a style="text-decoration:none;" href="dailyfigure.php?action=viewdetails&date=<?php echo date("Y-m-d", $iOms);  ?>"><?php
					$q1 = "SELECT SUM(bettingwin) as TotalBettingWin FROM bettingdetail WHERE userid = '".$_SESSION['user_id']."' and result='1' and DATE(create_at) = '".date("Y-m-d", $iOms)."'";
					$result1 = mysqli_query($con,$q1);
					$iBetDet1 = mysqli_fetch_array($result1);	
					
					
					$q2 = "SELECT SUM(riskamount) as TotalRiskAmount FROM bettingdetail WHERE userid = '".$_SESSION['user_id']."'  and status = '2' and result='2' and DATE(create_at) = '".date("Y-m-d", $iOms)."'";
					$result2 = mysqli_query($con,$q2);
					$iBetDet2 = mysqli_fetch_array($result2);	
					
					
					$iFinalAmount= $iBetDet1['TotalBettingWin']+$iBetDet2['TotalRiskAmount'];
					
					if($iFinalAmount<0) { echo '<span style="color:red"> -'. $iFinalAmount .'</span>';  } else { echo $iFinalAmount;  }
					
					$iTotalFinalAamou += $iFinalAmount;
											
						 // echo date("Y-m-d", $iOms);  ?> </a>   </td>
                    		<?php } ?>
                            
                         <td style="width:20px;"> 	<?php echo $iTotalFinalAamou; ?></td>
                        <td style="width:200px;"> 	0.00 </td>
                        <td style="width: 30px;">  
                        <?php
						$q3 = "SELECT SUM(riskamount) as iWeekPendingAmount FROM bettingdetail WHERE userid = '".$_SESSION['user_id']."'  and status = '2' and result='0' and DATE(create_at) BETWEEN  '".$start_week."' and  '".$end_week."'";
					$result3 = mysqli_query($con,$q3);
					$iBetDet3 = mysqli_fetch_array($result3);	
				if($iBetDet3 ['iWeekPendingAmount']=='') { echo '0'; } else {	echo $iBetDet3 ['iWeekPendingAmount'];	}
					
					?>
                        
                        
                        </td>
                        <td style="width: 30px;">  	0.00 </td>
                  </tr>
                  <?php  } ?>
                </tbody>
              </table>
              
     <?php } ?>         
              
              
  </form>  
  
  
            </div>
          </div>
        </main>
      </div>
    </article>
  </main>
</div>
<script src="<?php echo $baseurl?>/js/jquery-1.11.0.min.js"></script> 
<?php include $basedir . '/common/foot.php'; ?>
<script src="<?php echo $baseurl?>/js/jquery.remodal.js"></script> 
<script type="text/javascript" src="<?php echo $baseurl?>/js/main_frontend.js"></script> 
<script src="<?php echo $baseurl?>/js/jquery.minimalect.min.js"></script> 
</body>
</html>
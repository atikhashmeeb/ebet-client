<?php 
require_once('../include/config.php');
require_once($basedir . "/include/functions.php");
require_once($basedir . '/include/user_functions.php');
if (!$user_id) {
	// go to login page
	header('Location: '. $baseurl . '#login');
	exit;
}

$settingsmenu = 'active';
$accountmenu='active';

$def_tz = ($_SESSION['user_timezone']) ? $_SESSION['user_timezone'] : 'Asia/Tokyo';
$languages = getLanguages();

$file = $basedir . '/temp/all_users.txt';
$data = json_decode(file_get_contents($file), true);
?>
<!DOCTYPE HTML>
<html>
<?php include $basedir . '/common/header.php'; ?>
<body>

	<?php include $basedir . '/common/head.php'; ?>


	<div class="container row">
		<?php include $basedir . '/common/myheadmenu.php';?>
		<main role="main" class="row gutters mypage">

			<article id="myaccount" class="col span_9">

				<div class="box">
					<div class="title_box">
						<h4 class="title">Login History</h4>
						<p class="desc"><?php echo $lang[486];?></p>
					</div>
	

					 <table class="table table-striped table-bordered table-hover table-condensed">
                     <thead>
                     	<tr>
                        <th>Date Time</th>
                        <th>IP Address</th>
                        </tr>
                      </thead>
                      <tbody>                     
                     <?php 	$iLoginIpDetailsNew = mysqli_query($con, "SELECT * FROM login_log WHERE userid = '".$_SESSION['user_id']."'");
					  		while($iLogIp = mysqli_fetch_array($iLoginIpDetailsNew)) { ?>
                     <tr>
                     <td><?php echo $iLogIp['create_at']; ?></td>
                     <td><?php echo $iLogIp['ip_address']; ?></td>
                     </tr>
                     <?php } ?>
                     </tbody>
                     
                     </table>

				</div>

			</article>
			<aside id="mymenu" role="mymenu" class="col span_3">

				<?php include $basedir . '/common/mymenu.php'; ?>

			</aside>
		</main>

	</div>
	<script src="<?php echo $baseurl?>/js/jquery-1.11.0.min.js"></script>
    <?php include $basedir . '/common/foot.php'; ?>

    <script src="<?php echo $baseurl?>/js/jquery.remodal.js"></script>
	<script type="text/javascript" src="<?php echo $baseurl?>/js/main_frontend.js"></script>

	<script src="<?php echo $baseurl?>/js/jquery.minimalect.min.js"></script>
	<script type="text/javascript">
	  $(document).ready(function(){
	    $("#myaccount form.inputform select").minimalect();
	  });
	</script>


</body>

</html>
<script>
$(document).ready(function () {
	$("form").bind("submit", submitForm);
	function submitForm(e) {
	    e.preventDefault();
	    e.target.checkValidity();
	    var lang = $('select[name="language"]').val();
	    var old_lang = $('input[name="old_lang"]').val();
	    var tz = $('select[name="timezone"]').val();
	    var t = "<?php echo $time;?>";
	    var url = "<?php echo $baseurl ?>";
	    var key = "<?php echo $public_key?>";
	    var hash = "<?php echo $hash?>";
	    url = url + "/ajax/changelangtz.php";
	    var uri = 'hash=' + hash + '&public=' + key + '&t=' + t;
	    uri += '&lang=' + lang + '&tz=' + tz;
	
	    $.ajax({
	        type: 'POST',
	        url: url,
	        beforeSend: function(x) {
	            if(x && x.overrideMimeType) {
	                x.overrideMimeType("application/json;charset=UTF-8");
	            }
	        },
	        data: uri,
	        success: function(data){
	        	if (lang && lang !== old_lang) {
		        	window.location.reload(true);
	        	}
                $('#message').html(data.message);
                setTimeout(function() {
	                $('#message').html('');
                }, 3000)
	        }
	        
	    });
	}
})
</script>
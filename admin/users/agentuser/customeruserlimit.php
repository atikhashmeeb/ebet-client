<?php
require_once("../../../include/config.php");
require_once($basedir . "/admin/include/functions.php");
include $basedir . '/admin/include/isadmin.php';
$agents = getAssignUser();
$usermenu='active';
$datatables='active';


$iThisUserDetail=mysqli_query($con, "SELECT * FROM  users WHERE user_id = '".$_GET['user_id']."'");
$iTHisUserDet = mysqli_fetch_array($iThisUserDetail);





?>
<!DOCTYPE html>
<html>
<head>
<?php include $basedir . '/admin/include/header.php'; ?>
 
</head>
<body class="skin-blue">
<header class="header">
  <?php include $basedir . '/admin/include/header_menu.php'; ?>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <aside class="left-side sidebar-offcanvas">
    <section class="sidebar">
      <?php include $basedir . '/admin/include/sidebar.php'; ?>
    </section>
  </aside>
  <aside class="right-side">
    <section class="content-header">
      <h1><?php echo $lang[17] ?><small><?php echo $lang[31] ?></small></h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo $baseurl ?>/"><i class="fa fa-dashboard"></i><?php echo $lang[27] ?></a></li>
        <li><a href="<?php echo $baseurl ?>/users"><?php echo $lang[14] ?></a></li>
        <li class="active"><?php echo $lang[17] ?></li>
      </ol>
    </section>
    <section class="content">
      <?php include('miniheader.php'); ?>
      <div class="tabs-sections" style="height:-webkit-fill-available">
        <div class="tabs-sections__section  tabs-sections__section--active  tab-js  tab-active-js">
          <form action="" method="POST">
            <div class="box" style=" background:#fff;">
              <div class="box-body" style=" background:#fff;">
                <div  style=" background:#fff;     height: -webkit-fill-available;">
                  <div  style="width:400px;margin-right:100px; float:left;">
                  <table  class="table table-hover">
                    <tr> <td colspan="3" style="background:#4f4f4f; color:#fff;" >Limit</td></tr>
                    </table>
                    <div class="form-group">
                      <label>Credit Limit</label>
                      
                      <br>
                      <?php echo $iTHisUserDet['credit_limit']; ?>
                    </div>
                  </div><?php //echo $iTHisUSer['user_available_balance']; ?>
                  <div  style="width:50%;  margin-left:10px; float:left;">
                   <table  class="table table-hover">
                    <tr> <td colspan="3" style="background:#4f4f4f; color:#fff;" >General Limit</td></tr>
                    </table>
                    <table  class="table table-hover">
                      <thead>
                        <tr>
                          <th></th>
                          <th align="center">Min</th>
                          <th align="center">Max</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                        <?php
					
$iCreditLimit1=mysqli_query($con, "SELECT * FROM  credit_limit WHERE userid = '".$_GET['user_id']."' and type_id = '1'");
$iCredLimi1 = mysqli_fetch_array($iCreditLimit1);
						?>
                          <td>All </td>
                          <td><?php echo $iCredLimi1['minamount']; ?></td>
                          <td><?php echo $iCredLimi1['maxamount']; ?></td>
                        </tr>
                        <tr>
                        <?php
					
$iCreditLimit2=mysqli_query($con, "SELECT * FROM  credit_limit WHERE userid = '".$_GET['user_id']."' and type_id = '2'");
$iCredLimi2 = mysqli_fetch_array($iCreditLimit2);
						?>
                          <td>Straight </td>
                          <td><?php echo $iCredLimi2['minamount']; ?></td>
                          <td><?php echo $iCredLimi2['maxamount']; ?></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <br>
                
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
  </aside>
</div>
<?php include $basedir . '/include/javascript.php'; ?>
</body>
</html>
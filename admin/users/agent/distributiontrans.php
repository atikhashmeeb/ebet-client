<?php
require_once("../../../include/config.php");
require_once($basedir . "/admin/include/functions.php");
include $basedir . '/admin/include/isadmin.php';
$agents = getAssignUser();
$usermenu='active';
$datatables='active';



?>
<!DOCTYPE html>
<html>
<head>
<?php include $basedir . '/admin/include/header.php'; ?>
</head>
<body class="skin-blue">
<header class="header">
  <?php include $basedir . '/admin/include/header_menu.php'; ?>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <aside class="left-side sidebar-offcanvas">
    <section class="sidebar">
      <?php include $basedir . '/admin/include/sidebar.php'; ?>
    </section>
  </aside>
  <aside class="right-side">
    <section class="content-header">
      <h1><?php echo $lang[17] ?><small>Distribution Transcation</small></h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo $baseurl ?>/"><i class="fa fa-dashboard"></i><?php echo $lang[27] ?></a></li>
        <li><a href="<?php echo $baseurl ?>/users/agent">Agent</a></li>
        <li class="active">Distribution Transcation></li>
      </ol>
    </section>
    <section class="content">

      <div class="tabs-sections" style="height:-webkit-fill-available">
        <div class="tabs-sections__section  tabs-sections__section--active  tab-js  tab-active-js">
    
          <div class="box-body box  no-padding">
              <table id="datatable" class="table table-hover">
              <thead>
                <tr>
                  <th>S No.</th>
                  <th>Date</th>
                  <th>Amount</th>
                  <th>Description</th>
                  <th>Type</th>
                  <th>Source</th>
                </tr>
              </thead>
              <tbody>
                <?php 
					$i=0;
					if($iLoginRow['user_isadmin']=='1'){
						
					$iThisParticularUserDetail=mysqli_query($con, "SELECT * FROM  transaction  order by id desc");
					
					} else {
						
							
					$iThisParticularUserDetail=mysqli_query($con, "SELECT * FROM  transaction WHERE userid = '".$_SESSION['user_id']."' order by id desc");
					
					}
					
					
					while($iTHisUser = mysqli_fetch_array($iThisParticularUserDetail)) {
					 $i=$i+1;;
					 $iAgentId=mysqli_query($con, "SELECT * FROM  users WHERE user_id = '".$iTHisUser['agent_id']."'");
					 $iAgent = mysqli_fetch_assoc($iAgentId);
	                ?>
                <tr>
                  <td><?php echo $i	?></td>
                  <td><?php echo $iTHisUser['create_at']	?></td>
                  <td><?php echo $iTHisUser['amount']	?></td>
                  <td><?php echo $iTHisUser['description']	?></td>
                  <td><?php if($iTHisUser['type']=='0') { echo 'Withdrawal'; } else { echo 'Deposite'; }	?></td>
                  <td><?php if($iTHisUser['source']=='0') { 
						   											echo 'For Betting'; 
																 } 
								elseif($iTHisUser['source']=='1') { 
																	echo 'Done By Admin <b> (  '. $iAgent['user_name'].' ) </b>'; 
																	
																 }
								elseif($iTHisUser['source']=='2') { 
																	echo 'Wager Deleted By Agent <b> (  '. $iAgent['user_name'].' ) </b>'  ; 
																  }								 	
																  ?></td>
                </tr>
                <?php }  ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </section>
  </aside>
</div>
<?php include $basedir . '/include/javascript.php'; ?>
</body>
</html>

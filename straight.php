<?php
require_once('include/config.php');
require_once($basedir . "/include/functions.php");
require_once($basedir . '/include/user_functions.php');
if ($_SESSION['user_id'] == '') {
    header('Location: ' . $baseurl . '/log.php');
    exit;
}

$settingsmenu = 'active';
$accountmenu = 'active';

$def_tz = ($_SESSION['user_timezone']) ? $_SESSION['user_timezone'] : 'Asia/Tokyo';
$languages = getLanguages();

$file = $basedir . '/temp/all_users.txt';
$data = json_decode(file_get_contents($file), true);


$iDetailDisableSportId = mysqli_query($con, "SELECT group_concat(sport_league) as DisableSportId FROM wager_disable WHERE type = '1' LIMIT 0, 1");
$DisableSportId = mysqli_fetch_array($iDetailDisableSportId);
$iAllDisableSportId = explode(',', $DisableSportId['DisableSportId']);


$iDetailDisableLeagueId = mysqli_query($con, "SELECT group_concat(sport_league) as DisableLeagueId FROM wager_disable WHERE type = '2' LIMIT 0, 1");
$DisableLeagueId = mysqli_fetch_array($iDetailDisableLeagueId);
$iAllDisableLeagueId = explode(',', $DisableLeagueId['DisableLeagueId']);
?>

<!DOCTYPE HTML>
<html>
<?php include $basedir . '/common/header.php'; ?>
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Rubik">

<style>
    body {
        background: #ededed !important;
        font-family: 'Rubik', serif;
    }

    .btn-group a {
        background: white;
        border: 0;
        border-radius: 0;
        color: #1c3845;
        font-weight: 600;
        text-decoration: none;
        font-family: sans-serif;
        font-size: 14px;
    }

    .bg-light {
        background-color: #f8f9fa !important;
        padding: 20px;
    }

    .box h1 {
        padding: 0px 0px 0;
        font-size: 16px;
        font-family: sans-serif;
    }

    thead.bg-primary {
        border-bottom: 3px solid #b21e22;
        background-color: #525252 !important;
    }

    .btn-group, .btn-group-vertical {
        position: relative;
        display: -webkit-inline-box;
        display: -ms-inline-flexbox;
        display: inline-flex;
        vertical-align: middle;
    }

    .float-right {
        float: right !important;

        color: #fff;
        border: 1px solid #a7aaae;
        text-decoration: none;
        padding: 5px 11px;
        background-color: #b21e22;
        border-radius: 3px;
        margin-right: 20px;
        cursor: pointer;
    }

    .float-right:hover {
        background-color: #f45f62;
        color: white;
    }

    .col-md-6 {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 50%;
        flex: 0 0 50%;
        width: 470px;
        float: left;
        padding-left: 20px;
    }

    .col-md-9 {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 50%;
        flex: 0 0 50%;
        width: 470px;
        float: right;
        padding-right: 20px;
    }

    .btn {
        display: inline-block;
        font-weight: 400;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        border: 1px solid transparent;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        border-radius: .25rem;
        transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;
    }

    .btn-primary {
        color: #fff;
        background-color: #007bff;
        border-color: #007bff;
    }

    .box .table {
        margin: 0px auto 20px;
    }

    .box .status, .box .table {
        width: 100%;
        margin: 0 auto 10px;
        border-top: 0px;
        padding: 4px 0;
        height: inherit;
        border: 1px solid #DADADA;
    }

    .box .status, .box .table tbody tr td {
        text-align: left;
    }

    .bg-primary {
        background-color: #525252 !important;
        padding: 6px 15px 6px 13px;
        color: white;
        border: 0;
    }

    #myaccount .box, #dashboard .box {
        margin: 0px 0px 0px 0px;
    }

    .selectRow:hover {
        background-color: #cfd0d1;
        color: black;
    }

    .selectRow span.float-right {
        color: #f8f9fa;
    }

    @media only screen and (max-width: 991px) {
        #myaccount .box, #dashboard .box {
            margin: 0px 10px 0px 10px;
        }

        .selectRow:hover {
            background-color: #ffeb3b61;
            color: black;
        }

        .selectRow span.float-right {
            color: #f8f9fa;
        }

        .clicked {
            background-color: #ffeb3b61;
            color: black;
        }

        .btn-group a {
            border-radius: 0;
        }

        .btn-group.col-xs-12 {
            width: 100%;
            background: #f8f9fa;
        }

        .gutters .col:first-child {
            padding: 0px 0;
        }

        .box .status, .box .table {
            width: 100%;
            margin: 0 auto 10px;
            border-top: 0px;
            padding: 4px 0;
            height: inherit;
            border: 2px solid #060f42;
        }

        .box .status, .box .table tbody tr td {
            text-align: left;
        }

        .btn {
            display: inline-block;
            font-weight: 400;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: .375rem .75rem;
            font-size: 1rem;
            line-height: 1.5;
            border-radius: .25rem;
            transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;
        }

        .btn-primary {
            color: #fff;
            background-color: #007bff;
            border-color: #007bff;
        }

        .btn-success {
            color: #fff;
            background-color: #28a745;
            border-color: #28a745;
        }
</style>
<script>
    function addleague(iLeagueId) {


        if (document.getElementById("addremove" + iLeagueId).classList.contains('trselected')) {

            document.getElementById("addremove" + iLeagueId).classList.remove('trselected');
            document.getElementById("leagueid" + iLeagueId).value = '';

        }
        else {
            document.getElementById("addremove" + iLeagueId).classList.add('trselected');
            document.getElementById("leagueid" + iLeagueId).value = iLeagueId;
        }
    }
    function Submitform() {


        var empty = 0;
        var totaltextbox = 0;
        $('.leaguetext').each(function () {
            totaltextbox++;
            if (this.value == "") {

                empty++;

            }
        })

        if (empty == totaltextbox) {
            alert('Please select atleast 1 league');
        }
        else {
            document.getElementById("straightbetform").submit();
        }
    }
</script>
<body>
<?php include $basedir . '/common/head.php'; ?>
<div class="container row">
    <?php include $basedir . '/common/myheadmenu.php'; ?>
    <main role="main" class="row gutters mypage">
        <article id="myaccount" class="col span_12">
            <div class="box">
                <form action="StraightBet-new.php" id="straightbetform" method="post">
                    <div class='clearfix'>
                        <marquee>
                            <?php

                            $iThisParticularUserDetail = mysqli_query($con, "SELECT * FROM  notification WHERE userid = '" . $_SESSION['user_id'] . "' and is_seen='0' order by id desc");
                            while ($iTHisUser = mysqli_fetch_array($iThisParticularUserDetail)) {
                                ?>
                                <span style="font-size:15px;">*</span> <?php echo $iTHisUser['message'] ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <?php }
                            mysqli_query($con, "update notification set is_seen = '1' WHERE userid = '" . $_SESSION['user_id'] . "' and is_seen='0' order by id desc");

                            ?>
                        </marquee>
                        <input type="button" onClick="Submitform()" class="mt-3 mr-4 float-right" value="Continue">
                    </div>
                    <br>
                    </br>


                    <div class="col-md-6">
                        <?php if (!in_array("16", $iAllDisableSportId)) { ?>
                            <div class="col-md-6 d-inline-block">
                                <div class="table-responsive">
                                    <table class="mt-3 table table-dark">
                                        <thead class="bg-primary">
                                        <tr>
                                            <th scope="col"><h1 style="color:#fff">Baseball:</h1></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        'https://api.betsapi.com/v1/bet365/upcoming?sport_id=16&token=26928-6HWCoyS7zhskxm';
                                        $ch = curl_init();
                                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        curl_setopt($ch, CURLOPT_URL, 'https://api.betsapi.com/v1/bet365/upcoming?sport_id=16&token=26928-6HWCoyS7zhskxm');
                                        $result = curl_exec($ch);
                                        curl_close($ch);
                                        $obj = json_decode($result, true);
                                        ?>
                                        <?php
                                        $iLeagueId = array();
                                        foreach ($obj['results'] as $key => $val) {
                                            $iThisIsLeagueId = $val['league']['id'];

                                            if (!in_array($iThisIsLeagueId, $iAllDisableLeagueId)) {

                                                if (!in_array($iThisIsLeagueId, $iLeagueId)) {
                                                    array_push($iLeagueId, $iThisIsLeagueId); ?>
                                                    <tr class="selectRow" style="cursor:pointer"
                                                        id="addremove<?php echo $iThisIsLeagueId; ?>"
                                                        onClick="addleague('<?php echo $iThisIsLeagueId; ?>')">
                                                        <td><?php echo $val['league']['name']; ?>
                                                            <input type="hidden" name="leagueid[]" class="leaguetext"
                                                                   id="leagueid<?php echo $iThisIsLeagueId; ?>">
                                                            <input type="hidden" name="sport_id[]" class="leaguetext"
                                                                   value="16"></td>
                                                    </tr>
                                                <?php }
                                            }
                                        } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (!in_array("17", $iAllDisableSportId)) { ?>
                            <div class="col-md-6 d-inline-block">
                                <div class="table-responsive">
                                    <table class="mt-3 table table-dark">
                                        <thead class="bg-primary">
                                        <tr>
                                            <th scope="col"><h1 style="color:#fff">Hockey:</h1></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $ch = curl_init();
                                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        curl_setopt($ch, CURLOPT_URL, 'https://api.betsapi.com/v1/bet365/upcoming?sport_id=17&token=26928-6HWCoyS7zhskxm');
                                        $result = curl_exec($ch);
                                        curl_close($ch);
                                        $obj = json_decode($result, true);
                                        ?>
                                        <?php
                                        $iLeagueId = array();
                                        foreach ($obj['results'] as $key => $val) {
                                            $iThisIsLeagueId = $val['league']['id'];

                                            if (!in_array($iThisIsLeagueId, $iAllDisableLeagueId)) {


                                                if (!in_array($iThisIsLeagueId, $iLeagueId)) {
                                                    array_push($iLeagueId, $iThisIsLeagueId); ?>
                                                    <tr class="selectRow" style="cursor:pointer"
                                                        id="addremove<?php echo $iThisIsLeagueId; ?>"
                                                        onClick="addleague('<?php echo $iThisIsLeagueId; ?>')">
                                                        <td><?php echo $val['league']['name']; ?>
                                                            <input type="hidden" name="leagueid[]" class="leaguetext"
                                                                   id="leagueid<?php echo $iThisIsLeagueId; ?>">
                                                            <input type="hidden" name="sport_id[]" class="leaguetext"
                                                                   value="17"></td>
                                                    </tr>
                                                <?php }
                                            }
                                        } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (!in_array("66", $iAllDisableSportId)) { ?>
                            <div class="col-md-6 d-inline-block">
                                <div class="table-responsive">
                                    <table class="mt-3 table table-dark">
                                        <thead class="bg-primary">
                                        <tr>
                                            <th scope="col"><h1 style="color:#fff">Golf:</h1></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $ch = curl_init();
                                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        curl_setopt($ch, CURLOPT_URL, 'https://api.betsapi.com/v1/bet365/upcoming?sport_id=66&token=26928-6HWCoyS7zhskxm');
                                        $result = curl_exec($ch);
                                        curl_close($ch);
                                        $obj = json_decode($result, true);
                                        ?>
                                        <?php
                                        $iLeagueId = array();
                                        foreach ($obj['results'] as $key => $val) {
                                            $iThisIsLeagueId = $val['league']['id'];

                                            if (!in_array($iThisIsLeagueId, $iAllDisableLeagueId)) {


                                                if (!in_array($iThisIsLeagueId, $iLeagueId)) {
                                                    array_push($iLeagueId, $iThisIsLeagueId); ?>
                                                    <tr class="selectRow" style="cursor:pointer"
                                                        id="addremove<?php echo $iThisIsLeagueId; ?>"
                                                        onClick="addleague('<?php echo $iThisIsLeagueId; ?>')">
                                                        <td><?php echo $val['league']['name']; ?>
                                                            <input type="hidden" name="leagueid[]" class="leaguetext"
                                                                   id="leagueid<?php echo $iThisIsLeagueId; ?>">
                                                            <input type="hidden" name="sport_id[]" class="leaguetext"
                                                                   value="66"></td>
                                                    </tr>
                                                <?php }
                                            }
                                        } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (!in_array("9", $iAllDisableSportId)) { ?>
                            <div class="col-md-6 d-inline-block">
                                <div class="table-responsive">
                                    <table class="mt-3 table table-dark">
                                        <thead class="bg-primary">
                                        <tr>
                                            <th scope="col"><h1 style="color:#fff">MMA:</h1></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $ch = curl_init();
                                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        curl_setopt($ch, CURLOPT_URL, 'https://api.betsapi.com/v1/bet365/upcoming?sport_id=9&token=26928-6HWCoyS7zhskxm');
                                        $result = curl_exec($ch);
                                        curl_close($ch);
                                        $obj = json_decode($result, true);
                                        ?>
                                        <?php
                                        $iLeagueId = array();
                                        foreach ($obj['results'] as $key => $val) {
                                            $iThisIsLeagueId = $val['league']['id'];

                                            if (!in_array($iThisIsLeagueId, $iAllDisableLeagueId)) {


                                                if (!in_array($iThisIsLeagueId, $iLeagueId)) {
                                                    array_push($iLeagueId, $iThisIsLeagueId); ?>
                                                    <tr class="selectRow" style="cursor:pointer"
                                                        id="addremove<?php echo $iThisIsLeagueId; ?>"
                                                        onClick="addleague('<?php echo $iThisIsLeagueId; ?>')">
                                                        <td><?php echo $val['league']['name']; ?>
                                                            <input type="hidden" name="leagueid[]" class="leaguetext"
                                                                   id="leagueid<?php echo $iThisIsLeagueId; ?>">
                                                            <input type="hidden" name="sport_id[]" class="leaguetext"
                                                                   value="9"></td>
                                                    </tr>
                                                <?php }
                                            }
                                        } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php } ?>


                        <?php if (!in_array("13", $iAllDisableSportId)) { ?>
                            <div class="col-md-6 d-inline-block">
                                <div class="table-responsive">
                                    <table class="mt-3 table table-dark">
                                        <thead class="bg-primary">
                                        <tr>
                                            <th scope="col"><h1 style="color:#fff">Tennis:</h1></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $ch = curl_init();
                                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        curl_setopt($ch, CURLOPT_URL, 'https://api.betsapi.com/v1/bet365/upcoming?sport_id=13&token=26928-6HWCoyS7zhskxm');
                                        $result = curl_exec($ch);
                                        curl_close($ch);
                                        $obj = json_decode($result, true);
                                        ?>
                                        <?php
                                        $iLeagueId = array();
                                        foreach ($obj['results'] as $key => $val) {
                                            $iThisIsLeagueId = $val['league']['id'];

                                            if (!in_array($iThisIsLeagueId, $iAllDisableLeagueId)) {


                                                if (!in_array($iThisIsLeagueId, $iLeagueId)) {
                                                    array_push($iLeagueId, $iThisIsLeagueId); ?>
                                                    <tr class="selectRow" style="cursor:pointer"
                                                        id="addremove<?php echo $iThisIsLeagueId; ?>"
                                                        onClick="addleague('<?php echo $iThisIsLeagueId; ?>')">
                                                        <td><?php echo $val['league']['name']; ?>
                                                            <input type="hidden" name="leagueid[]" class="leaguetext"
                                                                   id="leagueid<?php echo $iThisIsLeagueId; ?>">
                                                            <input type="hidden" name="sport_id[]" class="leaguetext"
                                                                   value="13"></td>
                                                    </tr>
                                                <?php }
                                            }
                                        } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php } ?>


                        <div class="col-md-6 d-inline-block">
                            <div class="table-responsive">
                                <table class="mt-3 table table-dark">
                                    <thead class="bg-primary">
                                    <tr>
                                        <th scope="col"><h1 style="color:#fff">Other:</h1></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr id="MLB" class="selectRow">
                                        <td>Rugby</td>
                                    </tr>
                                    <tr id="MLB" class="selectRow">
                                        <td>Darts</td>
                                    </tr>
                                    <tr id="MLB" class="selectRow">
                                        <td>MLB - Alternate Lines</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                        <br>
                        </br>
                    </div>

                    <div class="col-md-6">

                        <?php if (!in_array("12", $iAllDisableSportId)) { ?>
                            <div class="col-md-6 d-inline-block">
                                <div class="table-responsive">
                                    <table class="mt-3 table table-dark">
                                        <thead class="bg-primary">
                                        <tr>
                                            <th scope="col"><h1 style="color:#fff">Football:</h1></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $ch = curl_init();
                                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        curl_setopt($ch, CURLOPT_URL, 'https://api.betsapi.com/v1/bet365/upcoming?sport_id=12&token=26928-6HWCoyS7zhskxm');
                                        $result = curl_exec($ch);
                                        curl_close($ch);
                                        $obj = json_decode($result, true);
                                        ?>
                                        <?php
                                        $iLeagueId = array();
                                        foreach ($obj['results'] as $key => $val) {
                                            $iThisIsLeagueId = $val['league']['id'];

                                            if (!in_array($iThisIsLeagueId, $iAllDisableLeagueId)) {

                                                if (!in_array($iThisIsLeagueId, $iLeagueId)) {
                                                    array_push($iLeagueId, $iThisIsLeagueId); ?>
                                                    <tr class="selectRow" style="cursor:pointer"
                                                        id="addremove<?php echo $iThisIsLeagueId; ?>"
                                                        onClick="addleague('<?php echo $iThisIsLeagueId; ?>')">
                                                        <td><?php echo $val['league']['name']; ?>
                                                            <input type="hidden" name="leagueid[]" class="leaguetext"
                                                                   id="leagueid<?php echo $iThisIsLeagueId; ?>">
                                                            <input type="hidden" name="sport_id[]" class="leaguetext"
                                                                   value="12"></td>
                                                    </tr>
                                                <?php }
                                            }
                                        } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (!in_array("1", $iAllDisableSportId)) { ?>
                            <div class="col-md-6 d-inline-block">
                                <div class="table-responsive">
                                    <table class="mt-3 table table-dark">
                                        <thead class="bg-primary">
                                        <tr>
                                            <th scope="col"><h1 style="color:#fff">Soccer:</h1></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $ch = curl_init();
                                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        curl_setopt($ch, CURLOPT_URL, 'https://api.betsapi.com/v1/bet365/upcoming?sport_id=1&token=26928-6HWCoyS7zhskxm');
                                        $result = curl_exec($ch);
                                        curl_close($ch);
                                        $obj = json_decode($result, true);
                                        ?>
                                        <?php
                                        $iLeagueId = array();
                                        foreach ($obj['results'] as $key => $val) {
                                            $iThisIsLeagueId = $val['league']['id'];

                                            if (!in_array($iThisIsLeagueId, $iAllDisableLeagueId)) {


                                                if (!in_array($iThisIsLeagueId, $iLeagueId)) {
                                                    array_push($iLeagueId, $iThisIsLeagueId); ?>
                                                    <tr class="selectRow" style="cursor:pointer"
                                                        id="addremove<?php echo $iThisIsLeagueId; ?>"
                                                        onClick="addleague('<?php echo $iThisIsLeagueId; ?>')">
                                                        <td><?php echo $val['league']['name']; ?>
                                                            <input type="hidden" name="leagueid[]" class="leaguetext"
                                                                   id="leagueid<?php echo $iThisIsLeagueId; ?>">
                                                            <input type="hidden" name="sport_id[]" class="leaguetext"
                                                                   value="1"></td>
                                                    </tr>
                                                <?php }
                                            }
                                        } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php } ?>


                        <?php if (!in_array("18", $iAllDisableSportId)) { ?>
                            <div class="col-md-6 d-inline-block">
                                <div class="table-responsive">
                                    <table class="mt-3 table table-dark">
                                        <thead class="bg-primary">
                                        <tr>
                                            <th scope="col"><h1 style="color:#fff">Basketball:</h1></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $ch = curl_init();
                                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        curl_setopt($ch, CURLOPT_URL, 'https://api.betsapi.com/v1/bet365/upcoming?sport_id=18&token=26928-6HWCoyS7zhskxm');
                                        $result = curl_exec($ch);
                                        curl_close($ch);
                                        $obj = json_decode($result, true);
                                        ?>
                                        <?php
                                        $iLeagueId = array();
                                        foreach ($obj['results'] as $key => $val) {
                                            $iThisIsLeagueId = $val['league']['id'];

                                            if (!in_array($iThisIsLeagueId, $iAllDisableLeagueId)) {


                                                if (!in_array($iThisIsLeagueId, $iLeagueId)) {
                                                    array_push($iLeagueId, $iThisIsLeagueId); ?>
                                                    <tr class="selectRow" style="cursor:pointer"
                                                        id="addremove<?php echo $iThisIsLeagueId; ?>"
                                                        onClick="addleague('<?php echo $iThisIsLeagueId; ?>')">
                                                        <td><?php echo $val['league']['name']; ?>
                                                            <input type="hidden" name="leagueid[]" class="leaguetext"
                                                                   id="leagueid<?php echo $iThisIsLeagueId; ?>">
                                                            <input type="hidden" name="sport_id[]" class="leaguetext"
                                                                   value="18"></td>
                                                    </tr>
                                                <?php }
                                            }
                                        } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (!in_array("1", $iAllDisableSportId)) { ?>
                            <div class="col-md-6 d-inline-block">
                                <div class="table-responsive">
                                    <table class="mt-3 table table-dark">
                                        <thead class="bg-primary">
                                        <tr>
                                            <th scope="col"><h1 style="color:#fff">E-Sports:</h1></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $ch = curl_init();
                                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        curl_setopt($ch, CURLOPT_URL, 'https://api.betsapi.com/v1/bet365/upcoming?sport_id=151&token=26928-6HWCoyS7zhskxm');
                                        $result = curl_exec($ch);
                                        curl_close($ch);
                                        $obj = json_decode($result, true);
                                        ?>
                                        <?php
                                        $iLeagueId = array();
                                        foreach ($obj['results'] as $key => $val) {
                                            $iThisIsLeagueId = $val['league']['id'];

                                            if (!in_array($iThisIsLeagueId, $iAllDisableLeagueId)) {


                                                if (!in_array($iThisIsLeagueId, $iLeagueId)) {
                                                    array_push($iLeagueId, $iThisIsLeagueId); ?>
                                                    <tr class="selectRow" style="cursor:pointer"
                                                        id="addremove<?php echo $iThisIsLeagueId; ?>"
                                                        onClick="addleague('<?php echo $iThisIsLeagueId; ?>')">
                                                        <td><?php echo $val['league']['name']; ?>
                                                            <input type="hidden" name="leagueid[]" class="leaguetext"
                                                                   id="leagueid<?php echo $iThisIsLeagueId; ?>">
                                                            <input type="hidden" name="sport_id[]" class="leaguetext"
                                                                   value="151"></td>
                                                    </tr>
                                                <?php }
                                            }
                                        } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php } ?>


                    </div>


                </form>
            </div>
        </article>
    </main>
</div>
<style>
    .trselected {
        background-color: #b21e22 !important;
        color: #fff !important;
    }
</style>
<script src="<?php echo $baseurl ?>/js/jquery-1.11.0.min.js"></script>
<script src="http://ebet21.com/oldwebsite/php/Assets/JS/Index_JS.js"></script>
<?php include $basedir . '/common/foot.php'; ?>
<script src="<?php echo $baseurl ?>/js/jquery.remodal.js"></script>
<script type="text/javascript" src="<?php echo $baseurl ?>/js/main_frontend.js"></script>
<script src="<?php echo $baseurl ?>/js/jquery.minimalect.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#myaccount form.inputform select").minimalect();
    });
</script>
</body>
</html>
<script>
    $(document).ready(function () {
        $("form").bind("submit", submitForm);
        function submitForm(e) {
            e.preventDefault();
            e.target.checkValidity();
            var lang = $('select[name="language"]').val();
            var old_lang = $('input[name="old_lang"]').val();
            var tz = $('select[name="timezone"]').val();
            var t = "<?php echo $time;?>";
            var url = "<?php echo $baseurl ?>";
            var key = "<?php echo $public_key?>";
            var hash = "<?php echo $hash?>";
            url = url + "/ajax/changelangtz.php";
            var uri = 'hash=' + hash + '&public=' + key + '&t=' + t;
            uri += '&lang=' + lang + '&tz=' + tz;

            $.ajax({
                type: 'POST',
                url: url,
                beforeSend: function (x) {
                    if (x && x.overrideMimeType) {
                        x.overrideMimeType("application/json;charset=UTF-8");
                    }
                },
                data: uri,
                success: function (data) {
                    if (lang && lang !== old_lang) {
                        window.location.reload(true);
                    }
                    $('#message').html(data.message);
                    setTimeout(function () {
                        $('#message').html('');
                    }, 3000)
                }

            });
        }
    })
</script>

</body>
</html>
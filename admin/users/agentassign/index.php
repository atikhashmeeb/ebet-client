<?php
require_once("../../../include/config.php");
require_once($basedir . "/admin/include/functions.php");
include $basedir . '/admin/include/isadmin.php';

$agents = getAssignUser();
$usermenu='active';
$datatables='active';
?>
<!DOCTYPE html>
<html>
	<head>
		<?php include $basedir . '/admin/include/header.php'; ?>
	</head>
	<body class="skin-blue">
	<header class="header">
		<?php include $basedir . '/admin/include/header_menu.php'; ?>
	</header>
	<div class="wrapper row-offcanvas row-offcanvas-left">
		<aside class="left-side sidebar-offcanvas">                
			<section class="sidebar">
				<?php include $basedir . '/admin/include/sidebar.php'; ?>
			</section>
		</aside>
		<aside class="right-side">
			<section class="content-header">
				<h1><?php echo $lang[17] ?><small><?php echo $lang[31] ?></small></h1>
				<ol class="breadcrumb">
					<li><a href="<?php echo $baseurl ?>/"><i class="fa fa-dashboard"></i><?php echo $lang[27] ?></a></li>
					<li><a href="<?php echo $baseurl ?>/users"><?php echo $lang[14] ?></a></li>
					<li class="active"><?php echo $lang[17] ?></li>
				</ol>
			</section>
			<section class="content">
            	<?php 
				
				if($_GET['action']=='edit') {  
				
				include $basedir . '/admin/users/agentassign/editcustomer.php'; 
				
				} 
				elseif($_GET['action']=='chanpass') {  
				
				include $basedir . '/admin/users/agentassign/passwordcustomer.php'; 
				
				} 
				
				 else {  
				
				include $basedir . '/admin/users/agentassign/agentlistbody.php'; 
				
				} ?>
			</section>
		</aside>
	</div>
	<?php include $basedir . '/include/javascript.php'; ?>
	</body>
</html>
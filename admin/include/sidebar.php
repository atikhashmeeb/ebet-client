<?php $iCurrentFileName = basename($_SERVER['PHP_SELF']); ?>
<div class="user-panel">
	<div class="pull-left image">
		<img src="<?php echo $PROFILE_PIC; ?>" class="img-circle" alt="User Image" />
	</div>
	<div class="pull-left info">
		<p><?php echo $lang[1] ?><?php echo $USERNAME?></p>
	</div>
</div>

<?php /*
<!-- search form -->
	<?php  include ''.$baseurl.'/include/sidebar_search.php'; ?>
<!-- /.search form -->
*/
?>

<!-- sidebar menu -->
<ul class="sidebar-menu">
	<!-- Dashboard -->
	<li>
		<a href="<?php echo $baseurl ?>/admin/?lang=<?php echo $LANGUAGE ?>">
			<i class="fa fa-dashboard"></i><span><?php echo $lang[2] ?></span>
		</a>
	</li>
    
    
    
    <?php
	$iLoginUser = "SELECT * FROM users WHERE user_id = '".$_SESSION['user_id']."'";
	$iLoginresult = mysqli_query($con,$iLoginUser);
    $iLoginRow = mysqli_fetch_array($iLoginresult);
	
	if($iLoginRow['user_isadmin']=='5') {
		?>

	<li class="treeview <?php if($iCurrentFileName=='agentusercustomer.php' || $iCurrentFileName=='agentuseractiveuser.php' ) { echo "active"; } ?>">
		<a href="#">
			<i class="fa fa-group"></i><span>User Management</span>
			<i class="fa fa-angle-right pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo $baseurl ?>/admin/users/agentuser/agentusercustomer.php?lang=en"><i class="fa fa-angle-double-right"></i>Users</a></li>
			<li><a href="<?php echo $baseurl ?>/admin/users/agentuser/agentuseractiveuser.php?lang=en&active=player"><i class="fa fa-angle-double-right"></i>Active Users</a></li>
		</ul>
	</li>


<li class="treeview <?php if($iCurrentFileName=='agentuserweeklyfigure.php' || $iCurrentFileName=='agentuserallwager.php' || $iCurrentFileName=='agentuserpendingwager.php' || $iCurrentFileName=='agentuserdeleted_wager.php') { echo "active"; } ?>">
		<a href="#">
			<i class="fa fa-list-alt"></i><span>User Records</span>
			<i class="fa fa-angle-right pull-right"></i>
		</a>
		<ul class="treeview-menu">
		    <li><a href="<?php echo $baseurl ?>/admin/users/agentuser/agentuserweeklyfigure.php?lang=en"><i class="fa fa-angle-double-right"></i>Weekly Figures</a></li>
			<li><a href="<?php echo $baseurl ?>/admin/users/agentuser/agentuserallwager.php?lang=en"><i class="fa fa-angle-double-right"></i>Wagers</a></li>
			<li><a href="<?php echo $baseurl ?>/admin/users/agentuser/agentuserpendingwager.php?lang=en"><i class="fa fa-angle-double-right"></i>Pending Wagers</a></li>
			
			<li><a href="<?php echo $baseurl ?>/admin/users/agentuser/agentuserdeleted_wager.php?lang=en"><i class="fa fa-angle-double-right"></i>Deleted Wagers</a></li>
		
			
		</ul>
	</li>





    <?php }  elseif($iLoginRow['user_isadmin']=='2') {
		?>

	<li class="treeview <?php if($iCurrentFileName=='customer.php' ) { echo "active"; } ?>">
		<a href="#">
			<i class="fa fa-group"></i><span>User Management</span>
			<i class="fa fa-angle-right pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo $baseurl ?>/admin/users/agentassign/customer.php?lang=en"><i class="fa fa-angle-double-right"></i>Users</a></li>
			<li><a href="<?php echo $baseurl ?>/admin/users/agentassign/customer.php?lang=en&active=player"><i class="fa fa-angle-double-right"></i>Active Users</a></li>
			
	        			
		</ul>
	</li>

	<li class="treeview <?php if($iCurrentFileName=='allcustomer.php' || $iCurrentFileName=='all_wager.php' || $iCurrentFileName=='pending_wager.php' || $iCurrentFileName=='deleted_wager.php') { echo "active"; } ?>">
		<a href="#">
			<i class="fa fa-list-alt"></i><span>User Records</span>
			<i class="fa fa-angle-right pull-right"></i>
		</a>
		<ul class="treeview-menu">
		    <li><a href="<?php echo $baseurl ?>/admin/users/agentassign/allcustomer.php?lang=en"><i class="fa fa-angle-double-right"></i>Weekly Figures</a></li>
			<li><a href="<?php echo $baseurl ?>/admin/users/agentassign/all_wager.php?lang=en"><i class="fa fa-angle-double-right"></i>Wagers</a></li>
			<li><a href="<?php echo $baseurl ?>/admin/users/agentassign/pending_wager.php?lang=en"><i class="fa fa-angle-double-right"></i>Pending Wagers</a></li>
			
			<li><a href="<?php echo $baseurl ?>/admin/users/agentassign/deleted_wager.php?lang=en"><i class="fa fa-angle-double-right"></i>Deleted Wagers</a></li>
		
			
		</ul>
	</li>

	<li class="treeview <?php if($iCurrentFileName=='pending_wager.php' || $iCurrentFileName=='distributionlog.php') { echo "active"; } ?>">
		<a href="#">
			<i class="fa fa-bar-chart-o"></i><span>Distribution</span>
			<i class="fa fa-angle-right pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo $baseurl ?>/admin/users/agent/pending_wager.php?lang=en"><i class="fa fa-angle-double-right"></i>Agent Summary</a></li>
			<li><a href="<?php echo $baseurl ?>/admin/users/agent/distributionlog.php?lang=en"><i class="fa fa-angle-double-right"></i>Total Weekly Distribution</a></li>
		</ul>
	</li>


	<li class="treeview <?php if($iCurrentFileName=='agentuser.php' || $iCurrentFileName=='agentuserassign.php') { echo "active"; } ?>">
		<a href="#">
			<i class="fa fa-bar-chart-o"></i><span>Manage Agent</span>
			<i class="fa fa-angle-right pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo $baseurl ?>/admin/users/agentuser/agentuser.php?lang=en"><i class="fa fa-angle-double-right"></i>Manage Agent</a></li>
			<li><a href="<?php echo $baseurl ?>/admin/users/agentuser/agentuserassign.php?lang=en"><i class="fa fa-angle-double-right"></i>Assign User To Agent</a></li>
		</ul>
	</li>


    
    <?php } else { ?>
	

	


	<li class="treeview <?php if($iCurrentFileName=='allcustomers.php' || $iCurrentFileName=='index.php') { echo "active"; } ?>">
		<a href="#">
			<i class="fa fa-group"></i><span>User Management</span>
			<i class="fa fa-angle-right pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo $baseurl ?>/admin/users/agentassign/allcustomers.php?lang=en"><i class="fa fa-angle-double-right"></i>Users</a></li>
			<li><a href="<?php echo $baseurl ?>/admin/users/agentassign/allcustomers.php?lang=en&active=player"><i class="fa fa-angle-double-right"></i>Active Users</a></li>
			
			<li><a href="<?php echo $baseurl ?>/admin/users/agent?lang=<?php echo $LANGUAGE ?>"><i class="fa fa-angle-double-right"></i>Agents</a></li>
			<li><a href="<?php echo $baseurl ?>/admin/users/adminlist?lang=<?php echo $LANGUAGE ?>"><i class="fa fa-angle-double-right"></i>Administrators</a></li>
            
			
		</ul>
	</li>

	<li class="treeview <?php if($iCurrentFileName=='all_wager.php' || $iCurrentFileName=='pending_wager.php' || $iCurrentFileName=='deleted_wager.php') { echo "active"; } ?>">
		<a href="#">
			<i class="fa fa-list-alt"></i><span>User Records</span>
			<i class="fa fa-angle-right pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo $baseurl ?>/admin/users/agentassign/all_wager.php?lang=en"><i class="fa fa-angle-double-right"></i>All Wagers</a></li>
			<li><a href="<?php echo $baseurl ?>/admin/users/agentassign/pending_wager.php?lang=en"><i class="fa fa-angle-double-right"></i>Pending Wagers</a></li>
			
			<li><a href="<?php echo $baseurl ?>/admin/users/agentassign/deleted_wager.php?lang=en"><i class="fa fa-angle-double-right"></i>Deleted Wagers</a></li>
			
			
		</ul>
	</li>
	
	<!-- Game Schedules -->
<!--
	<li>
		<a href="<?php echo $baseurl ?>/calendar?lang=<?php echo $LANGUAGE ?>">
			<i class="fa fa-calendar"></i><span><?php echo $lang[21] ?></span>
			<i class-"fa fa-dansh"> </i> php echo @lang [21}] </span> 
		</a>
	</li>
-->
	
	<!-- Sales Management -->	
    
    
    

	<!-- Users -->	
	
	
	

	<!-- Settings -->	
	

	



	<!-- Notification -->	
<!--
	<li>
		<a href="#">
			<i class="fa fa-bullhorn"></i><span><?php echo $lang[22] ?></span>
			<small class="badge pull-right bg-yellow">12</small>
		</a>
	</li>
-->

<?php } ?>

</ul><!-- /.sidebar-menu -->

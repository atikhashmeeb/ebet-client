<?php
require_once 'unirest-php/src/Unirest.php';


$sportid=$_GET['sport_id'];
$response = Unirest\Request::get("https://therundown-therundown-v1.p.rapidapi.com/sports/$sportid/events/yyyy-mm-dd?include=all_periods%2C+scores%2C+and%2For+teams",
  array(
    "X-RapidAPI-Host" => "therundown-therundown-v1.p.rapidapi.com",
    "X-RapidAPI-Key" => "e8a6e38749msh8f0f3b316b9fc6dp1c6261jsn336d96c4f009"
  )
);
print_r($response);
?>

<style>
table.blueTable {
  font-family: Verdana, Geneva, sans-serif;
  border: 1px solid #1C6EA4;
  background-color: #EEEEEE;
  width: 100%;
  text-align: left;
  border-collapse: collapse;
}
table.blueTable td, table.blueTable th {
  border: 1px solid #AAAAAA;
  padding: 3px 2px;
}
table.blueTable tbody td {
  font-size: 16px;
}
table.blueTable tr:nth-child(even) {
  background: #D0E4F5;
}
table.blueTable thead {
  background: #1C6EA4;
  background: -moz-linear-gradient(top, #5592bb 0%, #327cad 66%, #1C6EA4 100%);
  background: -webkit-linear-gradient(top, #5592bb 0%, #327cad 66%, #1C6EA4 100%);
  background: linear-gradient(to bottom, #5592bb 0%, #327cad 66%, #1C6EA4 100%);
  border-bottom: 2px solid #444444;
}
table.blueTable thead th {
  font-size: 15px;
  font-weight: bold;
  color: #FFFFFF;
  border-left: 2px solid #D0E4F5;
}
table.blueTable thead th:first-child {
  border-left: none;
}

table.blueTable tfoot {
  font-size: 14px;
  font-weight: bold;
  color: #FFFFFF;
  background: #D0E4F5;
  background: -moz-linear-gradient(top, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);
  background: -webkit-linear-gradient(top, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);
  background: linear-gradient(to bottom, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);
  border-top: 2px solid #444444;
}
table.blueTable tfoot td {
  font-size: 14px;
}
table.blueTable tfoot .links {
  text-align: right;
}
table.blueTable tfoot .links a{
  display: inline-block;
  background: #1C6EA4;
  color: #FFFFFF;
  padding: 2px 8px;
  border-radius: 5px;
}
</style>
<table class="abctab blueTable">
<?php
foreach($response->body->events as $key => $val)
	{			?>
    <tr>
	
	<td>
   <?php echo $response->body->events[$key]->event_date; ?></td>
   
   <td>
   <?php
   foreach($response->body->events[$key]->teams as $key1 => $val1)
	{	
	echo $response->body->events[$key]->teams[$key1]->name; 
    echo '<br>';
	}
   ?>
   </td>

   <td>

   </td>

   </tr>
    <?php  } ?></table>
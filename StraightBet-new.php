<?php


require_once('include/config.php');
require_once($basedir . "/include/functions.php");
require_once($basedir . '/include/user_functions.php');


if ($_SESSION['user_id'] == '') {
    header('Location: ' . $baseurl . '/log.php');
    exit;
}


$iThisLoginUser = mysqli_query($con, "SELECT * FROM users WHERE user_id = '" . $_SESSION['user_id'] . "'");
$iThisLogUs = mysqli_fetch_array($iThisLoginUser);

$iThisLoginUserCreditLimit = mysqli_query($con, "SELECT * FROM credit_limit WHERE userid = '" . $_SESSION['user_id'] . "' and type_id	 = '1'");
$iThisLogUsCreditLimit = mysqli_fetch_array($iThisLoginUserCreditLimit);

if ($iThisLogUs['user_status'] == '2') {
    redirect("insufficentfund.php?action=authorized");
    exit;
}


/*    author atikdev
      code for generating message on realtime client credit limit and betting status 
*/

$iThisLoginUserCreditLimit = mysqli_query($con, "SELECT * FROM credit_limit WHERE userid = '" . $_SESSION['user_id'] . "' and type_id = '1'");
$iThisLogUsCreditLimit = mysqli_fetch_array($iThisLoginUserCreditLimit);
	 $iThisIsMaxAmount = $iThisLogUsCreditLimit['maxamount'];
	 $iThisIsMinAmount = $iThisLogUsCreditLimit['minamount'];
/*  
       end of credit limit section
*/


function toAmericanDecimal($number){
    return $number < 2 ?  round((-100) / ($number - 1)) : round(($number - 1) * 100);
}

function RandomStringGenerator($n)
{
    $generated_string = "";
    $domain = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    $len = strlen($domain);
    for ($i = 0; $i < $n; $i++) {
        $index = rand(0, $len - 1);
        $generated_string = $generated_string . $domain[$index];
    }

    return $generated_string;
}


function RandomNumberGenerator($n)
{
    $generated_string = "";
    $domain = "1234567890";
    $len = strlen($domain);
    for ($i = 0; $i < $n; $i++) {
        $index = rand(0, $len - 1);
        $generated_string = $generated_string . $domain[$index];
    }

    return $generated_string;
}

function redirect($url = NULL)
{
    if (is_null($url)) $url = curPageURL();
    if (headers_sent()) {
        echo "<script>window.location='" . $url . "'</script>";
    } else {
        header("Location:" . $url);
    }
    exit;
}


function datasumited($is_away, $is_home, $tokenid, $eventid, $oAnDu, $teamid, $SportName, $AbrNam, $iBetingWin, $BettingCondition, $BettingConditionOrginal, $iRiskAMount, $BettingAmount, $EventDate, $wager, $type, $iTicketNumber)
{
    global $con;

    $iThisLoginUser = mysqli_query($con, "SELECT * FROM users WHERE user_id = '" . $_SESSION['user_id'] . "'");
    $iThisLogUs = mysqli_fetch_array($iThisLoginUser);

    $iThisLoginUserCreditLimit = mysqli_query($con, "SELECT * FROM credit_limit WHERE userid = '" . $_SESSION['user_id'] . "' and type_id	 = '1'");
    $iThisLogUsCreditLimit = mysqli_fetch_array($iThisLoginUserCreditLimit);


    if ($_POST['freeplay'] == '') {
        $iFreePlay = '0';
    } else {
        $iFreePlay = $_POST['freeplay'];
    }


    $q = "INSERT INTO bettingdetail (	
										is_away, 
										is_home, 
										tokenid, 
										o_and_u,
										event_id, 
										team_id, 
										sport_name, 
										abrevation_name, 
										bettingwin, 
										bettingcondition,
										bettingcondition_orginal, 
										riskamount, 
										bettingamount,  
										event_date, 
										wager,
										type,  
										ticketid, 
										userid, 
										create_at,
										status,
										user_current_credit_limit,
										user_current_max_limit,
										user_current_balance,
										result,
										freeplay	) 
	
							VALUES ( 	'" . $is_away . "',
										'" . $is_home . "',
										'" . $tokenid . "',
										'" . $oAnDu . "', 
										'" . $eventid . "', 
										'" . $teamid . "', 
										'" . $SportName . "',  
										'" . $AbrNam . "', 
										'" . abs($iBetingWin) . "', 
										'" . $BettingCondition . "', 
										'" . $BettingConditionOrginal . "', 
										'" . abs($iRiskAMount) . "', 
										'" . $BettingAmount . "', 
										'" . $EventDate . "', 
										'" . $wager . "', 
										'" . $type . "', 
										'" . $iTicketNumber . "', 
										'" . $_SESSION['user_id'] . "',
										'" . date("Y-m-d H:i:s") . "',
										'0',
										'" . $iThisLogUs['credit_limit'] . "',
										'" . $iThisLogUsCreditLimit['maxamount'] . "',
										'" . $iThisLogUs['user_available_balance'] . "',
										'0'	,
										'" . $iFreePlay . "'									
										)";


    mysqli_query($con, $q);


}


function BettingEvent($event_id, $score_away, $score_home, $winner_away, $winner_home, $score_away_by_period, $score_home_by_period, $event_status)
{
    global $con;


    $iResultBetEvent = mysqli_query($con, "SELECT * FROM betting_event WHERE event_id = '" . $event_id . "' and event_status = '" . $event_status . "'");
    $iResBetrow = mysqli_fetch_array($iResultBetEvent);

    if ($iResBetrow['id'] == '') {

        $q = "INSERT INTO betting_event (	event_id, 
										score_away,
										score_home, 
										winner_away, 
										winner_home, 
										score_away_by_period, 
										score_home_by_period,
										event_status ,
										create_at ,
										update_at
										) 
	
							VALUES ( 	'" . $event_id . "',
										'" . $score_away . "', 
										'" . $score_home . "', 
										'" . $winner_away . "', 
										'" . $winner_home . "',  
										'" . $score_away_by_period . "', 
										'" . $score_home_by_period . "', 
										'" . $event_status . "',
										'" . date("Y-m-d H:i:s") . "',
										'" . date("Y-m-d H:i:s") . "'									
										)";


        mysqli_query($con, $q);

    } else {

        $qNe = "UPDATE betting_event set  score_away = '" . $score_away . "',  score_home = '" . $score_home . "',  winner_away = '" . $winner_away . "',  winner_home = '" . $winner_home . "',  score_away_by_period = '" . $score_away_by_period . "',  score_home_by_period = '" . $score_home_by_period . "',  event_status = '" . $event_status . "',  update_at = '" . date("Y-m-d H:i:s") . "'   where event_id = '" . $event_id . "' ";
        mysqli_query($con, $qNe);

    }


}


$settingsmenu = 'active';
$accountmenu = 'active';


$def_tz = ($_SESSION['user_timezone']) ? $_SESSION['user_timezone'] : 'Asia/Tokyo';
$languages = getLanguages();

$file = $basedir . '/temp/all_users.txt';
$data = json_decode(file_get_contents($file), true);
$tokenid = RandomStringGenerator(10);
if (isset($_POST['actionsubmit'])) {

    $tokenid = RandomStringGenerator(10);
    $iTicketNumber = RandomNumberGenerator(10);


    foreach ($_POST['event_id'] as $key => $val) {


        /******----------------------------------------------------RUN LINE******RUN LINE******RUN LINE******RUN--------------------------------------**************/
        /******----------------------------------------------------RUN LINE******RUN LINE******RUN LINE******RUN--------------------------------------**************/
        /******----------------------------------------------------RUN LINE******RUN LINE******RUN LINE******RUN--------------------------------------**************/
        /******----------------------------------------------------RUN LINE******RUN LINE******RUN LINE******RUN--------------------------------------**************/

        if ($_POST['runlinevalue_1_' . $val] != '') {


            BettingEvent($_POST['event_id' . $val], $_POST['score_away' . $val], $_POST['score_home' . $val], $_POST['winner_away' . $val], $_POST['winner_home' . $val], $_POST['score_away_by_period' . $val], $_POST['score_home_by_period' . $val], $_POST['event_status' . $val]);

            if ($_POST['wager'] == '1') {

                if ($_POST['runline_1_' . $val] < 0) {

                    $iDivideBy = 100 / $_POST['runline_1_' . $val];
                    $iGetAmount = ($_POST['runlinevalue_1_' . $val]) * $iDivideBy;

                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['runlinevalue_1_' . $val];
                } else {

                    $iDivideBy = $_POST['runline_1_' . $val] / 100;
                    $iGetAmount = ($_POST['runlinevalue_1_' . $val]) * $iDivideBy;

                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['runlinevalue_1_' . $val];
                }
            } elseif ($_POST['wager'] == '2') {

                if ($_POST['runline_1_' . $val] < 0) {

                    $iDivideBy = $_POST['runline_1_' . $val] / 100;
                    $iGetAmount = ($_POST['runlinevalue_1_' . $val]) * $iDivideBy;

                    $iWinAmount = $_POST['runlinevalue_1_' . $val];
                    $iRiskAMount = $iGetAmount;
                } else {

                    $iDivideBy = 100 / $_POST['runline_1_' . $val];
                    $iGetAmount = ($_POST['runlinevalue_1_' . $val]) * $iDivideBy;

                    $iWinAmount = $_POST['runlinevalue_1_' . $val];
                    $iRiskAMount = $iGetAmount;
                }
            } else {

                $iDivideBy = $_POST['runline_1_' . $val] / 100;
                $iGetAmount = ($_POST['runlinevalue_1_' . $val]) * $iDivideBy;


                if ($_POST['runline_1_' . $val] < 0) {
                    $iWinAmount = $_POST['runlinevalue_1_' . $val];
                    $iRiskAMount = $iGetAmount;
                } else {
                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['runlinevalue_1_' . $val];
                }
            }


///// datasumited($tokenid, $eventid, $teamid, $SportName, $AbrNam, $iBetingWin, $BettingCondition, $BettingConditionOrginal,  $iRiskAMount, $BettingAmount, $EventDate, $wager, $type, $iTicketNumber)


            datasumited($_POST['is_away_1_' . $val], $_POST['is_home_1_' . $val], $tokenid, $val, $_POST['o_and_u_1_' . $val], $_POST['team_id_1_' . $val], $_POST['sport_name_1_' . $val], $_POST['abrevation_name_1_' . $val], abs($iWinAmount), $_POST['runline_1_' . $val], $_POST['runline_orginal_1_' . $val], abs($iRiskAMount), $_POST['runlinevalue_1_' . $val], $_POST['event_date_' . $val], $_POST['wager'], '1', $iTicketNumber);


        }

        if ($_POST['runlinevalue_2_' . $val] != '') {


            BettingEvent($_POST['event_id' . $val], $_POST['score_away' . $val], $_POST['score_home' . $val], $_POST['winner_away' . $val], $_POST['winner_home' . $val], $_POST['score_away_by_period' . $val], $_POST['score_home_by_period' . $val], $_POST['event_status' . $val]);

            if ($_POST['wager'] == '1') {

                if ($_POST['runline_2_' . $val] < 0) {

                    $iDivideBy = 100 / $_POST['runline_2_' . $val];
                    $iGetAmount = ($_POST['runlinevalue_2_' . $val]) * $iDivideBy;

                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['runlinevalue_2_' . $val];
                } else {

                    $iDivideBy = $_POST['runline_2_' . $val] / 100;
                    $iGetAmount = ($_POST['runlinevalue_2_' . $val]) * $iDivideBy;

                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['runlinevalue_2_' . $val];
                }
            } elseif ($_POST['wager'] == '2') {

                if ($_POST['runline_2_' . $val] < 0) {

                    $iDivideBy = $_POST['runline_2_' . $val] / 100;
                    $iGetAmount = ($_POST['runlinevalue_2_' . $val]) * $iDivideBy;
                    $iWinAmount = $_POST['runlinevalue_2_' . $val];
                    $iRiskAMount = $iGetAmount;
                } else {

                    $iDivideBy = 100 / $_POST['runline_2_' . $val];
                    $iGetAmount = ($_POST['runlinevalue_2_' . $val]) * $iDivideBy;
                    $iWinAmount = $_POST['runlinevalue_2_' . $val];
                    $iRiskAMount = $iGetAmount;
                }
            } else {

                $iDivideBy = $_POST['runline_2_' . $val] / 100;
                $iGetAmount = ($_POST['runlinevalue_2_' . $val]) * $iDivideBy;

                if ($_POST['runline_2_' . $val] < 0) {
                    $iWinAmount = $_POST['runlinevalue_2_' . $val];
                    $iRiskAMount = $iGetAmount;
                } else {
                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['runlinevalue_2_' . $val];
                }
            }


///// datasumited($tokenid, $eventid, $teamid, $SportName, $AbrNam, $iBetingWin, $BettingCondition, $BettingConditionOrginal,  $iRiskAMount, $BettingAmount, $EventDate, $wager, $type, $iTicketNumber)

            datasumited($_POST['is_away_2_' . $val], $_POST['is_home_2_' . $val], $tokenid, $val, $_POST['o_and_u_2_' . $val], $_POST['team_id_2_' . $val], $_POST['sport_name_2_' . $val], $_POST['abrevation_name_2_' . $val], abs($iWinAmount), $_POST['runline_2_' . $val], $_POST['runline_orginal_2_' . $val], abs($iRiskAMount), $_POST['runlinevalue_2_' . $val], $_POST['event_date_' . $val], $_POST['wager'], '1', $iTicketNumber);


        }

        /******------------------------------------------SPREAD******SPREAD*******SPREAD---------------------------**************/
        /******------------------------------------------SPREAD******SPREAD*******SPREAD---------------------------**************/
        /******------------------------------------------SPREAD******SPREAD*******SPREAD---------------------------**************/
        /******------------------------------------------SPREAD******SPREAD*******SPREAD---------------------------**************/


        if ($_POST['spreadvalue_1_' . $val] != '') {


            BettingEvent($_POST['event_id' . $val], $_POST['score_away' . $val], $_POST['score_home' . $val], $_POST['winner_away' . $val], $_POST['winner_home' . $val], $_POST['score_away_by_period' . $val], $_POST['score_home_by_period' . $val], $_POST['event_status' . $val]);

            if ($_POST['wager'] == '1') {

                if ($_POST['spread_1_' . $val] < 0) {

                    $iDivideBy = 100 / $_POST['spread_1_' . $val];
                    $iGetAmount = ($_POST['spreadvalue_1_' . $val]) * $iDivideBy;

                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['spreadvalue_1_' . $val];
                } else {

                    $iDivideBy = $_POST['spread_1_' . $val] / 100;
                    $iGetAmount = ($_POST['spreadvalue_1_' . $val]) * $iDivideBy;

                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['spreadvalue_1_' . $val];
                }
            } elseif ($_POST['wager'] == '2') {

                if ($_POST['spread_1_' . $val] < 0) {

                    $iDivideBy = $_POST['spread_1_' . $val] / 100;
                    $iGetAmount = ($_POST['spreadvalue_1_' . $val]) * $iDivideBy;

                    $iWinAmount = $_POST['spreadvalue_1_' . $val];
                    $iRiskAMount = $iGetAmount;
                } else {

                    $iDivideBy = 100 / $_POST['spread_1_' . $val];
                    $iGetAmount = ($_POST['spreadvalue_1_' . $val]) * $iDivideBy;

                    $iWinAmount = $_POST['spreadvalue_1_' . $val];
                    $iRiskAMount = $iGetAmount;
                }
            } else {

                $iDivideBy = $_POST['spread_1_' . $val] / 100;
                $iGetAmount = ($_POST['spreadvalue_1_' . $val]) * $iDivideBy;


                if ($_POST['spread_1_' . $val] < 0) {
                    $iWinAmount = $_POST['spreadvalue_1_' . $val];
                    $iRiskAMount = $iGetAmount;
                } else {
                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['spreadvalue_1_' . $val];
                }
            }


///// datasumited($tokenid, $eventid, $teamid, $SportName, $AbrNam, $iBetingWin, $BettingCondition, $BettingConditionOrginal,  $iRiskAMount, $BettingAmount, $EventDate, $wager, $type, $iTicketNumber)


            datasumited($_POST['is_away_1_' . $val], $_POST['is_home_1_' . $val], $tokenid, $val, $_POST['o_and_u_1_' . $val], $_POST['team_id_1_' . $val], $_POST['sport_name_1_' . $val], $_POST['abrevation_name_1_' . $val], abs($iWinAmount), $_POST['runline_1_' . $val], $_POST['runline_orginal_1_' . $val], abs($iRiskAMount), $_POST['runlinevalue_1_' . $val], $_POST['event_date_' . $val], $_POST['wager'], '1', $iTicketNumber);


        }

        if ($_POST['spreadvalue_2_' . $val] != '') {


            BettingEvent($_POST['event_id' . $val], $_POST['score_away' . $val], $_POST['score_home' . $val], $_POST['winner_away' . $val], $_POST['winner_home' . $val], $_POST['score_away_by_period' . $val], $_POST['score_home_by_period' . $val], $_POST['event_status' . $val]);

            if ($_POST['wager'] == '1') {

                if ($_POST['spread_2_' . $val] < 0) {

                    $iDivideBy = 100 / $_POST['spread_2_' . $val];
                    $iGetAmount = ($_POST['spreadvalue_2_' . $val]) * $iDivideBy;

                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['spreadvalue_2_' . $val];
                } else {

                    $iDivideBy = $_POST['spread_2_' . $val] / 100;
                    $iGetAmount = ($_POST['spreadvalue_2_' . $val]) * $iDivideBy;

                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['spreadvalue_2_' . $val];
                }
            } elseif ($_POST['wager'] == '2') {

                if ($_POST['spread_2_' . $val] < 0) {

                    $iDivideBy = $_POST['spread_2_' . $val] / 100;
                    $iGetAmount = ($_POST['spreadvalue_2_' . $val]) * $iDivideBy;
                    $iWinAmount = $_POST['spreadvalue_2_' . $val];
                    $iRiskAMount = $iGetAmount;
                } else {

                    $iDivideBy = 100 / $_POST['spread_2_' . $val];
                    $iGetAmount = ($_POST['spreadvalue_2_' . $val]) * $iDivideBy;
                    $iWinAmount = $_POST['spreadvalue_2_' . $val];
                    $iRiskAMount = $iGetAmount;
                }
            } else {

                $iDivideBy = $_POST['spread_2_' . $val] / 100;
                $iGetAmount = ($_POST['spreadvalue_2_' . $val]) * $iDivideBy;

                if ($_POST['spread_2_' . $val] < 0) {
                    $iWinAmount = $_POST['spreadvalue_2_' . $val];
                    $iRiskAMount = $iGetAmount;
                } else {
                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['spreadvalue_2_' . $val];
                }
            }


///// datasumited($tokenid, $eventid, $teamid, $SportName, $AbrNam, $iBetingWin, $BettingCondition, $BettingConditionOrginal,  $iRiskAMount, $BettingAmount, $EventDate, $wager, $type, $iTicketNumber)

            datasumited($_POST['is_away_2_' . $val], $_POST['is_home_2_' . $val], $tokenid, $val, $_POST['o_and_u_2_' . $val], $_POST['team_id_2_' . $val], $_POST['sport_name_2_' . $val], $_POST['abrevation_name_2_' . $val], abs($iWinAmount), $_POST['runline_2_' . $val], $_POST['runline_orginal_2_' . $val], abs($iRiskAMount), $_POST['runlinevalue_2_' . $val], $_POST['event_date_' . $val], $_POST['wager'], '1', $iTicketNumber);


        }

        /******------------------------------------------SOCCER SPREAD******SOCCER SPREAD*******SOCCER SPREAD---------------------------**************/
        /******------------------------------------------SOCCER SPREAD******SOCCER SPREAD*******SOCCER SPREAD---------------------------**************/
        /******------------------------------------------SOCCER SPREAD******SOCCER SPREAD*******SOCCER SPREAD---------------------------**************/
        /******------------------------------------------SOCCER SPREAD******SOCCER SPREAD*******SOCCER SPREAD---------------------------**************/


        if ($_POST['soccerspreadvalue_1_' . $val] != '') {


            BettingEvent($_POST['event_id' . $val], $_POST['score_away' . $val], $_POST['score_home' . $val], $_POST['winner_away' . $val], $_POST['winner_home' . $val], $_POST['score_away_by_period' . $val], $_POST['score_home_by_period' . $val], $_POST['event_status' . $val]);

            if ($_POST['wager'] == '1') {

                if ($_POST['soccerspread_1_' . $val] < 0) {

                    $iDivideBy = 100 / $_POST['soccerspread_1_' . $val];
                    $iGetAmount = ($_POST['soccerspreadvalue_1_' . $val]) * $iDivideBy;

                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['soccerspreadvalue_1_' . $val];
                } else {

                    $iDivideBy = $_POST['soccerspread_1_' . $val] / 100;
                    $iGetAmount = ($_POST['soccerspreadvalue_1_' . $val]) * $iDivideBy;

                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['soccerspreadvalue_1_' . $val];
                }
            } elseif ($_POST['wager'] == '2') {

                if ($_POST['soccerspread_1_' . $val] < 0) {

                    $iDivideBy = $_POST['soccerspread_1_' . $val] / 100;
                    $iGetAmount = ($_POST['soccerspreadvalue_1_' . $val]) * $iDivideBy;

                    $iWinAmount = $_POST['soccerspreadvalue_1_' . $val];
                    $iRiskAMount = $iGetAmount;
                } else {

                    $iDivideBy = 100 / $_POST['soccerspread_1_' . $val];
                    $iGetAmount = ($_POST['soccerspreadvalue_1_' . $val]) * $iDivideBy;

                    $iWinAmount = $_POST['soccerspreadvalue_1_' . $val];
                    $iRiskAMount = $iGetAmount;
                }
            } else {

                $iDivideBy = $_POST['soccerspread_1_' . $val] / 100;
                $iGetAmount = ($_POST['soccerspreadvalue_1_' . $val]) * $iDivideBy;


                if ($_POST['soccerspread_1_' . $val] < 0) {
                    $iWinAmount = $_POST['soccerspreadvalue_1_' . $val];
                    $iRiskAMount = $iGetAmount;
                } else {
                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['soccerspreadvalue_1_' . $val];
                }
            }


///// datasumited($tokenid, $eventid, $teamid, $SportName, $AbrNam, $iBetingWin, $BettingCondition, $BettingConditionOrginal,  $iRiskAMount, $BettingAmount, $EventDate, $wager, $type, $iTicketNumber)


            datasumited($_POST['is_away_1_' . $val], $_POST['is_home_1_' . $val], $tokenid, $val, $_POST['o_and_u_1_' . $val], $_POST['team_id_1_' . $val], $_POST['sport_name_1_' . $val], $_POST['abrevation_name_1_' . $val], abs($iWinAmount), $_POST['runline_1_' . $val], $_POST['runline_orginal_1_' . $val], abs($iRiskAMount), $_POST['runlinevalue_1_' . $val], $_POST['event_date_' . $val], $_POST['wager'], '1', $iTicketNumber);


        }

        if ($_POST['soccerspreadvalue_2_' . $val] != '') {


            BettingEvent($_POST['event_id' . $val], $_POST['score_away' . $val], $_POST['score_home' . $val], $_POST['winner_away' . $val], $_POST['winner_home' . $val], $_POST['score_away_by_period' . $val], $_POST['score_home_by_period' . $val], $_POST['event_status' . $val]);

            if ($_POST['wager'] == '1') {

                if ($_POST['soccerspread_2_' . $val] < 0) {

                    $iDivideBy = 100 / $_POST['soccerspread_2_' . $val];
                    $iGetAmount = ($_POST['soccerspreadvalue_2_' . $val]) * $iDivideBy;

                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['soccerspreadvalue_2_' . $val];
                } else {

                    $iDivideBy = $_POST['soccerspread_2_' . $val] / 100;
                    $iGetAmount = ($_POST['soccerspreadvalue_2_' . $val]) * $iDivideBy;

                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['soccerspreadvalue_2_' . $val];
                }
            } elseif ($_POST['wager'] == '2') {

                if ($_POST['soccerspread_2_' . $val] < 0) {

                    $iDivideBy = $_POST['soccerspread_2_' . $val] / 100;
                    $iGetAmount = ($_POST['soccerspreadvalue_2_' . $val]) * $iDivideBy;
                    $iWinAmount = $_POST['soccerspreadvalue_2_' . $val];
                    $iRiskAMount = $iGetAmount;
                } else {

                    $iDivideBy = 100 / $_POST['soccerspread_2_' . $val];
                    $iGetAmount = ($_POST['soccerspreadvalue_2_' . $val]) * $iDivideBy;
                    $iWinAmount = $_POST['soccerspreadvalue_2_' . $val];
                    $iRiskAMount = $iGetAmount;
                }
            } else {

                $iDivideBy = $_POST['soccerspread_2_' . $val] / 100;
                $iGetAmount = ($_POST['soccerspreadvalue_2_' . $val]) * $iDivideBy;

                if ($_POST['soccerspread_2_' . $val] < 0) {
                    $iWinAmount = $_POST['soccerspreadvalue_2_' . $val];
                    $iRiskAMount = $iGetAmount;
                } else {
                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['soccerspreadvalue_2_' . $val];
                }
            }


///// datasumited($tokenid, $eventid, $teamid, $SportName, $AbrNam, $iBetingWin, $BettingCondition, $BettingConditionOrginal,  $iRiskAMount, $BettingAmount, $EventDate, $wager, $type, $iTicketNumber)

            datasumited($_POST['is_away_2_' . $val], $_POST['is_home_2_' . $val], $tokenid, $val, $_POST['o_and_u_2_' . $val], $_POST['team_id_2_' . $val], $_POST['sport_name_2_' . $val], $_POST['abrevation_name_2_' . $val], abs($iWinAmount), $_POST['runline_2_' . $val], $_POST['runline_orginal_2_' . $val], abs($iRiskAMount), $_POST['runlinevalue_2_' . $val], $_POST['event_date_' . $val], $_POST['wager'], '1', $iTicketNumber);


        }


        /******------------------------------------------MONEY LINE******MONEY LINE*******MONEY LINE---------------------------**************/
        /******------------------------------------------MONEY LINE******MONEY LINE*******MONEY LINE---------------------------**************/
        /******------------------------------------------MONEY LINE******MONEY LINE*******MONEY LINE---------------------------**************/
        /******------------------------------------------MONEY LINE******MONEY LINE*******MONEY LINE---------------------------**************/


        if ($_POST['moneylinevalue_1_' . $val] != '') {

            BettingEvent($_POST['event_id' . $val], $_POST['score_away' . $val], $_POST['score_home' . $val], $_POST['winner_away' . $val], $_POST['winner_home' . $val], $_POST['score_away_by_period' . $val], $_POST['score_home_by_period' . $val], $_POST['event_status' . $val]);

            if ($_POST['wager'] == '1') {

                if ($_POST['moneyline_1_' . $val] < 0) {

                    $iDivideBy = 100 / $_POST['moneyline_1_' . $val];
                    $iGetAmount = ($_POST['moneylinevalue_1_' . $val]) * $iDivideBy;


                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['moneylinevalue_1_' . $val];
                } else {

                    $iDivideBy = $_POST['moneyline_1_' . $val] / 100;
                    $iGetAmount = ($_POST['moneylinevalue_1_' . $val]) * $iDivideBy;

                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['moneylinevalue_1_' . $val];
                }

            } elseif ($_POST['wager'] == '2') {

                if ($_POST['moneyline_1_' . $val] < 0) {

                    $iDivideBy = $_POST['moneyline_1_' . $val] / 100;
                    $iGetAmount = ($_POST['moneylinevalue_1_' . $val]) * $iDivideBy;
                    $iWinAmount = $_POST['moneylinevalue_1_' . $val];
                    $iRiskAMount = $iGetAmount;
                } else {
                    $iDivideBy = 100 / $_POST['moneyline_1_' . $val];
                    $iGetAmount = ($_POST['moneylinevalue_1_' . $val]) * $iDivideBy;
                    $iWinAmount = $_POST['moneylinevalue_1_' . $val];
                    $iRiskAMount = $iGetAmount;
                }

            } else {
                $iDivideBy = $_POST['moneyline_1_' . $val] / 100;
                $iGetAmount = ($_POST['moneylinevalue_1_' . $val]) * $iDivideBy;
                if ($_POST['moneyline_1_' . $val] < 0) {
                    $iWinAmount = $_POST['moneylinevalue_1_' . $val];
                    $iRiskAMount = $iGetAmount;
                } else {
                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['moneylinevalue_1_' . $val];
                }
            }
///// datasumited($tokenid, $eventid, $teamid, $SportName, $AbrNam, $iBetingWin, $BettingCondition, $BettingConditionOrginal,  $iRiskAMount, $BettingAmount, $EventDate, $wager, $type, $iTicketNumber)

            datasumited($_POST['is_away_1_' . $val], $_POST['is_home_1_' . $val], $tokenid, $val, $_POST['o_and_u_1_' . $val], $_POST['team_id_1_' . $val], $_POST['sport_name_1_' . $val], $_POST['abrevation_name_1_' . $val], abs($iWinAmount), $_POST['moneyline_1_' . $val], $_POST['moneyline_orginal_1_' . $val], abs($iRiskAMount), $_POST['moneylinevalue_1_' . $val], $_POST['event_date_' . $val], $_POST['wager'], '2', $iTicketNumber);


        }

        if ($_POST['moneylinevalue_2_' . $val] != '') {

            BettingEvent($_POST['event_id' . $val], $_POST['score_away' . $val], $_POST['score_home' . $val], $_POST['winner_away' . $val], $_POST['winner_home' . $val], $_POST['score_away_by_period' . $val], $_POST['score_home_by_period' . $val], $_POST['event_status' . $val]);


            if ($_POST['wager'] == '1') {

                if ($_POST['moneyline_2_' . $val] < 0) {

                    $iDivideBy = 100 / $_POST['moneyline_2_' . $val];
                    $iGetAmount = ($_POST['moneylinevalue_2_' . $val]) * $iDivideBy;


                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['moneylinevalue_2_' . $val];
                } else {

                    $iDivideBy = $_POST['moneyline_2_' . $val] / 100;
                    $iGetAmount = ($_POST['moneylinevalue_2_' . $val]) * $iDivideBy;

                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['moneylinevalue_2_' . $val];
                }

            } elseif ($_POST['wager'] == '2') {

                if ($_POST['moneyline_2_' . $val] < 0) {

                    $iDivideBy = $_POST['moneyline_2_' . $val] / 100;
                    $iGetAmount = ($_POST['moneylinevalue_2_' . $val]) * $iDivideBy;
                    $iWinAmount = $_POST['moneylinevalue_2_' . $val];
                    $iRiskAMount = $iGetAmount;
                } else {
                    $iDivideBy = 100 / $_POST['moneyline_2_' . $val];
                    $iGetAmount = ($_POST['moneylinevalue_2_' . $val]) * $iDivideBy;
                    $iWinAmount = $_POST['moneylinevalue_2_' . $val];
                    $iRiskAMount = $iGetAmount;
                }

            } else {
                $iDivideBy = $_POST['moneyline_2_' . $val] / 100;
                $iGetAmount = ($_POST['moneylinevalue_2_' . $val]) * $iDivideBy;
                if ($_POST['moneyline_2_' . $val] < 0) {

                    $iWinAmount = $_POST['moneylinevalue_2_' . $val];
                    $iRiskAMount = $iGetAmount;
                } else {

                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['moneylinevalue_2_' . $val];
                }
            }
///// datasumited($tokenid, $eventid, $teamid, $SportName, $AbrNam, $iBetingWin, $BettingCondition, $BettingConditionOrginal,  $iRiskAMount, $BettingAmount, $EventDate, $wager, $type, $iTicketNumber)

            datasumited($_POST['is_away_2_' . $val], $_POST['is_home_2_' . $val], $tokenid, $val, $_POST['o_and_u_2_' . $val], $_POST['team_id_2_' . $val], $_POST['sport_name_2_' . $val], $_POST['abrevation_name_2_' . $val], abs($iWinAmount), $_POST['moneyline_2_' . $val], $_POST['moneyline_orginal_2_' . $val], abs($iRiskAMount), $_POST['moneylinevalue_2_' . $val], $_POST['event_date_' . $val], $_POST['wager'], '2', $iTicketNumber);


        }

        /******------------------------------------------MONEY LINE2******MONEY LINE2*******MONEY LINE2---------------------------**************/
        /******------------------------------------------MONEY LINE2******MONEY LINE2*******MONEY LINE2---------------------------**************/
        /******------------------------------------------MONEY LINE2******MONEY LINE2*******MONEY LINE2---------------------------**************/
        /******------------------------------------------MONEY LINE2******MONEY LINE2*******MONEY LINE2---------------------------**************/


        if ($_POST['moneyline2value_1_' . $val] != '') {

            BettingEvent($_POST['event_id' . $val], $_POST['score_away' . $val], $_POST['score_home' . $val], $_POST['winner_away' . $val], $_POST['winner_home' . $val], $_POST['score_away_by_period' . $val], $_POST['score_home_by_period' . $val], $_POST['event_status' . $val]);

            if ($_POST['wager'] == '1') {

                if ($_POST['moneyline2_1_' . $val] < 0) {

                    $iDivideBy = 100 / $_POST['moneyline2_1_' . $val];
                    $iGetAmount = ($_POST['moneyline2value_1_' . $val]) * $iDivideBy;


                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['moneyline2value_1_' . $val];
                } else {

                    $iDivideBy = $_POST['moneyline2_1_' . $val] / 100;
                    $iGetAmount = ($_POST['moneyline2value_1_' . $val]) * $iDivideBy;

                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['moneyline2value_1_' . $val];
                }

            } elseif ($_POST['wager'] == '2') {

                if ($_POST['moneyline2_1_'. $val] < 0) {

                    $iDivideBy = $_POST['moneyline2_1_'. $val] / 100;
                    $iGetAmount = ($_POST['moneyline2value_1_'. $val]) * $iDivideBy;
                    $iWinAmount = $_POST['moneyline2value_1_'. $val];
                    $iRiskAMount = $iGetAmount;
                } else {
                    $iDivideBy = 100 / $_POST['moneyline2_1_'. $val];
                    $iGetAmount = ($_POST['moneyline2value_1_'. $val]) * $iDivideBy;
                    $iWinAmount = $_POST['moneyline2value_1_'. $val];
                    $iRiskAMount = $iGetAmount;
                }

            } else {
                $iDivideBy = $_POST['moneyline2_1_' . $val] / 100;
                $iGetAmount = ($_POST['moneyline2value_1_' . $val]) * $iDivideBy;
                if ($_POST['moneyline2_1_' . $val] < 0) {
                    $iWinAmount = $_POST['moneyline2value_1_' . $val];
                    $iRiskAMount = $iGetAmount;
                } else {
                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['moneyline2value_1_' . $val];
                }
            }
///// datasumited($tokenid, $eventid, $teamid, $SportName, $AbrNam, $iBetingWin, $BettingCondition, $BettingConditionOrginal,  $iRiskAMount, $BettingAmount, $EventDate, $wager, $type, $iTicketNumber)

            datasumited($_POST['is_away_1_' . $val], $_POST['is_home_1_' . $val], $tokenid, $val, $_POST['o_and_u_1_' . $val], $_POST['team_id_1_' . $val], $_POST['sport_name_1_' . $val], $_POST['abrevation_name_1_' . $val], abs($iWinAmount), $_POST['moneyline_1_' . $val], $_POST['moneyline_orginal_1_' . $val], abs($iRiskAMount), $_POST['moneylinevalue_1_' . $val], $_POST['event_date_' . $val], $_POST['wager'], '2', $iTicketNumber);


        }

        if ($_POST['moneyline2value_2_' . $val] != '') {

            BettingEvent($_POST['event_id' . $val], $_POST['score_away' . $val], $_POST['score_home' . $val], $_POST['winner_away' . $val], $_POST['winner_home' . $val], $_POST['score_away_by_period' . $val], $_POST['score_home_by_period' . $val], $_POST['event_status' . $val]);


            if ($_POST['wager'] == '1') {

                if ($_POST['moneyline2_2_' . $val] < 0) {

                    $iDivideBy = 100 / $_POST['moneyline2_2_' . $val];
                    $iGetAmount = ($_POST['moneyline2value_2_' . $val]) * $iDivideBy;


                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['moneyline2value_2_' . $val];
                } else {

                    $iDivideBy = $_POST['moneyline2_2_' . $val] / 100;
                    $iGetAmount = ($_POST['moneyline2value_2_' . $val]) * $iDivideBy;

                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['moneyline2value_2_' . $val];
                }

            } elseif ($_POST['wager'] == '2') {

                if ($_POST['moneyline2_2_' . $val] < 0) {

                    $iDivideBy = $_POST['moneyline2_2_' . $val] / 100;
                    $iGetAmount = ($_POST['moneyline2value_2_' . $val]) * $iDivideBy;
                    $iWinAmount = $_POST['moneyline2value_2_' . $val];
                    $iRiskAMount = $iGetAmount;
                } else {
                    $iDivideBy = 100 / $_POST['moneyline2_2_' . $val];
                    $iGetAmount = ($_POST['moneyline2value_2_' . $val]) * $iDivideBy;
                    $iWinAmount = $_POST['moneyline2value_2_' . $val];
                    $iRiskAMount = $iGetAmount;
                }

            } else {
                $iDivideBy = $_POST['moneyline2_2_' . $val] / 100;
                $iGetAmount = ($_POST['moneyline2value_2_' . $val]) * $iDivideBy;
                if ($_POST['moneyline2_2_' . $val] < 0) {

                    $iWinAmount = $_POST['moneyline2value_2_' . $val];
                    $iRiskAMount = $iGetAmount;
                } else {

                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['moneyline2value_2_' . $val];
                }
            }
///// datasumited($tokenid, $eventid, $teamid, $SportName, $AbrNam, $iBetingWin, $BettingCondition, $BettingConditionOrginal,  $iRiskAMount, $BettingAmount, $EventDate, $wager, $type, $iTicketNumber)

            datasumited($_POST['is_away_2_' . $val], $_POST['is_home_2_' . $val], $tokenid, $val, $_POST['o_and_u_2_' . $val], $_POST['team_id_2_' . $val], $_POST['sport_name_2_' . $val], $_POST['abrevation_name_2_' . $val], abs($iWinAmount), $_POST['moneyline_2_' . $val], $_POST['moneyline_orginal_2_' . $val], abs($iRiskAMount), $_POST['moneylinevalue_2_' . $val], $_POST['event_date_' . $val], $_POST['wager'], '2', $iTicketNumber);


        }

        /******------------------------------------------SOCCER MONEY LINE******SOCCER MONEY LINE*******SOCCER MONEY LINE---------------------------**************/
        /******------------------------------------------SOCCER MONEY LINE******SOCCER MONEY LINE*******SOCCER MONEY LINE---------------------------**************/
        /******------------------------------------------SOCCER MONEY LINE******SOCCER MONEY LINE*******SOCCER MONEY LINE---------------------------**************/
        /******------------------------------------------SOCCER MONEY LINE******SOCCER MONEY LINE*******SOCCER MONEY LINE---------------------------**************/


        if ($_POST['soccermoneylinevalue_1_' . $val] != '') {

            BettingEvent($_POST['event_id' . $val], $_POST['score_away' . $val], $_POST['score_home' . $val], $_POST['winner_away' . $val], $_POST['winner_home' . $val], $_POST['score_away_by_period' . $val], $_POST['score_home_by_period' . $val], $_POST['event_status' . $val]);

            if ($_POST['wager'] == '1') {

                if ($_POST['soccermoneyline_1_' . $val] < 0) {

                    $iDivideBy = 100 / $_POST['soccermoneyline_1_' . $val];
                    $iGetAmount = ($_POST['soccermoneylinevalue_1_' . $val]) * $iDivideBy;


                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['soccermoneylinevalue_1_' . $val];
                } else {

                    $iDivideBy = $_POST['soccermoneyline_1_' . $val] / 100;
                    $iGetAmount = ($_POST['soccermoneylinevalue_1_' . $val]) * $iDivideBy;

                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['soccermoneylinevalue_1_' . $val];
                }

            } elseif ($_POST['wager'] == '2') {

                if ($_POST['soccermoneyline_1_' . $val] < 0) {

                    $iDivideBy = $_POST['soccermoneyline_1_' . $val] / 100;
                    $iGetAmount = ($_POST['soccermoneylinevalue_1_' . $val]) * $iDivideBy;
                    $iWinAmount = $_POST['soccermoneylinevalue_1_' . $val];
                    $iRiskAMount = $iGetAmount;
                } else {
                    $iDivideBy = 100 / $_POST['soccermoneyline_1_' . $val];
                    $iGetAmount = ($_POST['soccermoneylinevalue_1_' . $val]) * $iDivideBy;
                    $iWinAmount = $_POST['soccermoneylinevalue_1_' . $val];
                    $iRiskAMount = $iGetAmount;
                }

            } else {
                $iDivideBy = $_POST['soccermoneyline_1_' . $val] / 100;
                $iGetAmount = ($_POST['soccermoneylinevalue_1_' . $val]) * $iDivideBy;
                if ($_POST['soccermoneyline_1_' . $val] < 0) {
                    $iWinAmount = $_POST['soccermoneylinevalue_1_' . $val];
                    $iRiskAMount = $iGetAmount;
                } else {
                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['soccermoneylinevalue_1_' . $val];
                }
            }
///// datasumited($tokenid, $eventid, $teamid, $SportName, $AbrNam, $iBetingWin, $BettingCondition, $BettingConditionOrginal,  $iRiskAMount, $BettingAmount, $EventDate, $wager, $type, $iTicketNumber)

            datasumited($_POST['is_away_1_' . $val], $_POST['is_home_1_' . $val], $tokenid, $val, $_POST['o_and_u_1_' . $val], $_POST['team_id_1_' . $val], $_POST['sport_name_1_' . $val], $_POST['abrevation_name_1_' . $val], abs($iWinAmount), $_POST['moneyline_1_' . $val], $_POST['moneyline_orginal_1_' . $val], abs($iRiskAMount), $_POST['moneylinevalue_1_' . $val], $_POST['event_date_' . $val], $_POST['wager'], '2', $iTicketNumber);


        }

        if ($_POST['soccermoneylinevalue_2_' . $val] != '') {

            BettingEvent($_POST['event_id' . $val], $_POST['score_away' . $val], $_POST['score_home' . $val], $_POST['winner_away' . $val], $_POST['winner_home' . $val], $_POST['score_away_by_period' . $val], $_POST['score_home_by_period' . $val], $_POST['event_status' . $val]);


            if ($_POST['wager'] == '1') {

                if ($_POST['soccermoneyline_2_' . $val] < 0) {

                    $iDivideBy = 100 / $_POST['soccermoneyline_2_' . $val];
                    $iGetAmount = ($_POST['soccermoneylinevalue_2_' . $val]) * $iDivideBy;


                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['soccermoneylinevalue_2_' . $val];
                } else {

                    $iDivideBy = $_POST['soccermoneyline_2_' . $val] / 100;
                    $iGetAmount = ($_POST['soccermoneylinevalue_2_' . $val]) * $iDivideBy;

                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['soccermoneylinevalue_2_' . $val];
                }

            } elseif ($_POST['wager'] == '2') {

                if ($_POST['soccermoneyline_2_' . $val] < 0) {

                    $iDivideBy = $_POST['soccermoneyline_2_' . $val] / 100;
                    $iGetAmount = ($_POST['soccermoneylinevalue_2_' . $val]) * $iDivideBy;
                    $iWinAmount = $_POST['soccermoneylinevalue_2_' . $val];
                    $iRiskAMount = $iGetAmount;
                } else {
                    $iDivideBy = 100 / $_POST['soccermoneyline_2_' . $val];
                    $iGetAmount = ($_POST['soccermoneylinevalue_2_' . $val]) * $iDivideBy;
                    $iWinAmount = $_POST['soccermoneylinevalue_2_' . $val];
                    $iRiskAMount = $iGetAmount;
                }

            } else {
                $iDivideBy = $_POST['soccermoneyline_2_' . $val] / 100;
                $iGetAmount = ($_POST['soccermoneylinevalue_2_' . $val]) * $iDivideBy;
                if ($_POST['soccermoneyline_2_' . $val] < 0) {

                    $iWinAmount = $_POST['soccermoneylinevalue_2_' . $val];
                    $iRiskAMount = $iGetAmount;
                } else {

                    $iWinAmount = $iGetAmount;
                    $iRiskAMount = $_POST['soccermoneylinevalue_2_' . $val];
                }
            }
///// datasumited($tokenid, $eventid, $teamid, $SportName, $AbrNam, $iBetingWin, $BettingCondition, $BettingConditionOrginal,  $iRiskAMount, $BettingAmount, $EventDate, $wager, $type, $iTicketNumber)

            datasumited($_POST['is_away_2_' . $val], $_POST['is_home_2_' . $val], $tokenid, $val, $_POST['o_and_u_2_' . $val], $_POST['team_id_2_' . $val], $_POST['sport_name_2_' . $val], $_POST['abrevation_name_2_' . $val], abs($iWinAmount), $_POST['moneyline_2_' . $val], $_POST['moneyline_orginal_2_' . $val], abs($iRiskAMount), $_POST['moneylinevalue_2_' . $val], $_POST['event_date_' . $val], $_POST['wager'], '2', $iTicketNumber);


        }

        /******------------------------------------------TOTAL RUNS******TOTAL RUNS*******TOTAL RUNS---------------------------**************/
        /******------------------------------------------TOTAL RUNS******TOTAL RUNS*******TOTAL RUNS---------------------------**************/
        /******------------------------------------------TOTAL RUNS******TOTAL RUNS*******TOTAL RUNS---------------------------**************/
        /******------------------------------------------TOTAL RUNS******TOTAL RUNS*******TOTAL RUNS---------------------------**************/


        if ($_POST['totalrunsvalue_1_' . $val] != '') {

            BettingEvent($_POST['event_id' . $val], $_POST['score_away' . $val], $_POST['score_home' . $val], $_POST['winner_away' . $val], $_POST['winner_home' . $val], $_POST['score_away_by_period' . $val], $_POST['score_home_by_period' . $val], $_POST['event_status' . $val]);


            if ($_POST['wager'] == '1') {

                $iDivideBy = 100 / $_POST['totalruns_1_' . $val];
                $iGetAmount = ($_POST['totalrunsvalue_1_' . $val]) * $iDivideBy;

                $iWinAmount = $iGetAmount;
                $iRiskAMount = $_POST['totalrunsvalue_1_' . $val];


            } elseif ($_POST['wager'] == '2') {

                $iDivideBy = 100 / $_POST['totalruns_1_' . $val];
                $iGetAmount = ($_POST['totalrunsvalue_1_' . $val]) * $iDivideBy;

                $iWinAmount = $_POST['totalrunsvalue_1_' . $val];
                $iRiskAMount = $iGetAmount;


            } else {

                $iDivideBy = $_POST['totalruns_1_' . $val] / 100;
                $iGetAmount = ($_POST['totalrunsvalue_1_' . $val]) * $iDivideBy;

                $iWinAmount = $_POST['totalrunsvalue_1_' . $val];
                $iRiskAMount = $iGetAmount;

            }

///// datasumited($tokenid, $eventid, $teamid, $SportName, $AbrNam, $iBetingWin, $BettingCondition, $BettingConditionOrginal,  $iRiskAMount, $BettingAmount, $EventDate, $wager, $type, $iTicketNumber)

            datasumited($_POST['is_away_1_' . $val], $_POST['is_home_1_' . $val], $tokenid, $val, $_POST['o_and_u_1_' . $val], $_POST['team_id_1_' . $val], $_POST['sport_name_1_' . $val], $_POST['abrevation_name_1_' . $val], abs($iWinAmount), $_POST['totalruns_1_' . $val], $_POST['totalruns_orginal_1_' . $val], abs($iRiskAMount), $_POST['totalrunsvalue_1_' . $val], $_POST['event_date_' . $val], $_POST['wager'], '3', $iTicketNumber);


        }


        if ($_POST['totalrunsvalue_2_' . $val] != '') {


            BettingEvent($_POST['event_id' . $val], $_POST['score_away' . $val], $_POST['score_home' . $val], $_POST['winner_away' . $val], $_POST['winner_home' . $val], $_POST['score_away_by_period' . $val], $_POST['score_home_by_period' . $val], $_POST['event_status' . $val]);


            if ($_POST['wager'] == '1') {

                $iDivideBy = 100 / $_POST['totalruns_2_' . $val];
                $iGetAmount = ($_POST['totalrunsvalue_2_' . $val]) * $iDivideBy;

                $iWinAmount = $iGetAmount;
                $iRiskAMount = $_POST['totalrunsvalue_2_' . $val];


            } elseif ($_POST['wager'] == '2') {

                $iDivideBy = 100 / $_POST['totalruns_2_' . $val];
                $iGetAmount = ($_POST['totalrunsvalue_2_' . $val]) * $iDivideBy;

                $iWinAmount = $_POST['totalrunsvalue_2_' . $val];
                $iRiskAMount = $iGetAmount;


            } else {


                $iDivideBy = $_POST['totalruns_2_' . $val] / 100;
                $iGetAmount = ($_POST['totalrunsvalue_2_' . $val]) * $iDivideBy;

                $iWinAmount = $_POST['totalrunsvalue_2_' . $val];
                $iRiskAMount = $iGetAmount;
            }
///// datasumited($tokenid, $eventid, $teamid, $SportName, $AbrNam, $iBetingWin, $BettingCondition, $BettingConditionOrginal,  $iRiskAMount, $BettingAmount, $EventDate, $wager, $type, $iTicketNumber)

            datasumited($_POST['is_away_2_' . $val], $_POST['is_home_2_' . $val], $tokenid, $val, $_POST['o_and_u_2_' . $val], $_POST['team_id_2_' . $val], $_POST['sport_name_2_' . $val], $_POST['abrevation_name_2_' . $val], abs($iWinAmount), $_POST['totalruns_2_' . $val], $_POST['totalruns_orginal_2_' . $val], abs($iRiskAMount), $_POST['totalrunsvalue_2_' . $val], $_POST['event_date_' . $val], $_POST['wager'], '3', $iTicketNumber);


        }


        /******------------------------------------------GAMETOTAL******GAMETOTAL*******GAMETOTAL---------------------------**************/
        /******------------------------------------------GAMETOTAL******GAMETOTAL*******GAMETOTAL---------------------------**************/
        /******------------------------------------------GAMETOTAL******GAMETOTAL*******GAMETOTAL---------------------------**************/
        /******------------------------------------------GAMETOTAL******GAMETOTAL*******GAMETOTAL---------------------------**************/


        if ($_POST['gametotalsvalue_1_' . $val] != '') {

            BettingEvent($_POST['event_id' . $val], $_POST['score_away' . $val], $_POST['score_home' . $val], $_POST['winner_away' . $val], $_POST['winner_home' . $val], $_POST['score_away_by_period' . $val], $_POST['score_home_by_period' . $val], $_POST['event_status' . $val]);


            if ($_POST['wager'] == '1') {

                $iDivideBy = 100 / $_POST['gametotals_1_' . $val];
                $iGetAmount = ($_POST['gametotalsvalue_1_' . $val]) * $iDivideBy;

                $iWinAmount = $iGetAmount;
                $iRiskAMount = $_POST['gametotalsvalue_1_' . $val];


            } elseif ($_POST['wager'] == '2') {

                $iDivideBy = 100 / $_POST['gametotals_1_' . $val];
                $iGetAmount = ($_POST['gametotalsvalue_1_' . $val]) * $iDivideBy;

                $iWinAmount = $_POST['gametotalsvalue_1_' . $val];
                $iRiskAMount = $iGetAmount;


            } else {

                $iDivideBy = $_POST['gametotals_1_' . $val] / 100;
                $iGetAmount = ($_POST['gametotalsvalue_1_' . $val]) * $iDivideBy;

                $iWinAmount = $_POST['gametotalsvalue_1_' . $val];
                $iRiskAMount = $iGetAmount;

            }

///// datasumited($tokenid, $eventid, $teamid, $SportName, $AbrNam, $iBetingWin, $BettingCondition, $BettingConditionOrginal,  $iRiskAMount, $BettingAmount, $EventDate, $wager, $type, $iTicketNumber)

            datasumited($_POST['is_away_1_' . $val], $_POST['is_home_1_' . $val], $tokenid, $val, $_POST['o_and_u_1_' . $val], $_POST['team_id_1_' . $val], $_POST['sport_name_1_' . $val], $_POST['abrevation_name_1_' . $val], abs($iWinAmount), $_POST['totalruns_1_' . $val], $_POST['totalruns_orginal_1_' . $val], abs($iRiskAMount), $_POST['totalrunsvalue_1_' . $val], $_POST['event_date_' . $val], $_POST['wager'], '3', $iTicketNumber);


        }


        if ($_POST['gametotalsvalue_2_' . $val] != '') {


            BettingEvent($_POST['event_id' . $val], $_POST['score_away' . $val], $_POST['score_home' . $val], $_POST['winner_away' . $val], $_POST['winner_home' . $val], $_POST['score_away_by_period' . $val], $_POST['score_home_by_period' . $val], $_POST['event_status' . $val]);


            if ($_POST['wager'] == '1') {

                $iDivideBy = 100 / $_POST['gametotals_2_' . $val];
                $iGetAmount = ($_POST['gametotalsvalue_2_' . $val]) * $iDivideBy;

                $iWinAmount = $iGetAmount;
                $iRiskAMount = $_POST['gametotalsvalue_2_' . $val];


            } elseif ($_POST['wager'] == '2') {

                $iDivideBy = 100 / $_POST['gametotals_2_' . $val];
                $iGetAmount = ($_POST['gametotalsvalue_2_' . $val]) * $iDivideBy;

                $iWinAmount = $_POST['gametotalsvalue_2_' . $val];
                $iRiskAMount = $iGetAmount;


            } else {


                $iDivideBy = $_POST['gametotals_2_' . $val] / 100;
                $iGetAmount = ($_POST['gametotalsvalue_2_' . $val]) * $iDivideBy;

                $iWinAmount = $_POST['gametotalsvalue_2_' . $val];
                $iRiskAMount = $iGetAmount;
            }
///// datasumited($tokenid, $eventid, $teamid, $SportName, $AbrNam, $iBetingWin, $BettingCondition, $BettingConditionOrginal,  $iRiskAMount, $BettingAmount, $EventDate, $wager, $type, $iTicketNumber)

            datasumited($_POST['is_away_2_' . $val], $_POST['is_home_2_' . $val], $tokenid, $val, $_POST['o_and_u_2_' . $val], $_POST['team_id_2_' . $val], $_POST['sport_name_2_' . $val], $_POST['abrevation_name_2_' . $val], abs($iWinAmount), $_POST['totalruns_2_' . $val], $_POST['totalruns_orginal_2_' . $val], abs($iRiskAMount), $_POST['totalrunsvalue_2_' . $val], $_POST['event_date_' . $val], $_POST['wager'], '3', $iTicketNumber);


        }


        /******------------------------------------------TOTAL GOALS******TOTAL GOALS*******TOTAL GOALS---------------------------**************/
        /******------------------------------------------TOTAL GOALS******TOTAL GOALS*******TOTAL GOALS---------------------------**************/
        /******------------------------------------------TOTAL GOALS******TOTAL GOALS*******TOTAL GOALS---------------------------**************/
        /******------------------------------------------TOTAL GOALS******TOTAL GOALS*******TOTAL GOALS---------------------------**************/


        if ($_POST['totalgoalsvalue_1_' . $val] != '') {

            BettingEvent($_POST['event_id' . $val], $_POST['score_away' . $val], $_POST['score_home' . $val], $_POST['winner_away' . $val], $_POST['winner_home' . $val], $_POST['score_away_by_period' . $val], $_POST['score_home_by_period' . $val], $_POST['event_status' . $val]);


            if ($_POST['wager'] == '1') {

                $iDivideBy = 100 / $_POST['totalgoals_1_' . $val];
                $iGetAmount = ($_POST['totalgoalsvalue_1_' . $val]) * $iDivideBy;

                $iWinAmount = $iGetAmount;
                $iRiskAMount = $_POST['totalgoalsvalue_1_' . $val];


            } elseif ($_POST['wager'] == '2') {

                $iDivideBy = 100 / $_POST['totalgoals_1_' . $val];
                $iGetAmount = ($_POST['totalgoalsvalue_1_' . $val]) * $iDivideBy;

                $iWinAmount = $_POST['totalgoalsvalue_1_' . $val];
                $iRiskAMount = $iGetAmount;


            } else {

                $iDivideBy = $_POST['totalgoals_1_' . $val] / 100;
                $iGetAmount = ($_POST['totalgoalsvalue_1_' . $val]) * $iDivideBy;

                $iWinAmount = $_POST['totalgoalsvalue_1_' . $val];
                $iRiskAMount = $iGetAmount;

            }

///// datasumited($tokenid, $eventid, $teamid, $SportName, $AbrNam, $iBetingWin, $BettingCondition, $BettingConditionOrginal,  $iRiskAMount, $BettingAmount, $EventDate, $wager, $type, $iTicketNumber)

            datasumited($_POST['is_away_1_' . $val], $_POST['is_home_1_' . $val], $tokenid, $val, $_POST['o_and_u_1_' . $val], $_POST['team_id_1_' . $val], $_POST['sport_name_1_' . $val], $_POST['abrevation_name_1_' . $val], abs($iWinAmount), $_POST['totalruns_1_' . $val], $_POST['totalruns_orginal_1_' . $val], abs($iRiskAMount), $_POST['totalrunsvalue_1_' . $val], $_POST['event_date_' . $val], $_POST['wager'], '3', $iTicketNumber);


        }


        if ($_POST['totalgoalsvalue_2_' . $val] != '') {


            BettingEvent($_POST['event_id' . $val], $_POST['score_away' . $val], $_POST['score_home' . $val], $_POST['winner_away' . $val], $_POST['winner_home' . $val], $_POST['score_away_by_period' . $val], $_POST['score_home_by_period' . $val], $_POST['event_status' . $val]);


            if ($_POST['wager'] == '1') {

                $iDivideBy = 100 / $_POST['totalgoals_2_' . $val];
                $iGetAmount = ($_POST['totalgoalsvalue_2_' . $val]) * $iDivideBy;

                $iWinAmount = $iGetAmount;
                $iRiskAMount = $_POST['totalgoalsvalue_2_' . $val];


            } elseif ($_POST['wager'] == '2') {

                $iDivideBy = 100 / $_POST['totalgoals_2_' . $val];
                $iGetAmount = ($_POST['totalgoalsvalue_2_' . $val]) * $iDivideBy;

                $iWinAmount = $_POST['totalgoalsvalue_2_' . $val];
                $iRiskAMount = $iGetAmount;


            } else {


                $iDivideBy = $_POST['totalgoals_2_' . $val] / 100;
                $iGetAmount = ($_POST['totalgoalsvalue_2_' . $val]) * $iDivideBy;

                $iWinAmount = $_POST['totalgoalsvalue_2_' . $val];
                $iRiskAMount = $iGetAmount;
            }
///// datasumited($tokenid, $eventid, $teamid, $SportName, $AbrNam, $iBetingWin, $BettingCondition, $BettingConditionOrginal,  $iRiskAMount, $BettingAmount, $EventDate, $wager, $type, $iTicketNumber)

            datasumited($_POST['is_away_2_' . $val], $_POST['is_home_2_' . $val], $tokenid, $val, $_POST['o_and_u_2_' . $val], $_POST['team_id_2_' . $val], $_POST['sport_name_2_' . $val], $_POST['abrevation_name_2_' . $val], abs($iWinAmount), $_POST['totalruns_2_' . $val], $_POST['totalruns_orginal_2_' . $val], abs($iRiskAMount), $_POST['totalrunsvalue_2_' . $val], $_POST['event_date_' . $val], $_POST['wager'], '3', $iTicketNumber);


        }

        /******------------------------------------------TEAM TOTALS******TEAM TOTALS*******TEAM TOTALS---------------------------**************/
        /******------------------------------------------TEAM TOTALS******TEAM TOTALS*******TEAM TOTALS---------------------------**************/
        /******------------------------------------------TEAM TOTALS******TEAM TOTALS*******TEAM TOTALS---------------------------**************/
        /******------------------------------------------TEAM TOTALS******TEAM TOTALS*******TEAM TOTALS---------------------------**************/


        if ($_POST['teamtotalsvalue_1_' . $val] != '') {

            BettingEvent($_POST['event_id' . $val], $_POST['score_away' . $val], $_POST['score_home' . $val], $_POST['winner_away' . $val], $_POST['winner_home' . $val], $_POST['score_away_by_period' . $val], $_POST['score_home_by_period' . $val], $_POST['event_status' . $val]);


            if ($_POST['wager'] == '1') {

                $iDivideBy = 100 / $_POST['teamtotals_1_' . $val];
                $iGetAmount = ($_POST['teamtotalsvalue_1_' . $val]) * $iDivideBy;

                $iWinAmount = $iGetAmount;
                $iRiskAMount = $_POST['teamtotalsvalue_1_' . $val];


            } elseif ($_POST['wager'] == '2') {

                $iDivideBy = 100 / $_POST['teamtotals_1_' . $val];
                $iGetAmount = ($_POST['teamtotalsvalue_1_' . $val]) * $iDivideBy;

                $iWinAmount = $_POST['teamtotalsvalue_1_' . $val];
                $iRiskAMount = $iGetAmount;


            } else {

                $iDivideBy = $_POST['teamtotals_1_' . $val] / 100;
                $iGetAmount = ($_POST['teamtotalsvalue_1_' . $val]) * $iDivideBy;

                $iWinAmount = $_POST['teamtotalsvalue_1_' . $val];
                $iRiskAMount = $iGetAmount;

            }

///// datasumited($tokenid, $eventid, $teamid, $SportName, $AbrNam, $iBetingWin, $BettingCondition, $BettingConditionOrginal,  $iRiskAMount, $BettingAmount, $EventDate, $wager, $type, $iTicketNumber)

            datasumited($_POST['is_away_1_' . $val], $_POST['is_home_1_' . $val], $tokenid, $val, $_POST['o_and_u_1_' . $val], $_POST['team_id_1_' . $val], $_POST['sport_name_1_' . $val], $_POST['abrevation_name_1_' . $val], abs($iWinAmount), $_POST['totalruns_1_' . $val], $_POST['totalruns_orginal_1_' . $val], abs($iRiskAMount), $_POST['totalrunsvalue_1_' . $val], $_POST['event_date_' . $val], $_POST['wager'], '3', $iTicketNumber);


        }


        if ($_POST['teamtotalsvalue_2_' . $val] != '') {


            BettingEvent($_POST['event_id' . $val], $_POST['score_away' . $val], $_POST['score_home' . $val], $_POST['winner_away' . $val], $_POST['winner_home' . $val], $_POST['score_away_by_period' . $val], $_POST['score_home_by_period' . $val], $_POST['event_status' . $val]);


            if ($_POST['wager'] == '1') {

                $iDivideBy = 100 / $_POST['teamtotals_2_' . $val];
                $iGetAmount = ($_POST['teamtotalsvalue_2_' . $val]) * $iDivideBy;

                $iWinAmount = $iGetAmount;
                $iRiskAMount = $_POST['teamtotalsvalue_2_' . $val];


            } elseif ($_POST['wager'] == '2') {

                $iDivideBy = 100 / $_POST['totalruns_2_' . $val];
                $iGetAmount = ($_POST['teamtotalsvalue_2_' . $val]) * $iDivideBy;

                $iWinAmount = $_POST['teamtotalsvalue_2_' . $val];
                $iRiskAMount = $iGetAmount;


            } else {


                $iDivideBy = $_POST['teamtotals_2_' . $val] / 100;
                $iGetAmount = ($_POST['teamtotalsvalue_2_' . $val]) * $iDivideBy;

                $iWinAmount = $_POST['teamtotalsvalue_2_' . $val];
                $iRiskAMount = $iGetAmount;
            }
///// datasumited($tokenid, $eventid, $teamid, $SportName, $AbrNam, $iBetingWin, $BettingCondition, $BettingConditionOrginal,  $iRiskAMount, $BettingAmount, $EventDate, $wager, $type, $iTicketNumber)

            datasumited($_POST['is_away_2_' . $val], $_POST['is_home_2_' . $val], $tokenid, $val, $_POST['o_and_u_2_' . $val], $_POST['team_id_2_' . $val], $_POST['sport_name_2_' . $val], $_POST['abrevation_name_2_' . $val], abs($iWinAmount), $_POST['totalruns_2_' . $val], $_POST['totalruns_orginal_2_' . $val], abs($iRiskAMount), $_POST['totalrunsvalue_2_' . $val], $_POST['event_date_' . $val], $_POST['wager'], '3', $iTicketNumber);


        }


    }


    $q = "SELECT SUM(riskamount) as totalbettingprice FROM bettingdetail WHERE tokenid = '" . $tokenid . "'";
    $result = mysqli_query($con, $q);
    $row = mysqli_fetch_array($result);

    $iLoginUser = mysqli_query($con, "SELECT * FROM users WHERE user_id = '" . $_SESSION['user_id'] . "'");
    $iLoginUserDetails = mysqli_fetch_assoc($iLoginUser);


    if (($_POST['freeplay'] == '1') && ($iLoginUserDetails['free_pay_balance'] < $row['totalbettingprice'])) {
        $iDeleteDSate = mysqli_query($con, "DELETE FROM bettingdetail WHERE tokenid = '" . $tokenid . "'");
        redirect("insufficentfund.php?action=freepay&tokenid=" . $row['totalbettingprice']);
        exit;

    } elseif ($iLoginUserDetails['user_available_balance'] < $row['totalbettingprice']) {
        $iDeleteDSate = mysqli_query($con, "DELETE FROM bettingdetail WHERE tokenid = '" . $tokenid . "'");
        redirect("insufficentfund.php?tokenid=" . $tokenid);
        exit;
    }

    redirect("WagerTicket.php?tokenid=" . $tokenid);
    exit;


}
?>
<!DOCTYPE HTML>
<html>
<?php include $basedir . '/common/header.php'; ?>
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Rubik">
<style>

    body {
        background: #ededed !important;
        font-family: 'Rubik', serif;
    }

    #myaccount .box, #dashboard .box {
        margin: 0 !important;
    }

    #myFormTes table:first-child thead, #myFormTes table:first-child thead tr:first-child td {
        padding: 10px;
        text-align: left;
    }

    #myFormTes table:first-child thead {
        background: #666666;
        color: white;
        font-weight: 600;
    }

    #myFormTes td select {
        text-transform: none;
        width: 65px;
        height: 22px;
    }

    .HeaderColumnsName td {
        border-right: 1px solid;
        text-align: center;
        padding: 10px;
    }

    .HeaderColumnsName th {
        background: #a7aaae !important;
        font: bold 12px Arial !important;
        height: 24px !important;
        padding: 10px 0 10px 0 !important;
        text-align: center !important;
        color: #FFFFFF !important;
    }

    .tdSpread, .tdMoneyLine, .tdTotal {
        border-right: 1px solid;
    }

    .tdHeaderTeamTotal {
        width: 28%;
    }

    .TableTeamTotal {
        width: 100%;
    }

    .TableTeamTotal td input {
        float: left;
    }

    .Submitbutton {
        background: linear-gradient(#7fb3b2, #518194);
        border: 0;
        color: white;
        padding: 6px 15px 6px 13px;
        color: white;
        border: 0;
        border-radius: 3px;
        cursor: pointer;
    }

    input[type="text"] {
        padding: 10px 6px !important;
        width: 40px !important;
        box-shadow: inset 0 0 3px 0 black;
    }

    #tblcc tr > td {
        padding: 4px;
    }

    .td3 select {
        padding: 2px;
    }

    td.tdTeamName {
        font-weight: 600;
        border-right: 1px solid;
    }

    .submitwager {
        clear: right;
        float: right;
        padding-top: 5px;
        padding-bottom: 4px;
    }

    .HeaderTableLine {
        background: #525252;
        border-bottom: 1px solid #ffffff;
        line-height: 25px;
        padding: 0 17px;
        font-size: 14px;
        padding-right: 10px;
    }

    .HeaderColumnsName {
        background: #666666;
        font: bold 12px Arial;
        height: 24px;
        padding: 0 0 0 10px;
        text-align: left;
        color: #FFFFFF;
    }

    .BackgroundLineGray {
        background: #ffffff;
        height: 25px;
        color: #000000;
    }

    #tblcc tr > td:first-child {
        border-bottom: 0px;
    }

    .tdDate, .tdTelevised, .tdTeamName, .tdSpread, .tdMoneyLine, .tdTotal, .tdTeamTotal {
        text-align: left;
        font: normal 12px "Segoe UI", Arial, sans-serif;
    }

    .BackgroundLineGray {
        background: #ffffff;
        height: 25px;
        color: #000000;
    }

    .BackgroundLineAlternateGray {
        border: 1px solid;
        background: #66666630;
    }

    .lastRow {
        border-bottom: 1px solid gray;
    }

    .TableWager {
        border-collapse: collapse;
        width: 100%;
        font-size: 8px;
        font-family: Arial;
        height: 100%;
        border: 1px solid #000;
    }

    .tdContestTeamName, .tdTeamName {
        width: 16%;
        padding-left: 5px;
        vertical-align: inherit;
        white-space: nowrap;
    }

    .cellSportHeader {
        clear: left;
        float: left;
        padding-top: 8px;
        text-align: left;
        color: #ffffff;
    }

    .TableSpread {
        width: 100%;
        font-family: Arial;
        font-size: 12px;
    }

    #tblcc tr > td:first-child {
        border-bottom: 0px;
    }

    .TableSpread td.td1, .TableMoneyLine td.td1, .TableTotals td.td1 {
        width: 1%;
        text-align: right;
    }

    .TableSpread td.td2, .TableMoneyLine td.td2, .TableTotals td.td2 {
        text-align: right;
        width: 5%;
    }

    .TableSpread td.td3, .TableMoneyLine td.td3, .TableTotals td.td3 {
        text-align: left;
    }

    .textBox {
        cursor: text;
        font-family: Arial;
        font-size: 11px;
        width: 38px;
        height: 15px;
        color: Blue;
        background: #FFF;
        border: 1px solid #999;
        padding: 2px;
        outline: medium none;
    }

    input[type="text"], input[type="password"], input[type="date"], input[type="datetime"], input[type="datetime-local"], input[type="month"], input[type="week"], input[type="email"], input[type="number"], input[type="search"], input[type="tel"], input[type="time"], input[type="url"], textarea {
        padding: 15px 10px;
        color: black;
        font-size: 15px;
        width: 100px;
    }

    .tdTeamName p {
        font-weight: 400;
    }

    .mainapidata table thead tr th {
        text-align: center;
        background: #666666;
        color: #fff;
        padding: 10px 0px;
        font-size: 13px;
    }

    .mainapidata table thead tr td {
        text-align: center;
    }

    .mainapidata table tr td {
        width: 250px;
        font-size: 11px;
        vertical-align: middle;
        text-align: center;
    }

    table tbody tr td table.innertable tr {
        height: 30px;
    }

    table tbody tr td table.innertable tr td {
        cursor: pointer;
        padding: 7px;
        border-left: 1px solid #d3d3d3;
    }

    table tbody tr td table.innertable tr td:hover {
        background-color: #d4d4d4;
    }

    .innertable tr td {
        text-align: center;
    }

    .oddtble {
        background-color: #fff;
        padding-top: 10px;
    }

    .evetble {
        background-color: #fff;
        padding-top: 10px;
    }

    .spacing_div {
        height: 15px;
        background: #ededed;
    }

    table tbody tr td table.innertable tr td input[type="text"] {
        float: left;
        box-shadow: inset 0 0 3px 0 black;
        margin-right: 10px;
        width: 50px;
        height: 0px;
        font-size: 11px;
        margin: 0px;
    }

    .abrveiation {
        width: 50px !important;
    }

    #myFormTes table:first-child thead, #myFormTes table:first-child thead tr:first-child td input {
        vertical-align: middle;
    }

    .errror {
        background: #ff000096;
        padding: 10px 37px;
        color: #fff;
        font-size: 15px;
        width: 100%;
        border: 7px solid #fafafa;
        border-radius: 18px;
    }
    table {
        width: 100%;
    }

    .abul td{
        vertical-align: top !important;
        padding: 7px !important;
    }
</style>
<script type="text/javascript">
    function submitthisform() {
        document.getElementById("myFormTes").submit();

    }
</script>
<body>
<?php include $basedir . '/common/head.php'; ?>
<div class="container row">
    <?php include $basedir . '/common/myheadmenu.php'; ?>
    <main role="main" class="row gutters mypage" style="border: none;">
        <article id="myaccount" class="col span_12">
            <div class="box" style="margin: 0px 10px 0px 10px;">
                <div class="title_box">
                    <!--<h4 class="title"><?php echo $lang[406]; ?></h4>
						<p class="desc"><?php echo $lang[486]; ?></p>-->
                </div>
                <main class="bg-light clearfix">
                    <div class="row">
                        <div class="col-md-6">
                               
                            <div class="table-responsive mainapidata">
                                   
                                <form action="" method="post" id="myFormTes">
                                    <?php echo $iError; ?>
                                    <table class="abul">
                                        <thead>

                                        <?php
                                        function getDataByLeague($iLeagueId, $iSportId)
                                        {

                                            $ch = curl_init();
                                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            curl_setopt($ch, CURLOPT_URL, 'https://api.betsapi.com/v1/bet365/upcoming?sport_id=' . $iSportId . '&token=26928-6HWCoyS7zhskxm&league_id=' . $iLeagueId);
                                            $result = curl_exec($ch);
                                            curl_close($ch);
                                            return $obj = json_decode($result, true);
                                        }

                                        function getData($iFI)
                                        {


                                            $ch = curl_init();
                                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                            curl_setopt($ch, CURLOPT_URL, 'https://api.betsapi.com/v1/bet365/prematch?token=26928-6HWCoyS7zhskxm&FI=' . $iFI);
                                            $result = curl_exec($ch);
                                            curl_close($ch);
                                            return $obj = json_decode($result, true);
                                        }

                                        ?>


                                        <?php
                                        $aoplkjlk = 1;
                                        $i = 0;
                                        
                                            
                                        

                                        foreach ($_POST['leagueid'] as $KeyNew => $valNew)
                                        {
                                        if ($valNew != '')
                                        {
                                        
                                        $IMKae = 0;
                                        $obj = getDataByLeague($valNew, $_POST['sport_id'][$KeyNew]);
                                        
                                       
                                        foreach ($obj['results'] as $key => $val)
                                        {
                                            
                                        $IMKae = $IMKae + 1;

                                        if ($IMKae == 1) { ?>

                                        <tr>
                                            <td class="HeaderTableLine" colspan="7">
                                                <div>
                                                    <div class="cellSportHeader"><span
                                                             style="font-size: 18px"><?php echo $val['league']['name']." ".$val['league']['id']; ?></span>
                                                        <br>

                                                        <?php echo date("D, F d, Y", $val['time']); ?><br>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php

                                        $iMakLineResult = getData($val['id']);
                                          
                                        $run_line = $iMakLineResult['results'][0]['main']['sp']['run_line'][0]['odds'];
                                        $spread = $iMakLineResult['results'][0]['main']['sp']['point_spread'][0]['odds'];
                                                
                                        if($_POST['sport_id'][$KeyNew] == 151)    //e-sport
                                        {
                                            $spread = $iMakLineResult['results'][0]['main']['sp']['match_lines'][1]['odds'];
                                        }
                                        else if($_POST['sport_id'][$KeyNew] == 17)    //hockey
                                        {
                                            $spread = $iMakLineResult['results'][0]['main']['sp']['puck_line'][0]['odds'];
                                        }
                                        else if($_POST['sport_id'][$KeyNew] == 13)    //tennies
                                        {
                                            $spread = $iMakLineResult['results'][0]['main']['sp']['set_betting'][0]['odds'];
                                        }

                                        $soccerspread = $iMakLineResult['results'][0]['main']['sp']['handicap_result'][0]['odds'] 
                                                      ||$iMakLineResult['results'][0]['main']['sp']['full_time_result'][0]['odds'];
                                        $gametotal = $iMakLineResult['results'][0]['main']['sp']['game_totals'][0]['odds'];
                                        if($_POST['sport_id'][$KeyNew] == 17)    //hockey
                                        {
                                            $gametotal = $iMakLineResult['results'][0]['main']['sp']['game_totals'][0]['odds'];
                                        }
                                        if($_POST['sport_id'][$KeyNew] == 13)    //tennies
                                        {
                                            $gametotal = $iMakLineResult['results'][0]['main']['sp']['total_games_2_way'][0]['odds'];
                                        }
                                        $totalgoal = $iMakLineResult['results'][0]['main']['sp']['goals_over_under'][0]['odds'];
                                        
                                        $teamtotal = $iMakLineResult['results'][0]['main']['sp']['team_totals'][0]['odds'];
                                        $moneyline = $iMakLineResult['results'][0]['main']['sp']['money_line'][0]['odds'];
                                        $moneyline2 = $iMakLineResult['results'][0]['schedule']['sp']['main'][0]['odds'];
                                        if($_POST['sport_id'][$KeyNew] == 13)   //tennies
                                        {
                                            $moneyline2 = $iMakLineResult['results'][0]['main']['sp']['to_win_match'][0]['odds'];
                                        }
                                        else if($_POST['sport_id'][$KeyNew] == 151)    //e-sport
                                        {
                                            $moneyline2 = $iMakLineResult['results'][0]['main']['sp']['match_lines'][0]['odds'];
                                        }
                                        else if($_POST['sport_id'][$KeyNew] == 17)   //hockey
                                        {
                                                $moneyline2 = $iMakLineResult['results'][0]['main']['sp']['money_line_3_way'][0]['odds'];
                                        }
                                        else if($_POST['sport_id'][$KeyNew] == 1)   //soccer
                                        {
                                                $drawline = $iMakLineResult['results'][0]['main']['sp']['full_time_result'][1]['odds'];
                                        }
                                               
                                        $soccermoneyline = $iMakLineResult['results'][0]['main']['sp']['full_time_result'][0]['odds'];
                                        
                                        ?>
                                        <tr class="HeaderColumnsName">
                                            <th class="thHeaderPeriod" colspan="2">Game</th>
                                            <?php if ($spread) {
                                                echo '<th class="tdHeaderSpread">Spread</th>';
                                            } else if ($run_line) {
                                                echo '<th class="tdHeaderTotal">Run Line</th>';
                                            } else if ($soccerspread) {
                                                echo '<th class="tdHeaderTotal">Spread</th>';
                                            } 
                                            else if($_POST['sport_id'][$KeyNew] == 18 && (empty($spread) || empty($run_line) || empty($soccerspread)))
                                            {
                                                echo '<th class="tdHeaderTotal">Spread</th>';
                                            }
                                          
                                            ?>

                                            <?php if ($moneyline) {
                                                echo '<th class="tdHeaderMoneyLine">Money Line</th>';
                                            } else if ($soccermoneyline) {
                                                echo '<th class="tdHeaderTotal">Money Line2</th>';
                                            } else if ($moneyline2) {
                                                echo '<th class="tdHeaderTotal">Money Line</th>';
                                            }
                                            else if($_POST['sport_id'][$KeyNew] == 18 && (empty($moneyline) || empty($soccermoneyline) || empty($moneyline2)))
                                            {
                                                echo '<th class="tdHeaderMoneyLine">Money Line</th>';
                                            }
                                           
                                             ?>

                                            <?php 
                                            if ($gametotal) {
                                                echo '<th class="tdHeaderTotal">Totals</th>';
                                            }
                                            else if ($totalgoal) {
                                                echo '<th class="tdHeaderTotal">Totals</th>';
                                            }
                                            else if ($teamtotal) {
                                                echo '<th class="tdHeaderTotal">Team Totals</th>';
                                            }
                                            else if($_POST['sport_id'][$KeyNew] == 18 )
                                            {
                                                echo '<th class="tdHeaderTotal">Totals</th>';
                                            }
                                            ?>


                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?php
                                        }

                                        $i = $i + 1;
                                        ?>
                                        <tr class="spacing_div">
                                            <td colspan="2"></td>
                                            <?php if ($spread) {
                                                echo '<td></td>';

                                            } else if ($run_line) {
                                                echo '<td></td>';
                                            } else if ($soccerspread) {
                                                echo '<td></td>';
                                            } ?>

                                            <?php if ($moneyline) {
                                                echo '<td></td>';
                                            } else if ($soccermoneyline) {
                                                echo '<td></td>';
                                            } else if ($moneyline2) {
                                                echo '<td></td>';
                                            } ?>
                                            <?php if ($gametotal) {
                                                echo '<td></td>';
                                            }
                                            ?>
                                            <?php if ($totalgoal) {
                                                echo '<td></td>';
                                            }
                                            ?>
                                            <?php if ($teamtotal) {
                                                echo '<td></td>';
                                            }
                                            ?>

                                        </tr>
                                        <tr class="<?php if ($i % 2 == '0') {
                                            echo 'oddtble';
                                        } else {
                                            echo 'evetble';
                                        } ?>" data-rowid="<?= $val['id'] ?>" id="rowid_<?=$val['id']?>">
                                            <td style="background-color: #e3e3e3; font-size: 14px; font-weight: bold; color: #404040;">
                                                <!--                                                --><?php //echo $val['id'];
                                                //                                                echo $val['ss'];
                                                //                                                echo $val['scores'];
                                                //                                                // echo '----';  echo $time = date("m/d/Y h:i:s A T",$val['time']);; echo '---------------'; echo $val['league']['name'];
                                                //                                                ?>
                                                <input type="hidden"
                                                       value="<?php echo date("Y-m-d H:i:s", $val['time']); ?>"
                                                       name="event_date_<?php echo $val['id']; ?>">
                                                <input type="hidden" value="<?php echo $val['id']; ?>"
                                                       name="event_id[]">
                                                <input type="hidden" value="<?php echo $val['id']; ?>"
                                                       name="event_id_<?php echo $val['id']; ?>">

                                                <!-- Start atik's modification -->
                                                <input type="hidden" value="<?= $val['league']['name'] ?>"
                                                       id="league_name_<?php echo $val['id']; ?>">
                                                <input type="hidden" value="<?= date("m/d/Y", $val['time']) ?>"
                                                       id="event_date_<?php echo $val['id']; ?>">
                                                <input type="hidden" value="<?= date("h:i a", $val['time']) ?>"
                                                       id="event_time_<?php echo $val['id']; ?>">
                                                <!-- End atik's modification -->

                                                <!--                                                --><?php //echo $val['league']['name']; ?><!-- <br>-->
                                                <?php echo date("m/d/Y", $val['time']); ?><br>
                                                <?php echo date("h:i a", $val['time']); ?>
                                                <input type="hidden" value="<?php echo $val['id']; ?>"
                                                       name="event_id<?php echo $val['id']; ?>">
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td><?php echo $val['home']['id'].' '.$val['home']['name']; ?>
                                                            <input type="hidden"
                                                                   name="is_away_1_<?php echo $val['id']; ?>"
                                                                   value="1" readonly/>
                                                            <input type="hidden"
                                                                   name="is_home_1_<?php echo $val['id']; ?>"
                                                                   value="" readonly/>
                                                            <input type="hidden"
                                                                   name="sport_name_1_<?php echo $val['id']; ?>"
                                                                   value="<?php echo $val['home']['name']; ?>"
                                                                   style="background: transparent;border: none;box-shadow: none;width: 40px!important;"
                                                                   readonly/>
                                                            <input type="hidden"
                                                                   value="<?php echo $val['home']['id']; ?>"
                                                                   name="team_id_1_<?php echo $val['id']; ?>">
                                                            <input type="hidden" value="1"
                                                                   name="o_and_u_1_<?php echo $val['id']; ?>">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><?php echo $val['away']['id'] . ' ' . $val['away']['name']; ?>
                                                            <input type="hidden"
                                                                   name="is_away_2_<?php echo $val['id']; ?>"
                                                                   value="" readonly/>
                                                            <input type="hidden"
                                                                   name="is_home_2_<?php echo $val['id']; ?>"
                                                                   value="1" readonly/>
                                                            <input type="hidden"
                                                                   name="sport_name_2_<?php echo $val['id']; ?>"
                                                                   value="<?php echo $val['away']['name']; ?>"
                                                                   style="background: transparent;border: none;box-shadow: none;width: 40px!important;"
                                                                   readonly/>
                                                            <input type="hidden"
                                                                   value="<?php echo $val['away']['id']; ?>"
                                                                   name="team_id_2_<?php echo $val['id']; ?>">
                                                            <input type="hidden" value="2"
                                                                   name="o_and_u_2_<?php echo $val['id']; ?>">


                                                        </td>
                                                       
                                                       
                                                    </tr>
                                                    <?php 
                                                          if($_POST['sport_id'][$KeyNew] == 1) { ?>
                                                            <tr> 
                                                                <td> 
                                                              <strong>Draw</strong> 
                                                            </td>
                                                            </tr>
                                                          <?php
                                                          }
                                                        ?>
                                                </table>
                                            </td>
                                            <?php
                                            $iMakLineResult = getData($val['id']);
                                           
                                             /* echo "<pre>";
                                             print_r($iMakLineResult);
                                             echo "</pre>";
                                             exit; */
                                    
                                            foreach ($iMakLineResult['results'] as $keyNew => $valNew) {

                                                $iRunLine_1 = $valNew['main']['sp']['run_line'][0]['odds'];
                                                    $iRunLine_Value_1 = toAmericanDecimal($iRunLine_1);

                                                $iRunLine_2 = $valNew['main']['sp']['run_line'][1]['odds'];
                                                    $iRunLine_Value_2 = toAmericanDecimal($iRunLine_2);


                                                    // spread1 value start

                                                $iSpread_1 = $valNew['main']['sp']['point_spread'][0]['odds'];
                                                if($_POST['sport_id'][$KeyNew] == 151) //E-sport
                                                {
                                                    $iSpread_1 = $valNew['main']['sp']['match_lines'][1]['odds'];
                                                }
                                                if($_POST['sport_id'][$KeyNew] == 17) //hocckey
                                                {
                                                    $iSpread_1 = $valNew['main']['sp']['puck_line'][0]['odds'];
                                                }
                                                if($_POST['sport_id'][$KeyNew] == 13) //tennies
                                                {
                                                    $iSpread_1 = $valNew['main']['sp']['set_betting'][0]['odds'];
                                                }
                                                $iSpread_Value_1 = toAmericanDecimal($iSpread_1);



                                                // spread2 value start
                                                $iSpread_2 = $valNew['main']['sp']['point_spread'][1]['odds'];
                                                if($_POST['sport_id'][$KeyNew] == 151)
                                                {
                                                    $iSpread_2 = $valNew['main']['sp']['match_lines'][4]['odds'];
                                                }
                                                else if($_POST['sport_id'][$KeyNew] == 17)
                                                {
                                                    $iSpread_2 = $valNew['main']['sp']['puck_line'][1]['odds'];
                                                }
                                                else if($_POST['sport_id'][$KeyNew] == 13)
                                                {
                                                    $iSpread_2 = $valNew['main']['sp']['set_betting'][2]['odds'];
                                                }
                                                $iSpread_Value_2 = toAmericanDecimal($iSpread_2);


                                                $iSoccerSpread_1 =  empty($valNew['main']['sp']['handicap_result'][0]['odds']) 
                                                ? $valNew['main']['sp']['full_time_result'][0]['odds']
                                                : $valNew['main']['sp']['handicap_result'][0]['odds'];
                                                    $iSoccerSpread_Value_1 = toAmericanDecimal($iSoccerSpread_1);

                                                $iSoccerSpread_2 =  empty( $valNew['main']['sp']['handicap_result'][2]['odds'])
                                                ? $valNew['main']['sp']['full_time_result'][2]['odds']
                                                : $valNew['main']['sp']['handicap_result'][2]['odds'];
                                                    $iSoccerSpread_Value_2 = toAmericanDecimal($iSoccerSpread_2);

                                                $iMoneyLine_1 = $valNew['main']['sp']['money_line'][0]['odds'];
                                                    $iMoneyLine_Value_1 = toAmericanDecimal($iMoneyLine_1);

                                                $iMoneyLine_2 = $valNew['main']['sp']['money_line'][1]['odds'];
                                                    $iMoneyLine_Value_2 = toAmericanDecimal($iMoneyLine_2);

                                                $iMoneyLine2_1 = $valNew['schedule']['sp']['main'][0]['odds'];
                                                if($_POST['sport_id'][$KeyNew] == 13)
                                                {
                                                    $iMoneyLine2_1 = $valNew['main']['sp']['to_win_match'][0]['odds'];
                                                }
                                                else if($_POST['sport_id'][$KeyNew] == 17)
                                                {
                                                    $iMoneyLine2_1 = $valNew['main']['sp']['money_line_3_way'][0]['odds'];
                                                }
                                                else if($_POST['sport_id'][$KeyNew] == 151)
                                                {
                                                    $iMoneyLine2_1 = $valNew['main']['sp']['match_lines'][0]['odds'];
                                                }
                                                else if($_POST['sport_id'][$KeyNew] == 1)   //soccer
                                                {
                                                        $drawline = toAmericanDecimal($valNew['main']['sp']['full_time_result'][1]['odds']);
                                                }

                                                    $iMoneyLine2_Value_1 = toAmericanDecimal($iMoneyLine2_1);

                                                $iMoneyLine2_2 = $valNew['schedule']['sp']['main'][1]['odds'];
                                                if( $_POST['sport_id'][$KeyNew] == 13)
                                                {
                                                    $iMoneyLine2_2 = $valNew['main']['sp']['to_win_match'][1]['odds'];
                                                }
                                                else if($_POST['sport_id'][$KeyNew] == 17)
                                                 {
                                                    $iMoneyLine2_2 = $valNew['main']['sp']['money_line_3_way'][1]['odds'];
                                                 }
                                                else if($_POST['sport_id'][$KeyNew] == 151)
                                                 {
                                                    $iMoneyLine2_2 = $valNew['main']['sp']['match_lines'][3]['odds'];
                                                 }

                                                    $iMoneyLine2_Value_2 = toAmericanDecimal($iMoneyLine2_2);

                                                $iSoccerMoneyLine_1 = $valNew['main']['sp']['full_time_result'][0]['odds'];
                                                    $iSoccerMoneyLine_Value_1 = toAmericanDecimal($iSoccerMoneyLine_1);

                                                $iSoccerMoneyLine_2 = $valNew['main']['sp']['full_time_result'][2]['odds'];
                                                    $iSoccerMoneyLine_Value_2 = toAmericanDecimal($iSoccerMoneyLine_2);
                                                
                                                $iGameTotals_1 = $valNew['main']['sp']['game_totals'][0]['odds'];
                                                      
                                                if($_POST['sport_id'][$KeyNew] == 17) //hockey
                                                {
                                                   $iGameTotals_1 = $valNew['main']['sp']['game_totals'][0]['odds'];
                                                }
                                                else if($_POST['sport_id'][$KeyNew] == 13) //tennies
                                                {
                                                   $iGameTotals_1 = $valNew['main']['sp']['total_games_2_way'][0]['odds'];
                                                }
                                                    $iGameTotals_Value_1 = toAmericanDecimal($iGameTotals_1);


                                               
                                                $iGameTotals_2 = $valNew['main']['sp']['game_totals'][1]['odds'];
                                                if($_POST['sport_id'][$KeyNew] == 17) //hockey
                                                {
                                                   $iGameTotals_2 = $valNew['main']['sp']['game_totals'][1]['odds'];
                                                }
                                                else if($_POST['sport_id'][$KeyNew] == 13) //tennies
                                                {
                                                   $iGameTotals_2 = $valNew['main']['sp']['total_games_2_way'][1]['odds'];
                                                }
                                                    $iGameTotals_Value_2 = toAmericanDecimal($iGameTotals_2);
                                                
                                                $iTotalGoals_1 = $valNew['main']['sp']['goals_over_under'][0]['odds'];
                                                    $iTotalGoals_Value_1 = toAmericanDecimal($iTotalGoals_1);

                                                $iTotalGoals_2 = $valNew['main']['sp']['goals_over_under'][1]['odds'];
                                                    $iTotalGoals_Value_2 = toAmericanDecimal($iTotalGoals_2);
                                             
                                                $iTeamTotals_1 = $valNew['main']['sp']['team_totals'][0]['odds'];
                                                    $iTeamTotals_Value_1 = toAmericanDecimal($iTeamTotals_1);
                                              
                                                $iTeamTotals_2 = $valNew['main']['sp']['team_totals'][1]['odds'];
                                                    $iTeamTotals_Value_2 = toAmericanDecimal($iTeamTotals_2);
                                                
                                                $iHandicap_1 = $valNew['main']['sp']['run_line'][0]['handicap'];
                                                if($_POST['sport_id'][$KeyNew] == 13) //tennies
                                                {
                                                   $iHandicap_1 = $valNew['main']['sp']['set_betting'][0]['name'];
                                                }
                                                else if($_POST['sport_id'][$KeyNew] == 17) //hocckey
                                                {
                                                   $iHandicap_1 = $valNew['main']['sp']['puck_line'][0]['name'];
                                                }
                                                
                                                $iHandicap_2 = $valNew['main']['sp']['run_line'][1]['handicap'];
                                                if($_POST['sport_id'][$KeyNew] == 13) //tennies
                                                {
                                                   $iHandicap_2 = $valNew['main']['sp']['set_betting'][3]['name'];
                                                }
                                                else if($_POST['sport_id'][$KeyNew] == 17) //hocckey
                                                {
                                                   $iHandicap_2 = $valNew['main']['sp']['puck_line'][1]['name'];
                                                }
                                                

                                                $iSHandicap_1 = $valNew['main']['sp']['point_spread'][0]['handicap'];
                                                if($_POST['sport_id'][$KeyNew] == 151) //E-Sport
                                                {
                                                   $iSHandicap_1 = $valNew['main']['sp']['match_lines'][1]['handicap'];
                                                }
                                                else if($_POST['sport_id'][$KeyNew] == 17) //hockey
                                                {
                                                    $iSHandicap_1 = $valNew['main']['sp']['puck_line'][0]['handicap'];
                                                }
                                                else if($_POST['sport_id'][$KeyNew] == 13) //Tennies
                                                {
                                                    $iSHandicap_1 = $valNew['main']['sp']['set_betting'][0]['name'];
                                                }
                                                $iSHandicap_2 = $valNew['main']['sp']['point_spread'][1]['handicap'];
                                                if($_POST['sport_id'][$KeyNew] == 151) //E-Sport
                                                {
                                                   $iSHandicap_2 = $valNew['main']['sp']['match_lines'][1]['handicap'];
                                                }
                                                else if($_POST['sport_id'][$KeyNew] == 17) //hockey
                                                {
                                                    $iSHandicap_2 = $valNew['main']['sp']['puck_line'][1]['handicap'];
                                                }
                                                else if($_POST['sport_id'][$KeyNew] == 13) //tennies
                                                {
                                                    $iSHandicap_2 = $valNew['main']['sp']['set_betting'][3]['name'];
                                                }

                                                $iSSHandicap_1 = $valNew['main']['sp']['handicap_result'][0]['opp'];
                                                $iSSHandicap_2 = $valNew['main']['sp']['handicap_result'][2]['opp'];

                                                $iGtOver_1 = $valNew['main']['sp']['game_totals'][0]['name'];
                                                 if($_POST['sport_id'][$KeyNew] == 13) //tennies
                                                 {
                                                    $iGtOver_1 = $valNew['main']['sp']['total_games_2_way'][0]['name'];
                                                 }
                                                else if($_POST['sport_id'][$KeyNew] == 17) //hocckey
                                                 {
                                                    $iGtOver_1 = $valNew['main']['sp']['game_totals'][0]['name'];
                                                 }
                                                $iGtOver_2 = $valNew['main']['sp']['game_totals'][1]['name'];

                                                if($_POST['sport_id'][$KeyNew] == 13) //tennies
                                                 {
                                                    $iGtOver_2 = $valNew['main']['sp']['total_games_2_way'][1]['name'];
                                                 }
                                                else if($_POST['sport_id'][$KeyNew] == 13) //hocckey
                                                 {
                                                    $iGtOver_2 = $valNew['main']['sp']['game_totals'][1]['name'];
                                                 }

                                                $iTTOver_1 = $valNew['main']['sp']['team_totals'][0]['name'];
                                                $iTTOver_2 = $valNew['main']['sp']['team_totals'][1]['name'];

                                                $iTgOver_1 = $valNew['main']['sp']['goals_over_under'][0]['goals'];
                                                $iTgOver_2 = $valNew['main']['sp']['goals_over_under'][1]['goals'];

                                                $iTggOver_1 = $valNew['main']['sp']['goals_over_under'][0]['header'];
                                                $iTggOver_2 = $valNew['main']['sp']['goals_over_under'][1]['header'];


                                                ?>

                                                <?php if ($spread) { ?>

                                                    <td>
                                                        <table class="innertable">
                                                            <tr>
                                                                <td id="spread_11_<?= $val['id'] ?>" class="activestate" style="border-bottom: 1px solid #d3d3d3"
                                                                    onclick="addtocart(<?= $val['id'] ?>,'home','spread')">
                                                                    <?php if ($iSpread_1 == '' || empty($iSpread_1)) {
                                                                        echo "OTB";
                                                                    } else { ?>
                                                                    <?php $iSpread_Value_1; ?>
                                                                    <!-- <input type="text"
                                                                           name="spreadvalue_1_"/> -->
                                                                    <?php $IPlusMinValu = '';
                                                                    if ($iSpread_Value_1 > 0) {
                                                                        $IPlusMinValu = '+';
                                                                    } ?>
                                                                    <b
                                                                            id="hadicap_1_<?= $val['id'] ?>"><?php if ($iSHandicap_1) {
                                                                            echo '( ' . $iSHandicap_1 . ' )';
                                                                        } ?></b>
                                                                    <?php echo $IPlusMinValu . $iSpread_Value_1; ?>
                                                                    <input type="hidden"
                                                                           name="spread_1_<?php echo $val['id']; ?>"
                                                                           value="<?php echo $IPlusMinValu . $iSpread_Value_1; ?>"
                                                                           readonly>

                                                                    <input type="hidden"
                                                                           name="spread_orginal_1_<?php echo $val['id']; ?>"
                                                                           value="<?php echo $IPlusMinValu . $iSpread_Value_1; ?> <?php echo $iSpread_Value_1 ?>">
                                                                </td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td style="padding: 7px"
                                                                  class="activestate"
                                                                    id="spread_12_<?= $val['id'] ?>"
                                                                    onclick="addtocart(<?= $val['id'] ?>,'away','spread')">
                                                                    <?php if ($iSpread_2 == '' || empty($iSpread_2)) {
                                                                        echo "OTB";
                                                                    } else { ?>
                                                                        <?php $iSpread_Value_2; ?>
                                                                        <!-- <input type="text"
                                                                               name="spreadvalue_2_"/> -->
                                                                        <?php $IPlusMinValu = '';
                                                                        if ($iSpread_Value_2 > 0) {
                                                                            $IPlusMinValu = '+';
                                                                        } ?>
                                                                        <b id="hadicap_2_<?= $val['id'] ?>"><?php if ($iSHandicap_2) {
                                                                                echo '( ' . $iSHandicap_2 . ' )';
                                                                            } ?></b>
                                                                        <?php echo $IPlusMinValu . $iSpread_Value_2; ?>
                                                                        <input type="hidden"
                                                                               name="spread_2_<?php echo $val['id']; ?>"
                                                                               value="<?php echo $IPlusMinValu . $iSpread_Value_2; ?>"
                                                                               style="background: transparent;border: none;box-shadow: none;width: 40px!important;"
                                                                               readonly>
                                                                        <input type="hidden"
                                                                               name="spread_orginal_2_<?php echo $val['id']; ?>"
                                                                               value="<?php echo $IPlusMinValu . $iSpread_Value_2; ?> <?php echo $iSpread_Value_2 ?>">

                                                                    <?php } ?>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                <?php } elseif ($soccerspread) { ?>

                                                    <td>
                                                        <table class="innertable">
                                                            <tr>
                                                                <td id="soccerspread_11_<?=$val['id']?>" class="activestate" style="border-bottom: 1px solid #d3d3d3"
                                                                    onclick="addtocart(<?=$val['id']?>,'home','soccerspread')">
                                                                    <?php if ($iSoccerSpread_1 == '') {
                                                                        echo "OTB";
                                                                    } else { ?>
                                                                    <?php $iSoccerSpread_Value_1; ?>
                                                                    <!-- <input type="text"
                                                                           name="soccerspreadvalue_1_"/> -->
                                                                    <?php $IPlusMinValu = '';
                                                                    if ($iSoccerSpread_Value_1 > 0) {
                                                                        $IPlusMinValu = '+';
                                                                    } ?>
                                                                    <b><?php if ($iSSHandicap_1) {
                                                                            echo ' ' . $iSSHandicap_1 . ' ';
                                                                        } ?></b>
                                                                    <?php echo $IPlusMinValu . $iSoccerSpread_Value_1; ?>
                                                                    <input type="hidden"
                                                                           name="soccerspread_1_<?php echo $val['id']; ?>"
                                                                           value="<?php echo $IPlusMinValu . $iSoccerSpread_Value_1; ?>"
                                                                           readonly>

                                                                    <input type="hidden"
                                                                           name="soccerspread_orginal_1_<?php echo $val['id']; ?>"
                                                                           value="<?php echo $IPlusMinValu . $iSoccerSpread_Value_1; ?> <?php echo $iSoccerSpread_Value_1 ?>">
                                                                </td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td id="soccerspread_12_<?= $val['id'] ?>" class="activestate" 
                                                                    onclick="addtocart(<?= $val['id'] ?>,'away','soccerspread')">
                                                                    <?php if ($iSoccerSpread_2 == '') {
                                                                        echo "OTB";
                                                                    } else { ?>
                                                                        <?php $iSoccerSpread_Value_2; ?>
                                                                        <!-- <input type="text"
                                                                               name="soccerspreadvalue_2_"/> -->
                                                                        <?php $IPlusMinValu = '';
                                                                        if ($iSoccerSpread_Value_2 > 0) {
                                                                            $IPlusMinValu = '+';
                                                                        } ?>
                                                                        <b><?php if ($iSSHandicap_2) {
                                                                                echo ' ' . $iSSHandicap_2 . ' ';
                                                                            } ?></b>
                                                                        <?php echo $IPlusMinValu . $iSoccerSpread_Value_2; ?>
                                                                        <input type="hidden"
                                                                               name="soccerspread_2_<?php echo $val['id']; ?>"
                                                                               value="<?php echo $IPlusMinValu . $iSoccerSpread_Value_2; ?>"
                                                                               readonly>
                                                                        <input type="hidden"
                                                                               name="soccerspread_orginal_2_<?php echo $val['id']; ?>"
                                                                               value="<?php echo $IPlusMinValu . $iSoccerSpread_Value_2; ?> <?php echo $iSoccerSpread_Value_2 ?>">

                                                                    <?php } ?>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                <?php } elseif ($run_line) { ?>

                                                    <td>
                                                        <table class="innertable">
                                                            <tr>
                                                                <td id="run_line_11_<?=$val['id']?>" class="activestate" style="border-bottom: 1px solid #d3d3d3"
                                                                    onclick="addtocart(<?=$val['id']?>,'home','run_line')">
                                                                    <?php if ($iRunLine_1 == '') {
                                                                        echo "OTB";
                                                                    } else { ?>
                                                                    <?php $iRunLine_Value_1; ?>
                                                                    <!-- <input type="text"
                                                                           name="runlinevalue_1_"/> -->
                                                                    <?php $IPlusMinValu = '';
                                                                    if ($iRunLine_Value_1 > 0) {
                                                                        $IPlusMinValu = '+';
                                                                    } ?>
                                                                    <b><?php if ($iHandicap_1) {
                                                                            echo '( ' . $iHandicap_1 . ' )';
                                                                        } ?></b>
                                                                    <?php echo $IPlusMinValu . $iRunLine_Value_1; ?>
                                                                    <input type="hidden"
                                                                           name="runline_1_<?php echo $val['id']; ?>"
                                                                           value="<?php echo $IPlusMinValu . $iRunLine_Value_1; ?>"
                                                                           readonly>

                                                                    <input type="hidden"
                                                                           name="runline_orginal_1_<?php echo $val['id']; ?>"
                                                                           value="<?php echo $IPlusMinValu . $iRunLine_Value_1; ?> <?php echo $iRunLine_Value_1 ?>">
                                                                </td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td  id="run_line_12_<?=$val['id']?>" class="activestate"
                                                                    onclick="addtocart(<?=$val['id']?>,'away','run_line')">
                                                                    <?php if ($iRunLine_2 == '') {
                                                                        echo "OTB";
                                                                    } else { ?>
                                                                        <?php $iRunLine_Value_2; ?>
                                                                      <!--   <input type="text"
                                                                               name="runlinevalue_2_"/> -->
                                                                        <?php $IPlusMinValu = '';
                                                                        if ($iRunLine_Value_2 > 0) {
                                                                            $IPlusMinValu = '+';
                                                                        } ?>
                                                                        <b><?php if ($iHandicap_2) {
                                                                                echo '( ' . $iHandicap_2 . ' )';
                                                                            } ?></b>
                                                                        <?php echo $IPlusMinValu . $iRunLine_Value_2; ?>
                                                                        <input type="hidden"
                                                                               name="runline_2_<?php echo $val['id']; ?>"
                                                                               value="<?php echo $IPlusMinValu . $iRunLine_Value_2; ?>"
                                                                               readonly>
                                                                        <input type="hidden"
                                                                               name="runline_orginal_2_<?php echo $val['id']; ?>"
                                                                               value="<?php echo $IPlusMinValu . $iRunLine_Value_2; ?> <?php echo $iRunLine_Value_2 ?>">

                                                                    <?php } ?>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>

                                                <?php } ?>


                                                <?php if ($moneyline) { ?>

                                                    <td>
                                                        <table class="innertable">
                                                            <tr>
                                                                <td id="moneyline_11_<?= $val['id'] ?>" class="activestate" style="border-bottom: 1px solid #d3d3d3"
                                                                    onclick="addtocart(<?= $val['id'] ?>,'home','moneyline')">
                                                                    <?php if ($iMoneyLine_1 == '') {
                                                                        if ($run_line) {
                                                                            echo "OTB";
                                                                        }
                                                                    } else { ?>
                                                                        <?php $iMoneyLine_Value_1; ?>
                                                                         <!-- <input type="text"
                                                                               name="moneylinevalue_1_"/> --> 
                                                                        <?php $IPlusMinValu = '';
                                                                        if ($iMoneyLine_Value_1 > 0) {
                                                                            $IPlusMinValu = '+';
                                                                        } ?>
                                                                        <?php echo $IPlusMinValu . $iMoneyLine_Value_1; ?>
                                                                        <input type="hidden"
                                                                               name="moneyline_1_<?php echo $val['id']; ?>"
                                                                               id="moneyline_1_<?php echo $val['id']; ?>"
                                                                               value="<?php echo $IPlusMinValu . $iMoneyLine_Value_1; ?>"
                                                                               style="background: transparent;border: none;box-shadow: none;width: 40px!important;"
                                                                               readonly/>
                                                                        <input type="hidden"
                                                                               name="moneyline_orginal_1_<?php echo $val['id']; ?>"
                                                                               value="<?php echo $IPlusMinValu . $iMoneyLine_Value_1; ?>"/>
                                                                    <?php } ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="moneyline_12_<?= $val['id'] ?>"
                                                                class="activestate"
                                                                    onclick="addtocart(<?= $val['id'] ?>,'away','moneyline')">
                                                                    <?php if ($iMoneyLine_2 == '') {
                                                                        if ($run_line) {
                                                                            echo "OTB";
                                                                        }
                                                                    } else { ?>
                                                                        <?php $iMoneyLine_Value_2; ?>
                                                                        <!--  <input type="text"
                                                                               name="moneylinevalue_2_"   />  -->
                                                                        <?php $IPlusMinValu = '';
                                                                        if ($iMoneyLine_Value_2 > 0) {
                                                                            $IPlusMinValu = '+';
                                                                        } ?>
                                                                        <?php echo $IPlusMinValu . $iMoneyLine_Value_2 ?>
                                                                        <input type="hidden"
                                                                               name="moneyline_2_<?php echo $val['id']; ?>"
                                                                               value="<?php echo $IPlusMinValu . $iMoneyLine_Value_2 ?>"
                                                                               style="background: transparent;border: none;box-shadow: none;width: 40px!important;"
                                                                               readonly/>
                                                                        <input type="hidden"
                                                                               name="moneyline_orginal_2_<?php echo $val['id']; ?>"
                                                                               value="<?php echo $IPlusMinValu . $iMoneyLine_Value_2 ?>"/>
                                                                    <?php } ?>


                                                                </td>
                                                               
                                                            </tr>
                                                            
                                                           
                                                        
                                                        </table>
                                                    </td>
                                                <?php } elseif ($totalgoal) { ?>


                                                <?php } elseif ($moneyline2) { ?>

                                                    <td>
                                                        <table class="innertable">
                                                            <tr>
                                                                <td id="moneyline2_11_<?= $val['id'] ?>" class="activestate" style="border-bottom: 1px solid #d3d3d3"
                                                                    onclick="addtocart(<?= $val['id'] ?>,'home','moneyline2')">
                                                                    

                                                                        <?php if ($iMoneyLine2_1 == '') {
                                                                            if ($run_line) {
                                                                                echo "OTB";
                                                                            }
                                                                        } else { ?>
                                                                            <?php $iMoneyLine2_Value_1; ?>
                                                                            <!-- <input type="text"
                                                                                  name="moneyline2value_1_"/>  -->
                                                                            <?php $IPlusMinValu = '';
                                                                            if ($iMoneyLine2_Value_1 > 0) {
                                                                                $IPlusMinValu = '+';
                                                                            } ?>
                                                                            <?php echo $IPlusMinValu . $iMoneyLine2_Value_1; ?>
                                                                            <input type="hidden"
                                                                                   name="moneyline2_1_<?php echo $val['id']; ?>"
                                                                                   value="<?php echo $IPlusMinValu . $iMoneyLine2_Value_1; ?>"
                                                                                   style="background: transparent;border: none;box-shadow: none;width: 40px!important;"
                                                                                   readonly/>
                                                                            <input type="hidden"
                                                                                   name="moneyline2_orginal_1_<?php echo $val['id']; ?>"
                                                                                   value="<?php echo $IPlusMinValu . $iMoneyLine2_Value_1; ?>"/>
                                                                        <?php } ?>

                                                                   

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="moneyline2_12_<?= $val['id'] ?>" class="activestate"
                                                                    onclick="addtocart(<?= $val['id'] ?>,'away','moneyline2')">
                                                                    
                                                                        <?php if ($iMoneyLine2_2 == '') {
                                                                            if ($run_line) {
                                                                                echo "OTB";
                                                                            }
                                                                        } else { ?>
                                                                            <?php $iMoneyLine2_Value_2; ?>
                                                                            <!-- <input type="text"
                                                                                   name="moneylinevalue_2_"/> -->
                                                                            <?php $IPlusMinValu = '';
                                                                            if ( $iMoneyLine2_Value_2 > 0) {
                                                                                $IPlusMinValu = '+';
                                                                            } ?>
                                                                            <?php echo  $IPlusMinValu.$iMoneyLine2_Value_2 ?>
                                                                            <input type="hidden"
                                                                                   name="moneyline_2_<?php echo $val['id']; ?>"
                                                                                   value="<?php echo $IPlusMinValu . $iMoneyLine2_Value_2 ?>"
                                                                                   style="background: transparent;border: none;box-shadow: none;width: 40px!important;"
                                                                                   readonly/>
                                                                            <input type="hidden"
                                                                                   name="moneyline_orginal_2_<?php echo $val['id']; ?>"
                                                                                   value="<?php echo $IPlusMinValu . $iMoneyLine2_Value_2 ?>"/>
                                                                        <?php } ?>
                                                                    


                                                                </td>
                                                               
                                                            </tr>
                                                          
                                                        </table>
                                                    </td>
                                                <?php } ?>


                                                <?php if ($soccermoneyline) { ?>

                                                    <td>
                                                        <table class="innertable">
                                                            <tr>
                                                                <td id="soccermoneyline_11_<?= $val['id'] ?>" class="activestate" style="border-bottom: 1px solid #d3d3d3"
                                                                    onclick="addtocart(<?= $val['id'] ?>,'home','soccermoneyline')">
                                                                    <?php if ($iSoccerMoneyLine_1 == '') {
                                                                        if ($run_line) {
                                                                            echo "OTB";
                                                                        }
                                                                    } else { ?>
                                                                        <?php $iSoccerMoneyLine_Value_1; ?>
                                                                        <!-- <input type="text"
                                                                               name="soccermoneylinevalue_1_"/> -->
                                                                        <?php $IPlusMinValu = '';
                                                                        if ($iSoccerMoneyLine_Value_1 > 0) {
                                                                            $IPlusMinValu = '+';
                                                                        } ?>
                                                                        <input type="text"
                                                                               name="soccermoneyline_1_<?php echo $val['id']; ?>"
                                                                               value="<?php echo $IPlusMinValu . $iSoccerMoneyLine_Value_1; ?>"
                                                                               style="background: transparent;border: none;box-shadow: none;width: 40px!important;"
                                                                               readonly/>
                                                                        <input type="hidden"
                                                                               name="soccermoneyline_orginal_1_<?php echo $val['id']; ?>"
                                                                               value="<?php echo $IPlusMinValu . $iSoccerMoneyLine_Value_1; ?>"/>
                                                                    <?php } ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="soccermoneyline_12_<?= $val['id'] ?>" class="activestate"
                                                                    onclick="addtocart(<?= $val['id'] ?>,'away','soccermoneyline')">
                                                                    <?php if ($iSoccerMoneyLine_2 == '') {
                                                                        if ($run_line) {
                                                                            echo "OTB";
                                                                        }
                                                                    } else { ?>
                                                                        <?php $iSoccerMoneyLine_Value_2; ?>
                                                                        <!-- <input type="text"
                                                                               name="soccermoneylinevalue_2_"/> -->
                                                                        <?php $IPlusMinValu = '';
                                                                        if ($iSoccerMoneyLine_Value_2 > 0) {
                                                                            $IPlusMinValu = '+';
                                                                        } ?>
                                                                        <input type="text"
                                                                               name="soccermoneyline_2_<?php echo $val['id']; ?>"
                                                                               value="<?php echo $IPlusMinValu . $iSoccerMoneyLine_Value_2 ?>"
                                                                               style="background: transparent;border: none;box-shadow: none;width: 40px!important;"
                                                                               readonly/>
                                                                        <input type="hidden"
                                                                               name="soccermoneyline_orginal_2_<?php echo $val['id']; ?>"
                                                                               value="<?php echo $IPlusMinValu . $iSoccerMoneyLine_Value_2 ?>"/>
                                                                    <?php } ?>


                                                                </td>
                                                            </tr>
                                                            <?php 
                                                          if($drawline!="" && $_POST['sport_id'][$KeyNew] == 1) { ?>
                                                          <tr>
                                                         <td class="activestate"> 
                                                            <input type="text" value="<?=$drawline?>" 
                                                            style="background: transparent;border: none;box-shadow: none;width: 40px!important;"
                                                            readonly/>
                                                                 
                                                            </td>
                                                            </tr>
                                                          <?php
                                                          }
                                                        ?>
                                                        </table>
                                                    </td>
                                                <?php } ?>


                                                <?php if ($totalgoal) { ?>


                                                    <td>
                                                        <table class="innertable">
                                                            <tr>
                                                                <td id="totalgoal_11_<?= $val['id'] ?>" class="activestate" style="border-bottom: 1px solid #d3d3d3"
                                                                    onclick="addtocart(<?= $val['id'] ?>,'home','totalgoal')">
                                                                    <?php if ($iTotalGoals_1 == '') {
                                                                        echo "OTB";
                                                                    } else { ?>
                                                                        <?php $iTotalGoals_Value_1; ?>
                                                                        <!-- <input type="text"
                                                                               name="totalgoalsvalue_1_"/> -->

                                                                        <b id="tggover_1_<?=$val['id']?>" >(<?php echo $iTggOver_1; ?> <?php echo $iTgOver_1; ?>
                                                                            )</b>
                                                                        <?php echo $iTotalGoals_Value_1; ?>
                                                                        <input type="hidden"
                                                                               name="totalgoals_1_<?php echo $val['id']; ?>"
                                                                               value="<?php echo $iTotalGoals_Value_1; ?>"
                                                                               style="background: transparent;border: none;box-shadow: none;width: 40px!important;"
                                                                               readonly>

                                                                        <input type="hidden"
                                                                               name="totalgoals_orginal_1_<?php echo $val['id']; ?>"
                                                                               value="0 <?php echo $iTotalGoals_Value_1; ?>">
                                                                    <?php } ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="totalgoal_12_<?= $val['id'] ?>" class="activestate" 
                                                                    onclick="addtocart(<?= $val['id'] ?>,'home','totalgoal')">
                                                                    <?php if ($iTotalGoals_2 == '') {
                                                                        echo "OTB";
                                                                    } else { ?>
                                                                        <?php $iTotalGoals_Value_2; ?>
                                                                       <!--  <input type="text"
                                                                               name="totalgoalsvalue_2_"/> -->

                                                                        <b id="tggover_2_<?=$val['id']?>">(<?php echo $iTggOver_2; ?> <?php echo $iTgOver_2; ?>
                                                                            )</b>
                                                                        <?php echo $iTotalGoals_Value_2; ?>
                                                                        <input type="hidden"
                                                                               name="totalgoals_2_<?php echo $val['id']; ?>"
                                                                               value="<?php echo $iTotalGoals_Value_2; ?>"
                                                                               style="background: transparent;border: none;box-shadow: none;width: 40px!important;"
                                                                               readonly>

                                                                        <input type="hidden"
                                                                               name="totalgoals_orginal_2_<?php echo $val['id']; ?>"
                                                                               value="U <?php echo $iTotalGoals_Value_2; ?>">
                                                                    <?php } ?>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                <?php } ?>


                                                <?php if ($gametotal) { ?>


                                                    <td>
                                                        <table class="innertable">
                                                            <tr>
                                                                <td id="gametotals_11_<?= $val['id'] ?>" class="activestate" style="border-bottom: 1px solid #d3d3d3"
                                                                    onclick="addtocart(<?= $val['id'] ?>,'home','gametotal')">
                                                                    <?php if ($iGameTotals_1 == '') {
                                                                        echo "OTB";
                                                                    } else { ?>
                                                                        <?php $iGameTotals_Value_1; ?>
                                                                        <!-- <input type="text"
                                                                               name="gametotalsvalue_1_"/> -->

                                                                        <b
                                                                                id="igtoverrrr_1_<?= $val['id'] ?>">(<?php echo $iGtOver_1; ?>
                                                                            )</b>
                                                                        <?php echo $iGameTotals_Value_1; ?>
                                                                        <input type="hidden"
                                                                               name="gametotals_1_<?php echo $val['id']; ?>"
                                                                               value="<?php echo $iGameTotals_Value_1; ?>"
                                                                               style="background: transparent;border: none;box-shadow: none;width: 40px!important;"
                                                                               readonly>

                                                                        <input type="hidden"
                                                                               name="gametotals_orginal_1_<?php echo $val['id']; ?>"
                                                                               value="0 <?php echo $iGameTotals_Value_1; ?>">
                                                                    <?php } ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="gametotals_12_<?= $val['id'] ?>"
                                                                class="activestate"
                                                                    onclick="addtocart(<?= $val['id'] ?>,'away','gametotal')">
                                                                    <?php if ($iGameTotals_2 == '') {
                                                                        echo "OTB";
                                                                    } else { ?>
                                                                        <?php $iGameTotals_Value_2; ?>
                                                                        <!-- <input type="text"
                                                                               name="gametotalsvalue_2_"/> -->

                                                                        <b
                                                                                id="igtoverrrr_2_<?= $val['id'] ?>">(<?php echo $iGtOver_2; ?>
                                                                            )</b>
                                                                        <?php echo $iGameTotals_Value_2; ?>
                                                                        <input type="hidden"
                                                                               name="gametotals_2_<?php echo $val['id']; ?>"
                                                                               value="<?php echo $iGameTotals_Value_2; ?>"
                                                                               style="background: transparent;border: none;box-shadow: none;width: 40px!important;"
                                                                               readonly>

                                                                        <input type="hidden"
                                                                               name="gametotals_orginal_2_<?php echo $val['id']; ?>"
                                                                               value="U <?php echo $iGameTotals_Value_2; ?>">
                                                                    <?php } ?>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                <?php } ?>

                                                <?php if ($teamtotal) { ?>


                                                    <td>
                                                        <table class="innertable">
                                                            <tr>
                                                                <td id="teamtotal_11_<?= $val['id'] ?>" class="activestate" style="border-bottom: 1px solid #d3d3d3"
                                                                    onclick="addtocart(<?= $val['id'] ?>,'home','teamtotal')">
                                                                    <?php if ($iTeamTotals_1 == '') {
                                                                        echo "OTB";
                                                                    } else { ?>
                                                                        <?php $iTeamTotals_Value_1; ?>
                                                                        <!-- <input type="text"
                                                                               name="totalrunsvalue_1_"/> -->

                                                                        <b id="ttover_1_<?=$val['id']?>">(<?php echo $iTTOver_1; ?>
                                                                            )</b>
                                                                        <?php echo $iTeamTotals_Value_1; ?>
                                                                        <input type="hidden"
                                                                               name="totalruns_1_<?php echo $val['id']; ?>"
                                                                               value="<?php echo $iTeamTotals_Value_1; ?>"
                                                                               style="background: transparent;border: none;box-shadow: none;width: 40px!important;"
                                                                               readonly>

                                                                        <input type="hidden"
                                                                               name="totalruns_orginal_1_<?php echo $val['id']; ?>"
                                                                               value="0 <?php echo $iTeamTotals_Value_1; ?>">
                                                                    <?php } ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="teamtotal_12_<?= $val['id'] ?>" class="activestate"
                                                                    onclick="addtocart(<?= $val['id'] ?>,'away','teamtotal')">
                                                                    <?php if ($iTeamTotals_2 == '') {
                                                                        echo "OTB";
                                                                    } else { ?>
                                                                        <?php $iTeamTotals_Value_2; ?>
                                                                        <!-- <input type="text"
                                                                               name="totalrunsvalue_2_"/> -->

                                                                        <b id="ttover_2_<?=$val['id']?>">(<?php echo $iTTOver_2; ?>
                                                                            )</b>
                                                                        <?php echo $iTeamTotals_Value_2; ?>
                                                                        <input type="hidden"
                                                                               name="totalruns_2_<?php echo $val['id']; ?>"
                                                                               value="<?php echo $iTeamTotals_Value_2; ?>"
                                                                               style="background: transparent;border: none;box-shadow: none;width: 40px!important;"
                                                                               readonly>

                                                                        <input type="hidden"
                                                                               name="totalruns_orginal_2_<?php echo $val['id']; ?>"
                                                                               value="U <?php echo $iTeamTotals_Value_2; ?>">
                                                                    <?php } ?>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                <?php } ?>


                                            <?php } ?>
                                        </tr>
                                        <?php }
                                        }
                                        } ?>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>

                    </div>

                </main>


            </div>
        </article>

        <style>
            .side-bar-demo-content {
                position: absolute;
                width: 35%;
                min-width: 200px;
                /*top: 37px;*/
                right: -351px;
                height: auto;
                background-color: #fff;

            }

            .mypanel > .heading {
                padding: 12px;
                font-size: 16px;

                border-bottom: 2px solid #b21e22;
                background-color: #fff;
                text-transform: uppercase;
                text-align: center;
            }

            .badge {
                width: 30px;
                padding: 5px;
                border-radius: 50%;
                background-color: #b21e22;
                color: white;
            }

            .mypanel .card-items-box {
                font-size: 14px;
                background-color: #fff;
                display: flex;
                color: #3c3c3c;
                flex-direction: column;
            }

            .mypanel > .body {
                padding: 12px;
                font-size: 16px;
                background-color: #fff;
                display: flex;
                flex-direction: column;
                /*max-height: 500px;*/
                min-height: 100px;
            }

            .btn {
                padding: 10px;

            }

            .btn-danger {
                background-color: #b21e22;
                cursor: pointer;
                color: white;
                border: none;
                border-radius: 5px;
                margin-top: 10px;
            }

            .btn-danger:hover {
                background-color: #f45f62;
            }

            .myrate input[type=text] {
                width: 100% !important;
                height: 35px;
                border: 1px;
                border-radius: 3px
            }

            .totalbet_cal {
                display: flex;
                width: 100%;
                height: calc(100% / 30px);
                flex-direction: column;
                background-color: #525252;
                color: white;
                padding: 12px;
                font-size: 14px
            }

            .totalbet_cal span {
                font-size: 13px;
                opacity: 0.8;
                color: #fff;
                padding: 5px;
            }

            .eachbox {
                overflow: hidden;
            }

            .active {
                background: #b21e22;
            }

            .active input[type="text"] {
                color: white;
            }

            .closecard {
                font-size: 10px;
                color: #fff;
                padding: 4px;
                border-radius: 20%;
                background-color: #b21e22;
                margin-left: 9px;
                text-align: center;
                cursor: pointer;
            }
            .clearsection{
                text-decoration: underline;
                cursor: pointer;
                color: #e2e2e266;
            }

            .messgae{
                padding: 8px;
                border: none;
                margin-bottom: -5px;
                margin-left: -5px;
                margin-right: -5px;
            }
            .red-alert{
                background: #b21e22;
                color: white;
            }
            .green-alert{
                background: #09ec68de;
                color: whitesmoke;
            }
            .show{
                display:block;
            }
            .hide{
                display: none;
            }

        </style>
        <article class="side-bar-demo-content">
            <div class="mypanel">
                <div class="heading"> Bet slip <span class="badge" id="badge_count">0</span></div>
                <div class="body" id="card-body">
                    <div id="when-no-bet-slip">
                        <p style="text-align: center;"><span style="font-weight: 800">Bet Slip is Empty</span> <br> <span style="font-size: 12px">Not sure where to start?</span> <br>
                            <a href="#">Learn how to place a bet </a></p>
                    </div>
                </div>
                <div class="totalbet_cal">
                    <div style="padding: 5px"><input type="checkbox" name="freeplay" value="1" id="freeplay"> <label for="freeplay" style="opacity: 0.8; font-size: 13px">Free Play</label></div>
                    <div>
                        <p style="float:left;">
                            <span> Total bets </span>
                        </p>
                        <span style="float:right" id="total_bets">0</span>
                    </div>
                    <div>
                        <p style="float:left">
                            <span> Total Stake </span>
                        </p>
                        <span style="float:right" id="total_stake">0</span>
                    </div>
                    <div>
                        <p style="float:left">
                            <span> Possible Winning </span>
                        </p>
                        <span style="float:right" id="possible_winning">0</span>
                    </div>
                    <div>
                         <p class="messgae hide" id="messagebox">

                         </p>
                    </div>

                    <button type="button"class="btn btn-danger" id="confirmbutton"> Confirm</button>
                       
                    <p style="text-align:center;"><a  class="clearsection" id="clearBetSlip"> Clear all selection</a></p>
                </div>
            </div>
        </article>
    </main>

</div>


<script src="<?php echo $baseurl ?>/js/jquery-1.11.0.min.js"></script>
<script src="http://ebet21.com/oldwebsite/php/Assets/JS/Index_JS.js"></script>
<?php //include $basedir . '/common/foot.php'; ?>
<script src="<?php echo $baseurl ?>/js/jquery.remodal.js"></script>
<script type="text/javascript" src="<?php echo $baseurl ?>/js/main_frontend.js"></script>
<script src="<?php echo $baseurl ?>/js/jquery.minimalect.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#myaccount form.inputform select").minimalect();
    });
</script>
</body>
</html>
<script>
    $(document).ready(function () {
        $("form").bind("submit", submitForm);
        function submitForm(e) {
            e.preventDefault();
            e.target.checkValidity();
            var lang = $('select[name="language"]').val();
            var old_lang = $('input[name="old_lang"]').val();
            var tz = $('select[name="timezone"]').val();
            var t = "<?php echo $time;?>";
            var url = "<?php echo $baseurl ?>";
            var key = "<?php echo $public_key?>";
            var hash = "<?php echo $hash?>";
            url = url + "/ajax/changelangtz.php";
            var uri = 'hash=' + hash + '&public=' + key + '&t=' + t;
            uri += '&lang=' + lang + '&tz=' + tz;

            $.ajax({
                type: 'POST',
                url: url,
                beforeSend: function (x) {
                    if (x && x.overrideMimeType) {
                        x.overrideMimeType("application/json;charset=UTF-8");
                    }
                },
                data: uri,
                success: function (data) {
                    if (lang && lang !== old_lang) {
                        window.location.reload(true);
                    }
                    $('#message').html(data.message);
                    setTimeout(function () {
                        $('#message').html('');
                    }, 3000)
                }

            });
        }
    });


    let basecount = {
        bets: 0,
        stake: 0,
        winning: 0
    };

    let creditminLimit = <?=$iThisIsMinAmount?>;
    let creditmaxLimit = <?=$iThisIsMaxAmount?>;
    let userCreditLimit = <?=$iThisLogUs['credit_limit']?>;

    function addtocart(id, slug, wager) {
        let domAction, scores, teamid, teamName,is_away='',is_home='',o_and_u,original_money
            , showscore
            , eventName = $("#league_name_" + id).val()
            , eventDate = $("#event_date_" + id).val()
            , eventTime = $("#event_time_" + id).val()
            , teamHomeName = $("input[name=sport_name_1_" + id + "]").val()
            , teamHomeId = $("input[name=team_id_1_" + id + "]").val()
            , teamAwayName = $("input[name=sport_name_2_" + id + "]").val()
            , teamAwayId = $("input[name=team_id_2_" + id + "]").val()
            /////////   only to save in database
            , is_away_1 = $("input[name=is_away_1_" + id + "]").val()
            , is_away_2 = $("input[name=is_away_2_" + id + "]").val()
            , is_home_1 = $("input[name=is_home_1_" + id + "]").val()
            , is_home_2 = $("input[name=is_home_2_" + id + "]").val()
            , o_and_u_1 = $("input[name=o_and_u_1_" + id + "]").val()
            , o_and_u_2 = $("input[name=o_and_u_2_" + id + "]").val();
            
        if (slug === "home" ) {
              if(wager === "moneyline")
              {
                domAction = document.getElementById('moneyline_11_' + id);
                scores = $("input[name=moneyline_1_" + id + "]").val();
                showscore = scores;
                original_money = $("input[name=moneyline_orginal_1_" + id + "]").val();
              }
              else if(wager === "spread")
              {
                domAction = document.getElementById('spread_11_' + id);
                let handicap = $("#hadicap_1_" + id).text();
                scores = $("input[name=spread_1_" + id + "]").val();
                showscore = handicap + scores;
                original_money = $("input[name=spread_orginal_1_" + id + "]").val();
              }
              else if(wager === "gametotal")
              {
                domAction = document.getElementById('gametotals_11_' + id);
                let gtover = $("#igtoverrrr_1_" + id).text();
                scores = $("input[name=gametotals_1_" + id + "]").val();
                showscore = gtover + scores;
                original_money = $("input[name=gametotals_orginal_1_" + id + "]").val();
              }
              else if(wager === "totalgoal")
              {
                domAction = document.getElementById('totalgoal_11_' + id);
                let gtover = $("#tggover_1_" + id).text();
                scores = $("input[name=totalgoals_1_" + id + "]").val();
                showscore = gtover + scores;
                original_money = $("input[name=totalgoals_orginal_1_" + id + "]").val();
              }
              else if(wager === "teamtotal")
              {
                domAction = document.getElementById('teamtotal_11_' + id);
                let gtover = $("#ttover_1_" + id).text();
                scores = $("input[name=totalruns_1_" + id + "]").val();
                showscore = gtover + scores;
                original_money = $("input[name=totalruns_orginal_1_" + id + "]").val();
              }
              else if(wager === "soccerspread")
              {
                domAction = document.getElementById('soccerspread_11_' + id);
                scores = $("input[name=soccerspread_1_" + id + "]").val();
                showscore =  scores;
                original_money = $("input[name=soccerspread_orginal_1_" + id + "]").val();
              }
              else if(wager === "run_line")
              {
                domAction = document.getElementById('run_line_11_' + id);
                scores = $("input[name=runline_1_" + id + "]").val();
                showscore =  scores;
                original_money = $("input[name=runline_orginal_1_" + id + "]").val();
              }
              else if(wager === "moneyline2")
              {
                domAction = document.getElementById('moneyline2_11_' + id);
                scores = $("input[name=moneyline2_1_" + id + "]").val();
                showscore =  scores;
                original_money = $("input[name=moneyline2_orginal_1_" + id + "]").val();
              }
              else if(wager === "soccermoneyline")
              {
                domAction = document.getElementById('soccermoneyline_11_' + id);
                scores = $("input[name=soccermoneyline_1_" + id + "]").val();
                showscore =  scores;
                original_money = $("input[name=soccermoneyline_orginal_1_" + id + "]").val();
              }
            teamid = teamHomeId;
            teamName = teamHomeName;
            is_home =  is_home_1;
            is_away =  is_away_1;
            o_and_u = o_and_u_1;
        }
        else if (slug === "away") {
                if(wager === "moneyline")
                {
                    domAction = document.getElementById('moneyline_12_' + id);
                    scores = $("input[name=moneyline_2_" + id + "]").val();
                    showscore = scores;
                    original_money = $("input[name=moneyline_orginal_2_" + id + "]").val();
                }
                else if( wager === "spread")
                {
                    domAction = document.getElementById('spread_12_' + id);
                    let handicap = $("#hadicap_2_" + id).text();
                    scores = $("input[name=spread_2_" + id + "]").val();
                    showscore = handicap + scores;
                    original_money = $("input[name=spread_orginal_2_" + id + "]").val();
                }
                else if(wager === "gametotal")
                {
                    domAction = document.getElementById('gametotals_12_' + id);
                    let gtover = $("#igtoverrrr_2_" + id).text();
                    scores = $("input[name=gametotals_2_" + id + "]").val();
                    showscore = gtover + scores;
                    original_money = $("input[name=gametotals_orginal_2_" + id + "]").val();
                }
                else if(wager === "totalgoal")
                {
                    domAction = document.getElementById('totalgoal_12_' + id);
                    let gtover = $("#tggover_2_" + id).text();
                    scores = $("input[name=totalgoals_2_" + id + "]").val();
                    showscore = gtover + scores;
                    original_money = $("input[name=totalgoals_orginal_2_" + id + "]").val();
                }
                else if(wager === "teamtotal")
                {
                    domAction = document.getElementById('teamtotal_12_' + id);
                    let gtover = $("#ttover_2_" + id).text();
                    scores = $("input[name=totalruns_2_" + id + "]").val();
                    showscore = gtover + scores;
                    original_money = $("input[name=totalruns_orginal_2_" + id + "]").val();
                }
                else if(wager === "soccerspread")
                {
                    domAction = document.getElementById('soccerspread_12_' + id);
                    scores = $("input[name=soccerspread_2_" + id + "]").val();
                    showscore =  scores;
                    original_money = $("input[name=soccerspread_orginal_2_" + id + "]").val();
                }
                else if(wager === "run_line")
                {
                    domAction = document.getElementById('run_line_12_' + id);
                    scores = $("input[name=runline_2_" + id + "]").val();
                    showscore =  scores;
                    original_money = $("input[name=runline_orginal_2_" + id + "]").val();
                }
                else if(wager === "moneyline2")
                {
                    domAction = document.getElementById('moneyline2_12_' + id);
                    scores = $("input[name=moneyline_2_" + id + "]").val();
                    showscore =  scores;
                    original_money = $("input[name=moneyline_orginal_2_" + id + "]").val();
                }
                else if(wager === "soccermoneyline")
                {
                    domAction = document.getElementById('soccermoneyline_12_' + id);
                    scores = $("input[name=soccermoneyline_2_" + id + "]").val();
                    showscore =  scores;
                    original_money = $("input[name=soccermoneyline_orginal_2_" + id + "]").val();
                }
            
            teamid = teamAwayId;
            teamName = teamAwayName;
            is_home =  is_home_2;
            is_away =  is_away_2;
            o_and_u =  o_and_u_2;
        }
        
            
        let classLists = domAction.classList
            , cont = 0;


        classLists.toggle('active');   //toggle the class

        if (classLists.contains('active'))   //if active the state
        {
            cont++;
            let card_id = wager + "_" + slug + "_no_" + teamid;
            let inputid = wager + "_" + slug + "_" + teamid;
            let card_info = [wager, slug, teamid, id];
            $("#card-body").append('<div class="card-items-box" id="card_' + card_id + '"  data-idinfo="'+card_info+'" >'+ 
            '<input type="hidden" class="even_id" value="'+id+'" />'+ 
            '<input type="hidden" class="scores" value="'+scores+'" />'+ 
            '<input type="hidden" class="is_away" value="'+is_away+'" />'+ 
            '<input type="hidden" class="event_date" value="'+eventDate+'" />'+ 
            '<input type="hidden" class="event_time" value="'+eventTime+'" />'+ 
            '<input type="hidden" class="is_home" value="'+is_home+'" />'+ 
            '<input type="hidden" class="team_id" value="'+teamid+'" />'+ 
            '<input type="hidden" class="o_and_u" value="'+o_and_u+'" />'+ 
            '<input type="hidden" class="original_money" value="'+original_money+'" />'+ 
            '<div style="height: 1px; background-color: #c0c0c0; width: 100%; margin: 0 0 10px 0;"></div>'+ 
            '<div class="eachbox"><p style="float:left;font-size:16px;font-weight:bold;">'+ 
            '<strong class="teamname">' + teamName + '</strong><br><small style="font-size:12px;"> ' + wager + ' </small></p>'+ 
            '<span style="float:right;font-size:12px;color:rgba(0,0,0,0.5);"> ' + showscore + '  <a class="closecard" data-card="' + card_info + '" onclick="removeCardItem(event)"> X </a> </span>'+
            '</div><div class="eachbox"><p><strong> ' + teamHomeName + ' @ ' + teamAwayName + ' </strong> <br><small style="font-size:12px;"> ' + eventDate + ', ' + eventTime + ' </small></p></div>'+
            '<div class="row myrate eachbox" id="riskwincal"  tamid="' + teamid + '">'+
            '<div class="col span_6">'+
            '<input type="text" class="risk_stake_slip" data-id="' + inputid + '"  id="risk_' + inputid + '" onkeyup="riskCal(' + scores + ',this)" placeholder="Risk"></div>'+
            '<div class="col span_6"><input type="text" class="risk_win_slip" id="win_' + inputid + '" data-id="' + inputid + '" onkeyup="winCal(' + scores + ',this)" placeholder="Win"></div></div><div><p class="messgae hide" id="messagebox_'+inputid+'"></p></div></div>');
            $("#badge_count").text($('.card-items-box').length);
            $("#total_bets").text($('.card-items-box').length);
            $("#messagebox").removeClass('hide').addClass('show');
        }
        else {
            $('#card_' + wager + '_' + slug + '_no_' + teamid).remove();
            renderBetSLIP();
            $("#badge_count").text($('.card-items-box').length);
            $("#total_bets").text($('.card-items-box').length);

        }

        let childs = $("#card-body").children('.card-items-box');

        if (childs.length === 0) {
            $("#when-no-bet-slip").css('display', 'block');
        } else {
            $("#when-no-bet-slip").css('display', 'none');
        }


    }

    function removeCardItem(e) {
        let str = e.target.getAttribute('data-card'),
            str1 = str.split(',');
       
            $('#card_' + str1[0] + '_' + str1[1] + '_no_' + str1[2]).remove();
            // removing the active class after deleting item from the card
            if (str1[0] === "moneyline") {
                let str = str1[1] === "home" ? "moneyline_11_" : "moneyline_12_";
                $("#" + str + str1[3]).removeClass('active');
            }
            else if (str1[0] === "spread") {
                let str = str1[1] === "home" ? "spread_11_" : "spread_12_";
                $("#" + str + str1[3]).removeClass('active');
            }
            else if (str1[0] === "gametotal") {
                let str = str1[1] === "home" ? "gametotals_11_" : "gametotals_12_";
                $("#" + str + str1[3]).removeClass('active');
            }
            else if (str1[0] === "totalgoal") {
                let str = str1[1] === "home" ? "totalgoal_11_" : "totalgoal_12_";
                $("#" + str + str1[3]).removeClass('active');
            }
            else if (str1[0] === "teamtotal") {
                let str = str1[1] === "home" ? "teamtotal_11_" : "teamtotal_12_";
                $("#" + str + str1[3]).removeClass('active');
            }
            else if (str1[0] === "soccerspread") {
                let str = str1[1] === "home" ? "soccerspread_11_" : "soccerspread_12_";
                $("#" + str + str1[3]).removeClass('active');
            }
            else if (str1[0] === "run_line") {
                let str = str1[1] === "home" ? "run_line_11_" : "run_line_12_";
                $("#" + str + str1[3]).removeClass('active');
            }
            else if (str1[0] === "moneyline2") {
                let str = str1[1] === "home" ? "moneyline2_11_" : "moneyline2_12_";
                $("#" + str + str1[3]).removeClass('active');
            }
            else if (str1[0] === "soccermoneyline") {
                let str = str1[1] === "home" ? "soccermoneyline_11_" : "soccermoneyline_12_";
                $("#" + str + str1[3]).removeClass('active');
            }
            $("#badge_count").text($('.card-items-box').length);
            $("#total_bets").text($('.card-items-box').length);
            renderBetSLIP();
        

    }
    function riskCal(scores, currentval) {
        let inputvalue = currentval.value;
        let fieldid = currentval.getAttribute("data-id");
        checkLimit(inputvalue,fieldid);
        document.getElementById("win_" + fieldid).value = Math.abs(((100 / scores) * inputvalue).toFixed(2));
        updateTotal(0, inputvalue, Math.abs((100 / scores) * inputvalue));
    }
    function winCal(scores, currentval) {
        let inputvalue = currentval.value;
        let fieldid = currentval.getAttribute("data-id");
        let riskvalue = ("#risk_" + fieldid);
        $(riskvalue).val(Math.abs(((scores / 100) * inputvalue).toFixed(2)));
        checkLimit($(riskvalue).val(),fieldid);
        updateTotal(0, inputvalue, Math.abs((scores / 100) * inputvalue));
    }
    //  let betss=0,stakee=0,winnning=0;
    function updateTotal(i, s, w) {
        renderBetSLIP();
    }
    function renderBetSLIP() {
        var betss = $('.card-items-box').length;
        if (betss === 0) {
            $("#when-no-bet-slip").css('display', 'block');
        }
        var stakee = 0;
        $('.risk_stake_slip').each(function (i, v) {
            if (($.trim($(v).val())).length > 0) {
                stakee = stakee + parseFloat($(v).val());
            }
        });
        var winnning = 0;
        $('.risk_win_slip').each(function (i, v) {
            if (($.trim($(v).val())).length > 0) {
                winnning = winnning + parseFloat($(v).val());
            }
        });

        $("#total_stake").text(stakee.toFixed(2));
        $("#possible_winning").text(winnning.toFixed(2));
    }

    /* clear bet slips from the deck */
    $("#clearBetSlip").click(function(event){

           if($('.card-items-box').length>0){
                  
                       $(".card-items-box").each(function(i,v){
                             let card = $(v).attr('id');
                             $("#"+card).remove();
                       });

                       $("#badge_count").text($('.card-items-box').length);
                       $("#total_bets").text($('.card-items-box').length);
                       renderBetSLIP();
                       $(".activestate").each(function(i,v){
                            $(v).removeClass("active");
                       });
                  
           }else {
               alert("There is no bet slip listed");
           }

    });




    /* limit check started */

    function checkLimit(value,field)
     {
         let is_valid = 0;
         if (creditminLimit > value)
         {
             is_valid=1;
             popUpMessageBox("You cannot bet less than "+creditminLimit+" .","#messagebox_"+field,is_valid);
         }


         else if (creditmaxLimit < value)
         {
             is_valid=1;
             popUpMessageBox("You cannot bet more than "+creditmaxLimit+" .","#messagebox_"+field,is_valid);
         }

         else if ( parseFloat(userCreditLimit)  < parseFloat($("#total_stake").text())) {
                     alert(parseFloat($("#total_stake").text())+1);
             popUpMessageBox("Please Review Wagers Carefully! click to confirm", "#messagebox", 3);
         }
         else
         {   is_valid = 0;

             popUpMessageBox("", "#messagebox_"+field, is_valid);
             popUpMessageBox("Please Review Wagers Carefully! click to confirm", "#messagebox", 2);

         }
     }

    function popUpMessageBox(msg,cardid,validated){
            if(validated===1)
            {
                $(cardid).removeClass('hide').addClass('show').addClass('red-alert').text(msg);
                let fields = cardid.split("_");
                let card_id = fields[1] + "_" + fields[2] + "_no_" + fields[3];
                $("#card_"+card_id).css('border','1px solid #de4040').css('padding','5px');
            }
            else if(validated===2)
            {
                $(cardid).removeClass('hide').addClass('show').addClass('green-alert').text(msg);
            }
            else if(validated===3)
            {
                $(cardid).removeClass('hide').addClass('show').addClass('red-alert').text(msg);
            }
            else
            {
                $(cardid).removeClass('show').addClass('hide');
                let fields = cardid.split("_");
                let card_id = fields[1] + "_" + fields[2] + "_no_" + fields[3];
                $("#card_"+card_id).css('border','none');
            }
    }


    document.getElementById("confirmbutton").addEventListener("click",function(){

           let cardArr = [];
        $('.card-items-box').each(function(i,v){
              
               let carditem = {};
                carditem.is_away    = $(v).find('.is_away').val();
                carditem.is_home    = $(v).find('.is_home').val();
                carditem.o_and_u    = $(v).find('.o_and_u').val();
                carditem.teamname   = $(v).find('.teamname').text();
                carditem.team_id    = $(v).find('.team_id').val();
                carditem.even_id    = $(v).find('.even_id').val();
                carditem.scores     = $(v).find('.scores').val();
                carditem.event_date = $(v).find('.event_date').val()+'-'+$(v).find('.event_time').val();
                carditem.original_money = $(v).find('.original_money').val();
                carditem.risk_stake_slip = $(v).find('.risk_stake_slip').val();
                carditem.risk_win_slip = $(v).find('.risk_win_slip').val();
                cardArr.push(carditem);
        });


        $.ajax({
            url: "StraightBet-save-save.php",
            type: "POST",
            data: {
                items :  cardArr,
                status: "oka"
            },
            success: function(res){
          
               let resp  = JSON.parse(res);
               window.location.href = 'WagerTicket.php?tokenid='+resp.response
            }

        });

        console.log(cardArr);
        
   

});


</script>
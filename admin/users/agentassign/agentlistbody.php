<div class="row" style="margin-bottom: 15px;">
    
</div>

<div class="row">
    <div class="col-xs-12">
	    <div class="box">
	        <div class="box-header">
				<h3 class="box-title"><?php echo $lang[17] ?><a href="#adminregist" class="btn btn-default btn-flat" data-toggle="modal" data-target="#adminregist"><?php echo $lang[29] ?></a></h3>
				<div class="box-tools"></div>
	        </div><!-- /.box-header -->
	        <div class="box-body table-responsive no-padding">
	            <table id="datatable" class="table table-hover">
	                <thead>
	                    <tr>
	                       <th><?php echo $lang[63] ?></th>
	                        <th><?php echo $lang[175] ?></th>
	                        <th><?php echo $lang[176] ?></th>
	                        <th><?php echo $lang[179] ?></th>
	                        <th><?php echo $lang[180] ?></th>
	                        <th><?php echo $lang[181] ?></th>
                            <th>Action</th>
	                    </tr>
	                </thead>
	                <tbody>
	                <?php 
	                if ($agents) {
	                	foreach ($agents as $agent) {
	                ?>
	                    <tr>
	                      <td>  <?php echo $agent['user_id']?></td>
	                        <td><?php echo $agent['user_name']?></td>
	                         <td><?php echo $agent['user_fullname']?></td>
	                        <td><?php echo $agent['user_email']?></td>
	                        <td><?php echo date('m-d-Y', $agent['user_lastlogin'])?></td>
	                        <td><?php echo date('m-d-Y', strtotime($agent['user_registered']))?></td>
                            <td><a href="<?php echo $baseurl ?>/admin/users/agentassign/?action=edit&user_id=<?php echo $agent['user_id']?>">Edit</a> | 
                            <a href="<?php echo $baseurl ?>/admin/users/agentassign/?action=chanpass&user_id=<?php echo $agent['user_id']?>">Change password</a></td>
							
	                    </tr>
	                <?php 
	                	} // foreach
	                } else { // if 
	                ?>
	                    <tr>
	                        <td>&nbsp;</td>
	                        <td>&nbsp;</td>
	                        <td>&nbsp;</td>
	                        <td>&nbsp;</td>
	                        <td>&nbsp;</td>
	                        <td>&nbsp;</td>
                            <td>&nbsp;</td>
	                    </tr>
	                <?php } // else ?>
	                </tbody>
	                <tfoot>
	                    <tr>
	                        <th><?php echo $lang[63] ?></th>
	                        <th><?php echo $lang[175] ?></th>
	                        <th><?php echo $lang[176] ?></th>
	                        <th><?php echo $lang[179] ?></th>
	                        <th><?php echo $lang[180] ?></th>
	                        <th><?php echo $lang[181] ?></th>
                            <th>Action</th>
	                    </tr>
	                </tfoot>
	            </table>
	        </div><!-- /.box-body -->
	    </div><!-- /.box -->
	
    </div>
</div>





<!-- Modal -->
<div class="modal fade" id="adminregist" tabindex="-1" role="dialog" aria-labelledby="adminregist" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
		<div class="form-box" id="login-box">
			<div class="header">Register New Customer</div>
			<form id="form">
		    <div class="body bg-gray">
		        <div class="form-group">
		            <input type="text" id="name" class="form-control" placeholder="Full name" required>
		        </div>
		        <div class="form-group">
		            <input type="email" id="email" class="form-control" placeholder="E-mail" required>
		        </div>
		        <div class="form-group">
		            <input type="text" id="nick" class="form-control" placeholder="Nickname">
		        </div>
		        <div class="form-group">
		            <input type="password" id="password1" class="form-control" placeholder="Password" required>
		        </div>
		        <div class="form-group">
		            <input type="password" id="password2" class="form-control" placeholder="Retype password" required>
		        </div>
		    </div>
		    <div class="footer">                    
		        <button type="submit" id="signupadmin" class="btn bg-olive btn-block">Sign up</button>
		    </div>
		    <div align="center" id="message"></div>
		    </form>
		</div>

    </div>
  </div>
  

</div>


<?php
$public_key = $config['public_key'];
$time = time();
$games = false;
$hash = md5($public_key . $config['private_key'] . $time);

?> 
<script>
$(document).ready(function() {
	$("#form").bind("submit", manualValidate);
	function manualValidate(e) {
		e.preventDefault();
		e.target.checkValidity();
		var name = $('#name').val();
		var email = $('#email').val();
		var nick = $('#nick').val();
		var t = "<?php echo $time;?>";
		var p1 = $('#password1').val();
		var p2 = $('#password2').val();
		var url = "<?php echo $baseurl ?>";
		var key = "<?php echo $public_key?>";
		var hash = "<?php echo $hash?>";
		url = url + "/admin/users/agentassign/addnewagent.php";
		var uri = 'hash=' + hash + '&public=' + key + '&t=' + t;
		uri += '&name=' + name + '&email=' + email + '&p1=' + p1 + '&p2=' + p2 + '&nick=' + nick;

		if (p1 !== p2) {
			$('#message').html('Password not matched');
			setTimeout(function() {
				$('#message').html('');
			}, 2000);
		}
		$.ajax({
			type: 'POST',
			url: url,
			beforeSend: function(x) {
				if(x && x.overrideMimeType) {
					x.overrideMimeType("application/json;charset=UTF-8");
				}
			},
			data: uri,
			success: function(data){
				
				if (data.status === 'success') {
					location.reload();
				} else {
					$('#message').html(data.status);
					setTimeout(function() {
						$('#message').html('');
					}, 2000);
				}
			}
			
		});
	}
})
</script>
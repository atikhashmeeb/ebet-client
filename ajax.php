<?php
require_once('include/config.php');

require_once($basedir . "/include/functions.php");

if($_POST['action']=='makeregister')
{
			$data = array();
			$private_key = $config['private_key'];
			
			$hash = (isset($_POST['hash'])) ? $_POST['hash'] : 0;
			$public_key = (isset($_POST['public'])) ? $_POST['public'] : 0;
			$time = (isset($_POST['t'])) ? $_POST['t'] : 0;
			
			
			$myhash = md5($public_key . $private_key . $time);
			
			if ($hash != $myhash) {
			echo json_encode(array('error' => 1, 'status' => 'Hash is invalid'));
			exit;
			}
			
			$email = (isset($_POST['user_email'])) ? $_POST['user_email'] : false;
			$username = (isset($_POST['user_fullname'])) ? $_POST['user_fullname'] : false;
			$password = (isset($_POST['user_password'])) ? $_POST['user_password'] : false;
			
			$data = registerUser(htmlspecialchars($email), htmlspecialchars($username), htmlspecialchars($password));
			
			if ($data == 'success') {
			$newdata = array();
			$all_users = getAllUsers();
			foreach($all_users as $au) {
				$newdata[$au['user_id']] = $au;
			}
			recreateCache('all_users.txt', $newdata);
			
			echo json_encode(array('error' => 0, 'status' => $data)); // 'success'
			exit;
			} else {
			echo json_encode(array('error' => 1, 'status' => $data));
			exit;
			}

		
}
elseif($_POST['action']=='makelogin')               
 {   	
 
 		$data = array();
$private_key = $config['private_key'];

$hash = (isset($_POST['hash'])) ? $_POST['hash'] : 0;
$public_key = (isset($_POST['public'])) ? $_POST['public'] : 0;
$time = (isset($_POST['t'])) ? $_POST['t'] : 0;


$myhash = md5($public_key . $private_key . $time);

if ($hash != $myhash) {
	echo json_encode(array('error' => 1, 'status' => 'Hash is invalid'));
	exit;
}

$email = (isset($_POST['login_username'])) ? $_POST['login_username'] : false;
$pword = (isset($_POST['p'])) ? $_POST['p'] : false;
$ksi = (isset($_POST['ksi'])) ? $_POST['ksi'] : false;

$data = loginUsername(htmlspecialchars($email), htmlspecialchars($pword));



$result = mysqli_query($con, "SELECT * FROM users WHERE user_id = '".$data['user_id']."' ");
$iBetDet = mysqli_fetch_array($result);
					
					
if($iBetDet['user_status']=='0')
			{
				
				echo json_encode(array('error' => 1, 'status' => 'authentication failure'));
				exit;
			}

	if ($data) {
		$_SESSION['user_id'] = $data['user_id'];
		$_SESSION['user_name'] = $data['user_name'];
		$_SESSION['user_email'] = $data['user_email'];
		$_SESSION['user_fullname'] = $data['user_fullname'];
		$_SESSION['user_coins'] = $data['user_coins'];
		$_SESSION['user_password'] = $data['user_password']; 
		$_SESSION['user_lang'] = $data['user_lang']; 
		$_SESSION['user_pic'] = $data['user_pic'];
		
		
		$q = "INSERT INTO  login_log (userid, ip_address, create_at) VALUES ('".$data['user_id']."', '".get_client_ip()."', '".date("Y-m-d H:i:s")."')";
		mysqli_query($con,$q);
	
		if ($ksi == 'true') {
			setcookie("keep_signed_in", '1', time() + (86400*100), '/'); // 100 days 
			setcookie("email", $data['user_email'], time() + (86400*100), '/'); // 100 days 
			setcookie("password", $pword, time() + (86400*100), '/'); // 100 days 
		} else {
			setcookie("keep_signed_in", '0', time() + (86400*100), '/'); // 100 days 
		}
	
		echo json_encode(array('error' => 0, 'status' => 'success', 'user_isadmin' => $data['user_isadmin']));
		exit;
	} else {
		echo json_encode(array('error' => 1, 'status' => 'authentication failure'));
		exit;
	}
 }
?>
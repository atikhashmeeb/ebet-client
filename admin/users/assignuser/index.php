<?php
require_once("../../../include/config.php");
require_once($basedir . "/admin/include/functions.php");
include $basedir . '/admin/include/isadmin.php';

if($_SESSION['success']!="")
{
	$error_msg=$_SESSION['success'];
	unset($_SESSION['success']);
}

$customers = getCustomers();
$usermenu='active';
$datatables='active'; 

if(isset($_POST['select_customer']))
{
	$Delsql="DELETE from assign_customer where agent_id='".$_POST['agent_id']."'";
	$row=mysqli_query($con,$Delsql);
	
	foreach($_POST['customer_id'] as $key=>$val)
	{
		$Sql="insert into assign_customer (agent_id,customer_id) values ('".$_POST['agent_id']."','".$_POST['customer_id'][$key]."')";
		$Result=mysqli_query($con,$Sql);
	}
	header('location: '.$baseurl.'/admin/users/assignuser');
	$_SESSION['success']="Updated Successfully";
}


if(isset($_POST['submitform']))
{
	
	
	foreach($_POST['customer_id'] as $key=>$val)
	{
		$Sql="insert into assign_customer (agent_id,customer_id) values ('".$_POST['agent_id']."','".$_POST['customer_id'][$key]."')";
		$Result=mysqli_query($con,$Sql);
	}
	header('location: '.$baseurl.'/admin/users/assignuser');
	$_SESSION['success']="Submit Successfully";
}

if(isset($_POST['update']))
{
	
	 $sql="UPDATE users set user_name='".$_POST['user_name']."',user_fullname='".$_POST['user_fullname']."', user_email='".$_POST['user_email']."', user_password='".$_POST['user_password']."' where user_id='".$_GET['user_id']."'";
	$result=mysqli_query($con,$sql);
	header('location:'.$baseurl.'/admin/users/customers?lang='.$LANGUAGE);	
}	

$Sql = mysqli_query($con,"select * from users where user_id='".$_GET['user_id']."'"); 
$Row = mysqli_fetch_array($Sql);
?>
<!DOCTYPE html>
<html>
<head>
<?php include $basedir . '/admin/include/header.php'; ?>
</head>

<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<header class="header">
  <?php include $basedir . '/admin/include/header_menu.php'; ?>
</header>
<!-- ./header -->

<div class="wrapper row-offcanvas row-offcanvas-left"> 
  
  <!-- Left side column. -->
  <aside class="left-side sidebar-offcanvas"> 
    
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <?php include $basedir . '/admin/include/sidebar.php'; ?>
    </section>
    <!-- /.sidebar --> 
    
  </aside>
  <!-- /.left-side --> 
  
  <!-- Right side column -->
  <aside class="right-side"> 
    
    <!-- Header Nav (breadcrumb) -->
    <section class="content-header">
      <h1>User Assignment<small>assign users to agent</small></h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo $baseurl ?>/"><i class="fa fa-dashboard"></i><?php echo $lang[27] ?></a></li>
        <li><a href="<?php echo $baseurl ?>/users"><?php echo $lang[14] ?></a></li>
        <li class="active"><?php echo $lang[17] ?></li>
      </ol>
    </section>
    <!-- /.content-header --> 
    
    <!-- Main content -->
    <?php if($_GET['action']=='edit') { ?>
    <section class="content">
      <?php include $basedir . '/admin/users/assignuser/editcustomer.php'; ?>
    </section>
    <?php }  if($_GET['action']=='add') { ?>
    <section class="content">
      <?php include $basedir . '/admin/users/assignuser/addcustomer.php'; ?>
    </section>
    <?php }  else { ?>
    <section class="content">
      <?php include $basedir . '/admin/users/assignuser/customerlist.php'; ?>
    </section>
    <?php } ?>
    <!-- /.content --> 
    
  </aside>
  <!-- /.right-side --> 
  
</div>
<!-- ./wrapper -->

<?php include $basedir . '/include/javascript.php'; ?>
</body>
</html>
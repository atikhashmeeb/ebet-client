<?php
require_once("../../../include/config.php");
require_once($basedir . "/admin/include/functions.php");
include $basedir . '/admin/include/isadmin.php';

$iFileName='agentuserassign.php';


if($_SESSION['success']!="")
{
	$success_msg=$_SESSION['success'];
	unset($_SESSION['success']);
}

if(isset($_POST['updaterecord']))
{
	$Delsql="DELETE from assign_agent_customer where agent_id='".$_POST['agent_id']."'";
	$row=mysqli_query($con,$Delsql);
	
	foreach($_POST['customer_id'] as $key=>$val)
	{
		$Sql="insert into assign_agent_customer (agent_id,customer_id) values ('".$_POST['agent_id']."','".$_POST['customer_id'][$key]."')";
		$Result=mysqli_query($con,$Sql);
	}
	 
	$_SESSION['success']="Updated Successfully";
	unset($_POST);
	header('location:'.$iFileName);
	exit;
}


if(isset($_POST['submitform']))
{
		foreach($_POST['customer_id'] as $key=>$val)
		{
			$Sql="insert into assign_agent_customer (agent_id,customer_id) values ('".$_POST['agent_id']."','".$_POST['customer_id'][$key]."')";
			$Result=mysqli_query($con,$Sql);
		}
	 
		$_SESSION['success']="Submit Successfully";
		header('location:'.$iFileName);
		exit;
}

	


?>
<!DOCTYPE html>
<html>
<head>
<?php include $basedir . '/admin/include/header.php'; ?>
</head>
<body class="skin-blue">
<header class="header">
  <?php include $basedir . '/admin/include/header_menu.php'; ?>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left"> 
  <aside class="left-side sidebar-offcanvas"> 
    <section class="sidebar">
      <?php include $basedir . '/admin/include/sidebar.php'; ?>
    </section>
  </aside>
  <aside class="right-side"> 
    <section class="content-header">
      <h1>Assign User To Agent<small>Assign User</small></h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo $baseurl ?>/"><i class="fa fa-dashboard"></i><?php echo $lang[27] ?></a></li>
        <li><a href="<?php echo $baseurl ?>/users"><?php echo $lang[14] ?></a></li>
        <li class="active"><?php echo $lang[17] ?></li>
      </ol>
    </section>
    <?php 
	if($_GET['action']=='add') {
			   $iListOfMyCreatedAgents = mysqli_query($con,"SELECT group_concat(create_agentid ) as MyAgents FROM agent_user WHERE agent_id = '".$_SESSION['user_id']."' LIMIT 0, 1");
			  $MyCreatedAgents = mysqli_fetch_array($iListOfMyCreatedAgents);
		 ?>
    <section class="content">
     <div class="row">
  <div class="col-xs-12">
    <form action="" method="POST">
      <div class="box">
        <div class="box-body"> 
          <!-- text input -->
          <div class="form-group">
            <label>Select Agent</label>
            <select name="agent_id" required  class="form-control" >
              <option value="" >select Agent</option>
              <?php
						   
				   
                    $qry1="SELECT * FROM users WHERE user_isadmin = '5' and  user_id  In (".$MyCreatedAgents['MyAgents'].")";
                    $result1=mysqli_query($con, $qry1);
                    while($row1 = mysqli_fetch_array($result1))
                    {  ?>
              <option value="<?php echo $row1['user_id']; ?>"><?php echo $row1['user_name']; ?></option>
              <?php } ?>
            </select>
          </div>
          <br>
          <!-- textarea -->
          <div class="form-group">
            <label>Select Customer</label>
            <table class="table">
              <?php
$iAssignCustomerDetails = mysqli_query($con,"SELECT group_concat(customer_id) as CustomerId FROM assign_customer WHERE agent_id = '".$_SESSION['user_id']."' LIMIT 0, 1");
$iAssignCustomer = mysqli_fetch_array($iAssignCustomerDetails);				   
				   
				   
				   
                   
			  
			  
                    $qry1="SELECT * FROM users WHERE user_isadmin = '0' and  user_id  In (".$iAssignCustomer['CustomerId'].")";
                    $result1=mysqli_query($con, $qry1);
                    while($row1 = mysqli_fetch_array($result1))
                    {
						
                    $iCurrentlyAssign=mysqli_query($con, "SELECT * FROM assign_agent_customer WHERE customer_id = '".$row1['user_id']."'");
                    $iCurrentlyAgent = mysqli_fetch_array($iCurrentlyAssign);
					
					
					$iCurrentlyAssignAgent=mysqli_query($con, "SELECT * FROM users WHERE user_id = '".$iCurrentlyAgent['agent_id']."'");
                    $iCurrentlyAgentUserName = mysqli_fetch_array($iCurrentlyAssignAgent);						
						  ?>
              <tr>
                <td><input type="checkbox" value="<?php echo $row1['user_id']; ?>" name="customer_id[]" /></td>
                <td>
				 <?php   if($iCurrentlyAgentUserName['user_name']!='') { echo '<a><i class="fa fa-check" aria-hidden="true"></i></a>'; } else { echo ' &nbsp; &nbsp; &nbsp; '; } ?>
					<?php echo $row1['user_name']; ?> </td>
                  <td><b>Agent Assign : </b>  <?php   echo $iCurrentlyAgentUserName['user_name']; ?> <?php   if($iCurrentlyAgentUserName['user_name']!='') { echo '<a><i class="fa fa-check" aria-hidden="true"></i></a>'; } else { echo ' &nbsp; &nbsp; &nbsp; '; } ?> </td>
              </tr>
              <?php } ?>
            </table>
          </div>
          <br>
          <input type="submit" name="submitform" value="Submit" class="btn btn-info">
        </div>
        <!-- /.box-body --> 
      </div>
    </form>
  </div>
  <!-- /.box --> 
  
</div>
    </section>
    <?php }  
	elseif($_GET['action']=='edit') {
		 $qry1123="SELECT * FROM  assign_agent_customer WHERE agent_id  = '".$_GET['user_id']."'";
		$result1123=mysqli_query($con, $qry1123);
		$row1123 = mysqli_fetch_array($result1123);
	
		 ?>
    <section class="content">
      <div class="row">
  <div class="col-xs-12">
    <form action="" method="POST">
      <div class="box">
        <div class="box-body"> 
          <!-- text input -->
          <div class="form-group">
            <label>Select Agent</label>
            <select name="agent_id" required  class="form-control" >
              <option value="" >select Agent</option>
              <?php
                    $qry1121="SELECT DISTINCT create_agentid  FROM agent_user where agent_id ='".$_SESSION['user_id']."'";
            		$result1121=mysqli_query($con, $qry1121);
            		while($row1121 = mysqli_fetch_array($result1121))
            			{  $i=$i+1;
            
					$qry1="SELECT  user_id,user_name FROM users where user_id = '".$row1121['create_agentid']."'";
					$result1=mysqli_query($con, $qry1);
					$row1 = mysqli_fetch_array($result1); ?>
             <option value="<?php echo $row1['user_id']; ?>" <?php if($row1['user_id']==$row1123['agent_id']) { echo 'selected'; } ?>><?php echo $row1['user_name']; ?></option>
              <?php } ?>
            </select>
          </div>
          <br>
          <!-- textarea -->
          <div class="form-group">
            <label>Select Customer</label>
            <table class="table">
              <?php
			  
			  
$iAssignCustomerDetails = mysqli_query($con,"SELECT group_concat(customer_id) as CustomerId FROM assign_customer WHERE agent_id = '".$_SESSION['user_id']."' LIMIT 0, 1");
$iAssignCustomer = mysqli_fetch_array($iAssignCustomerDetails);


                    $qry1="SELECT * FROM users WHERE user_isadmin = '0' and user_id In (".$iAssignCustomer['CustomerId'].")";
                    $result1=mysqli_query($con, $qry1);
                    while($row1 = mysqli_fetch_array($result1))
                    { 
					
					
					 $qry1ab="SELECT * FROM assign_agent_customer WHERE customer_id = '".$row1['user_id']."' and agent_id = '".$row1123['agent_id']."'";
                    $result1ab=mysqli_query($con, $qry1ab);
                    $row1ab = mysqli_fetch_array($result1ab);
					
					
					
				 
                    $iCurrentlyAssign=mysqli_query($con, "SELECT * FROM assign_agent_customer WHERE customer_id = '".$row1['user_id']."'");
                    $iCurrentlyAgent = mysqli_fetch_array($iCurrentlyAssign);
					
					
					$iCurrentlyAssignAgent=mysqli_query($con, "SELECT * FROM users WHERE user_id = '".$iCurrentlyAgent['agent_id']."'");
                    $iCurrentlyAgentUserName = mysqli_fetch_array($iCurrentlyAssignAgent);
					
					 ?>
              <tr>
                <td><input type="checkbox" value="<?php echo $row1['user_id']; ?>" <?php if($row1ab['id']!='') { echo 'checked'; } ?> name="customer_id[]" /></td>
                <td> <?php   if($iCurrentlyAgentUserName['user_name']!='') { echo '<a><i class="fa fa-check" aria-hidden="true"></i></a>'; } else { echo ' &nbsp; &nbsp; &nbsp; '; } ?>  <?php echo $row1['user_name']; ?></td>
                
                 <td><b>Agent Assign : </b> <?php   echo $iCurrentlyAgentUserName['user_name']; ?>  <?php   if($iCurrentlyAgentUserName['user_name']!='') { echo '<a><i class="fa fa-check" aria-hidden="true"></i></a>'; } else { echo ' &nbsp; &nbsp; &nbsp; '; } ?> </td>
              </tr>
              <?php } ?>
            </table>
          </div>
          <br>
          <input type="submit" name="updaterecord" value="Submit" class="btn btn-info">
        </div>
        <!-- /.box-body --> 
      </div>
    </form>
  </div>
  <!-- /.box --> 
  
</div>
    </section>
    <?php }  	
	else { ?>
    <section class="content">
    
    <div class="box-header">
				<h3 class="box-title">
                <a href="<?php echo $iFileName ?>?action=add" class="btn btn-default btn-flat">Assign Agent</a></h3>
				<div class="box-tools"></div>
	        </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <form action="" method="POST">
              <div class="box-body table-responsive no-padding">
                <table id="datatable" class="table table-hover">
                  <thead>
                    <tr>
                      <th><?php echo $lang[63] ?></th>
                      <th>Agents</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                   <?php	$i=0;
				   
$iListOfMyCreatedAgents = mysqli_query($con,"SELECT group_concat(create_agentid ) as MyAgents FROM agent_user WHERE agent_id = '".$_SESSION['user_id']."' LIMIT 0, 1");
$MyCreatedAgents = mysqli_fetch_array($iListOfMyCreatedAgents);				   
				   
				   
				   
                    $qry1="SELECT DISTINCT agent_id FROM assign_agent_customer where   agent_id  In (".$MyCreatedAgents['MyAgents'].")";
                    $result1=mysqli_query($con, $qry1);
                    while($row1 = mysqli_fetch_array($result1))
                    {  $i=$i+1;
					
					$qry11="SELECT  user_name FROM users where user_id = '".$row1['agent_id']."'";
                    $result11=mysqli_query($con, $qry11);
                    $row11 = mysqli_fetch_array($result11);
					
					
					?>
              <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $row11['user_name']?></td>
                <td><a href="<?php echo $iFileName ?>?action=edit&user_id=<?php echo $row1['agent_id']; ?>"  class="table-action-btn" > <i class="fa fa-pencil"></i> </a></td>
              </tr>
              <?php  	} // foreach   ?>
                  </tbody>
                </table>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <?php } ?>
  </aside>
</div>
<?php include $basedir . '/include/javascript.php'; ?>
</body>
</html>
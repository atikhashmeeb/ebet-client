<?php

require_once("../../../include/config.php");
require_once($basedir . "/admin/include/functions.php");
include $basedir . '/admin/include/isadmin.php';
$agents = getAssignUser();
$usermenu='active';
$datatables='active';

?>
<!DOCTYPE html>
<html>
<head>
<?php include $basedir . '/admin/include/header.php'; ?>
</head>
<body class="skin-blue">
<header class="header">
  <?php include $basedir . '/admin/include/header_menu.php'; ?>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <aside class="left-side sidebar-offcanvas">
    <section class="sidebar">
      <?php include $basedir . '/admin/include/sidebar.php'; ?>
    </section>
  </aside>
  <aside class="right-side">
    <section class="content-header">
      <h1>Users<small>user list page</small></h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo $baseurl ?>/"><i class="fa fa-dashboard"></i><?php echo $lang[27] ?></a></li>
        <li><a href="<?php echo $baseurl ?>/users"><?php echo $lang[14] ?></a></li>
        <li class="active"><?php echo $lang[17] ?></li>
      </ol>
    </section>
    <section class="content">
    
      <div class="tabs-sections" style="height:-webkit-fill-available">
     
        <div class="tabs-sections__section  tabs-sections__section--active  tab-js  tab-active-js">
       
          <div class="box-body box  no-padding">
           

           <div class="box-header">
		 
				<div class="box-tools"></div>
	        </div><!-- /.box-header -->

            <table id="datatableabcde" class="table table-hover">
              <thead>
                <tr>
                  <th>S No.</th>
                  <th>ID</th>
                  <th>Username</th>
         
                 <th>Status</th>
                 <th>Credit</th>
                 <th>Int Limit</th>
                 <th>Pending</th>
                 <th>Balance</th>
                 <th>Last Wager</th>
                </tr>
              </thead>
              <tbody>
                <?php 

$iAssignCustomerDetails = mysqli_query($con,"SELECT group_concat(customer_id) as CustomerId FROM assign_customer WHERE agent_id = '".$_SESSION['user_id']."' LIMIT 0, 1");
$iAssignCustomer = mysqli_fetch_array($iAssignCustomerDetails);


if($_GET['active']=='player') {
	
$current_start_week = strtotime('monday this week');
$current_end_week 	= strtotime('sunday this week');
$current_start_week = date("Y-m-d",$current_start_week);
$current_end_week = date("Y-m-d",$current_end_week);
$current_start_week.'-------------'.$current_end_week;	
	
	
$UserIdComma='';			
$iTotalAmountSpendLastWeek = mysqli_query($con,"SELECT DISTINCT userid FROM bettingdetail WHERE  status!='0' and userid In (".$iAssignCustomer['CustomerId'].") and DATE(create_at) between '".$current_start_week."' and '".$current_end_week."'");
while($iTotAmtSpdLstWek = mysqli_fetch_array($iTotalAmountSpendLastWeek))  {
				$UserIdComma .= $iTotAmtSpdLstWek['userid']. ' ,';
	}
			$UserIdNewComma =  rtrim($UserIdComma,',') ;
			
	$iThisParticularUserDetail=mysqli_query($con, "SELECT * FROM   users WHERE user_status!='10' and user_isadmin = '0' and user_id IN ($UserIdNewComma) order by user_id desc");			
				
}
else {
		$iThisParticularUserDetail=mysqli_query($con, "SELECT * FROM   users WHERE user_status!='10' and user_isadmin = '0' order by user_id desc");

}
				
				
					$i=0;
					while($iTssUser = mysqli_fetch_assoc($iThisParticularUserDetail)) {	$i=$i+1;
 					 
		/*	 $qrny ="SELECT * FROM  users WHERE user_id = '".$iTssUser['user_id']."'";
			 $ThisGetUsid=mysqli_query($con, $qrny);
			 $iTssUser = mysqli_fetch_assoc($ThisGetUsid);*/
			 
			 $iBettingDetail=mysqli_query($con, "SELECT * FROM  bettingdetail WHERE userid = '".$iTssUser['user_id']."' order by ID desc");
			 $iBetDet = mysqli_fetch_assoc($iBettingDetail);
	                ?>
                <tr>
                	<td><?php echo $i; ?></td>
                   <td><a href="index.php?action=edit&user_id=<?php echo $iTssUser['user_id']	?>">USER <?php echo $iTssUser['user_id']	?></a></td>
                   <td> <?php echo $iTssUser['user_name']	?></td>
                   <td> <?php if($iTssUser['user_status']=='1') { echo 'Open'; } else {  echo 'Closed'; }	?></td>
                    <td><?php echo $iBetDet['user_current_credit_limit']; ?></td>
                    <td><?php echo $iBetDet['user_current_max_limit']; ?></td>
                    <td><?php echo $iBetDet['user_current_pending']; ?></td>
                    <td><?php echo $iBetDet['user_current_balance']; ?></td>
                    <td><?php echo $iBetDet['create_at']; ?></td>
                </tr>
                <?php }  ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </section>
  </aside>
</div>

<!-- Modal -->
 

<?php include $basedir . '/include/javascript.php'; ?>
<script type="text/javascript">
    $(function() {
        $('#searchtable').dataTable({});
        $('#datatableabcde').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": true
        });
    });
</script>



<?php
$public_key = $config['public_key'];
$time = time();
$games = false;
$hash = md5($public_key . $config['private_key'] . $time);

?> 
<script>
function admingetregister()
 {	
	var user_fullname = document.getElementById('user_fullname').value;
	var user_name = document.getElementById('user_name').value; 
	var user_email = document.getElementById('user_email').value;
	var password1 = document.getElementById('password1').value;
	var password2 = document.getElementById('password2').value;
	  
	var t = "<?php echo $time;	?>";
	var key = "<?php echo $public_key	?>";
	var hash = "<?php echo $hash	?>";
		
	if (user_fullname=='') {
        document.getElementById("reg_showdata").innerHTML = '<p class="sherorno">Please enter full name.</p>';
    } 
	else if (user_email=='') {
        document.getElementById("reg_showdata").innerHTML = '<p class="sherorno">Please enter email. </p>';
    }
	else if (user_name=='') {
        document.getElementById("reg_showdata").innerHTML = '<p class="sherorno">Please enter username. </p>';
    }
	
	else if (password1=='') {
        document.getElementById("reg_showdata").innerHTML = '<p class="sherorno">Please enter password. </p>';
    } 
	else if (password2=='') {
        document.getElementById("reg_showdata").innerHTML = '<p class="sherorno">Please re-enter password. </p>';
    } 
	
	else if (password1!=password2) {
        document.getElementById("reg_showdata").innerHTML = '<p class="sherorno">Password do not match.</p>';
    } 	

	else
		{
			$.post("ajax.php",
			{
						action			:	"makeregister",
						user_fullname	:	user_fullname,
						user_email    	: 	user_email,
						user_name    	: 	user_name,
						password1		: 	password1,
						t				:   t, 
						public			:   key, 
						hash			:   hash, 
			},
			function(data){  
						
						
					var data = JSON.parse(data);	
					
					if (data.error == 'success') {
					
					  	document.getElementById('reg_showdata').innerHTML='<p>Register Successfully</p>';
						 document.getElementById('registerform').reset();
						
					}
					else if (data.error == 'email_exit') {
					
					 	 document.getElementById('reg_showdata').innerHTML='<p>This email id already register</p>';
						
					}
					else {
						
						 document.getElementById('reg_showdata').innerHTML='<p>This username already register</p>';
					
					}

				 });
			}
 }
 </script>
</body>
</html>

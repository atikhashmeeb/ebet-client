<?php
require_once("../../../include/config.php");
require_once($basedir . "/admin/include/functions.php");
include $basedir . '/admin/include/isadmin.php';
$agents = getAssignUser();
$usermenu='active';
$datatables='active';


?>
<!DOCTYPE html>
<html>
<head>
<?php include $basedir . '/admin/include/header.php'; ?>
</head>
<body class="skin-blue">
<header class="header">
  <?php include $basedir . '/admin/include/header_menu.php'; ?>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <aside class="left-side sidebar-offcanvas">
    <section class="sidebar">
      <?php include $basedir . '/admin/include/sidebar.php'; ?>
    </section>
  </aside>
  <aside class="right-side">
    <section class="content-header">
      <h1><?php echo $lang[17] ?><small>Distribution Log</small></h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo $baseurl ?>/"><i class="fa fa-dashboard"></i><?php echo $lang[27] ?></a></li>
        <li><a href="<?php echo $baseurl ?>/users/agent">Agent</a></li>
        <li class="active">Distribution Log></li>
      </ol>
    </section>
    <section class="content">

      <div class="tabs-sections" style="height:-webkit-fill-available">
        <div class="tabs-sections__section  tabs-sections__section--active  tab-js  tab-active-js">
    
          <div class="box-body box  no-padding">
          
             <?php   	if($iLoginRow['user_isadmin']=='1'){	?>
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>Week</th>
                 
                   <th>Week Total</th>
                 <th>Agent Total</th>
                  <th>Active Player</th>
                </tr>
              </thead>
              <tbody>
              
            <?php 
						$iklymn = 0;
						$iTotAMounNew=0;
						  for($i=0; $i<6; $i++) {
							$previous_week = strtotime("-$i week +$i day");
							$start_week = strtotime("last sunday midnight",$previous_week);
							$end_week = strtotime("next saturday",$start_week);
							$start_week = date("Y-m-d",$start_week);
							$end_week = date("Y-m-d",$end_week);
		 			  
$iThisParticularUserDetail=mysqli_query($con, "SELECT  SUM(total_amount) as TotalAmount , SUM(totalplayer) as TotalPlayer , create_date , userid  FROM  agent_rate WHERE     create_date between '".$start_week."' and '".$end_week."' and userid!='' order by id desc");




				 
					
					while($iTHisUser = mysqli_fetch_assoc($iThisParticularUserDetail)) {
						
						if($iTHisUser['TotalAmount']!='')	{
					$iklymn = $iklymn+1;
					 $iAgentId=mysqli_query($con, "SELECT user_name  FROM  users WHERE user_id = '".$iTHisUser['userid']."'");
					 $iAgent = mysqli_fetch_assoc($iAgentId);
	                ?>
                <tr>
                  <td>Week <?php echo $iklymn; //echo $start_week .'-'.$end_week ;	?></td>
                
                  <td><?php echo $iTHisUser['TotalAmount']	?></td>
                  
                  <td><?php $iTotAMounNew += $iTHisUser['TotalAmount'];
				   echo $iTotAMounNew;	?></td>
                  
                  
                  
                  
                  <td><?php echo $iTHisUser['TotalPlayer']	?></td>
                  
                   
                </tr>
                <?php } 	}
				
				 } 
				?>
                 </tbody>
            </table>
            
            
            <?php  }	else {		?>
                
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>Week</th>
                
                  <th>Rate</th>
                   <th>Week Total</th>
                    <th>Agent Total</th>
                  <th>Active Player</th>
                </tr>
              </thead>
              <tbody>
              
              <?php			$iklymn = 0;
			 				$iTotAMounNew=0;
			   					//////////////// This is For Agent
			 				  for($i=0; $i<6; $i++) {
							$previous_week = strtotime("-$i week +$i day");
							$start_week = strtotime("last sunday midnight",$previous_week);
							$end_week = strtotime("next saturday",$start_week);
							$start_week = date("Y-m-d",$start_week);
							$end_week = date("Y-m-d",$end_week);
		 			  
				
				
				
				 
$iThisParticularUserDetail=mysqli_query($con, "SELECT userid, players_id, rate, total_amount ,totalplayer  FROM  agent_rate WHERE  userid = '".$_SESSION['user_id']."' and   create_date between '".$start_week."' and '".$end_week."' order by id desc");
				 
					
					
					while($iTHisUser = mysqli_fetch_assoc($iThisParticularUserDetail)) {
					$iklymn = $iklymn+1;
					 $iAgentId=mysqli_query($con, "SELECT user_name  FROM  users WHERE user_id = '".$iTHisUser['userid']."'");
					 $iAgent = mysqli_fetch_assoc($iAgentId);
	                ?>
                <tr>
                  <td> <?php echo $start_week  ;	?></td>
            
                  <td><?php echo $iTHisUser['rate']	?></td>
                  <td><?php echo $iTHisUser['total_amount']	?></td>
                   <td><?php $iTotAMounNew += $iTHisUser['total_amount'];
				   echo $iTotAMounNew;	?></td>
                  <td><?php echo $iTHisUser['totalplayer']	?></td>
                  
                   
                </tr>
                <?php } 
				
				 } 
				
				?>
              </tbody>
            </table>
                <?php  }	?>
          </div>
        </div>
      </div>
    </section>
  </aside>
</div>
<?php include $basedir . '/include/javascript.php'; ?>
</body>
</html>

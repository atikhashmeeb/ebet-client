<?php
require_once("../../../include/config.php");
require_once($basedir . "/admin/include/functions.php");
include $basedir . '/admin/include/isadmin.php';
$agents = getAssignUser();
$usermenu='active';
$datatables='active';

if($_GET['week']=='previous') {

$previous_week = strtotime("-1 week +1 day");
$start_week = strtotime("last monday midnight",$previous_week);
$end_week = strtotime("next sunday",$start_week);
$start_week = date("Y-m-d",$start_week);
$end_week = date("Y-m-d",$end_week);
$current_start_week = $start_week;
$current_end_week 	= $end_week;
$current_start_week.'-------------'.$current_end_week;
} else { 
$current_start_week = strtotime('monday this week');
$current_end_week 	= strtotime('sunday this week');
$current_start_week = date("Y-m-d",$current_start_week);
$current_end_week = date("Y-m-d",$current_end_week);
$current_start_week.'-------------'.$current_end_week;
}
?>
<!DOCTYPE html>
<html>
<head>
<?php include $basedir . '/admin/include/header.php'; ?>
</head>
<body class="skin-blue">
<header class="header">
  <?php include $basedir . '/admin/include/header_menu.php'; ?>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <aside class="left-side sidebar-offcanvas">
    <section class="sidebar">
      <?php include $basedir . '/admin/include/sidebar.php'; ?>
    </section>
  </aside>
  <aside class="right-side">
    <section class="content-header">
      <h1><?php echo $lang[17] ?><small><?php echo $lang[31] ?></small></h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo $baseurl ?>/"><i class="fa fa-dashboard"></i><?php echo $lang[27] ?></a></li>
        <li><a href="<?php echo $baseurl ?>/users"><?php echo $lang[14] ?></a></li>
        <li class="active"><?php echo $lang[17] ?></li>
      </ol>
    </section>
    <section class="content">
      <?php include('miniheader.php'); ?>
      <div class="tabs-sections" style="height:-webkit-fill-available">
        <div class="tabs-sections__section  tabs-sections__section--active  tab-js  tab-active-js">
          <div class="box-body box  no-padding">
          
            <table style="width:300px" class="table table-hover">
          	<tr><td><a href="agentweekly.php?user_id=<?php echo $_GET['user_id']; ?>" class="btn  btn-default">Current week</a></td>
            <td> <a  href="agentweekly.php?user_id=<?php echo $_GET['user_id']; ?>&week=previous"  class="btn  btn-default">Previous Week</a></td>
            </tr>
            </table>
            <table id="datatable" class="table table-hover">
              <thead>
                <tr>
                  <th>S No.</th>
                  <th>Username</th>
                  <th>Rate</th>
                 
                </tr>
              </thead>
              <tbody>
                <?php 
				
				$qry1123="SELECT * FROM  users WHERE user_id = '".$_GET['user_id']."'";
				$result1123=mysqli_query($con, $qry1123);
				$row1123 = mysqli_fetch_array($result1123);
				
				$iAssignCustomerDetails = mysqli_query($con, "SELECT group_concat(customer_id) as CustomerId FROM assign_customer WHERE agent_id = '".$row1123['user_id']."' LIMIT 0, 1");
				$iAssignCustomer = mysqli_fetch_array($iAssignCustomerDetails);
				
				$iTotalAmountSpendLastWeek = mysqli_query($con,"SELECT DISTINCT userid FROM bettingdetail WHERE status!='0' and userid In (".$iAssignCustomer['CustomerId'].") and DATE(create_at) between '".$current_start_week."' and '".$current_end_week."'");
				$iTotalUserCOunt=mysqli_num_rows($iTotalAmountSpendLastWeek);
				
					$i=0;
					while($iTHisUser = mysqli_fetch_array($iTotalAmountSpendLastWeek)) {
						
						
					 $i=$i+1;;
					 
					 $iAgentId=mysqli_query($con, "SELECT * FROM  users WHERE user_id = '".$iTHisUser['userid']."'");
					 $iAgent = mysqli_fetch_assoc($iAgentId);
	                ?>
                <tr>
                  <td><?php echo $i	?></td>
                  <td><?php echo $iAgent['user_name']	?></td>
                  <td><?php echo $row1123['rate']	?></td>
                </tr>
                <?php }  ?>
              </tbody>
            </table>
            <table class="table table-hover"><tr><td><b>Weekly Total : <?php echo $i*$row1123['rate']	?></b></td></tr></table>
          </div>
        </div>
      </div>
    </section>
  </aside>
</div>
<?php include $basedir . '/include/javascript.php'; ?>
</body>
</html>

<?php
require_once('../include/config.php');
require_once("../include/functions.php");
require_once('../include/user_functions.php');
if ($_SESSION['user_id']=='') {
    header('Location: ' . $baseurl . '/log.php');
    exit;
}

$settingsmenu = 'active';
$accountmenu='active';

$def_tz = ($_SESSION['user_timezone']) ? $_SESSION['user_timezone'] : 'Asia/Tokyo';
$languages = getLanguages();

$file = $basedir . '/temp/all_users.txt';
$data = json_decode(file_get_contents($file), true);
?>

<!DOCTYPE HTML>
<html>
<?php include $basedir . '/common/header.php'; ?>
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Rubik">

<style>
    body {
        background: #ededed !important;
        font-family: 'Rubik', serif;
    }

    .container {
        max-width: 1280px;
    }

    main.mypage {
        border: none;
        border-radius: 3px;
    }

    article {
        padding: 10px 20px 10px 20px;
    }

    article h3 {
        float: left;
        font-weight: 600;
    }
    article span {
        float: right;
        cursor: pointer;
    }

    article span a {
        text-decoration: none;
    }

    article span a:hover {
        font-weight: bold;
    }


    .game_content {
        position: relative;
    }

    .game_content .game_detail {
        width: 20%;
        padding: 0 10px 0 0;
        float: left;
    }

    .game_content .game_area {
        width: 80%;
        background: #fff;
        min-height: 100px;
        float: left;
    }

    .game_detail .info {
        background: #fff;
        min-height: 85vh;
    }

    .game_detail .info img {
        width: 100%;
        height: 125px;
    }

    .game_detail .info {
        font-size: 12px;
    }

    .info .explanation {
        padding: 15px;
    }

    .game_area iframe {
        width: 100%;
        height: 85vh;
    }
</style>

<body>

<div class="container row">

    <main role="main" class="row gutters mypage">
        <article id="myaccount" class="col span_12">
            <h3>Carbbean Stud Pocker</h3>
            <span><a href="<?php echo $baseurl; ?>/casino"> X</a></span>
        </article>
    </main>

    <div class="game_content">
        <div class="game_detail">
            <div class="info">
                <img src="../images/casino/genie_gift.jpg">
                <div class="explanation">
                    <p>No matter what time of day, the outdoor basketball courts are always brimming with fast-pace action in Streetball Star. You can score 243 unique ways simply by landing matching icons on consecutive reels. Beyond that, you can win bonus cash through an urban-style Spray Reels feature and a free spins mode. A stacked wild also dominates space on the reels, resulting in more wins. Think you’ve got what it takes to start a win streak in streetball? Hit the court and show them what you’re made of.</p>
                    <br><h6>Features</h6>
                    <p>Stacked Wild</p>
                    <p>Stacked wilds take up three spots on a reel and substitute for any icon other than scatters.</p>

                    <br><h6>Free Spins Mode</h6>
                    <p>Land at least three free spin scatters side by side on the reels to trigger the game’s free spins mode. Up to 15 free games are available every time.</p>

                    <br><h6>Spray Reels Feature</h6>
                    <p>After winning a payout, the winning icons get a dose of spray paint. New icons will appear in their place, giving you a chance to score another win.</p>
                    <p>Additional Information</p>
                    <p>5 reels.</p>
                </span>
                </div>
            </div>
        </div>
        <div class="game_area">
            <iframe src="<?php echo $baseurl; ?>/game/caribbean_stud_poker"></iframe>
        </div>
    </div>

</div>

<?php include $basedir . '/common/foot.php'; ?>

<script src="<?php echo $baseurl?>/js/jquery-1.11.0.min.js"></script>
<script src="https://ebet21.com/oldwebsite/php/Assets/JS/Index_JS.js"></script>
<script src="<?php echo $baseurl?>/js/jquery.remodal.js"></script>
<script type="text/javascript" src="<?php echo $baseurl?>/js/main_frontend.js"></script>
<script src="<?php echo $baseurl?>/js/jquery.minimalect.min.js"></script>

</body>

</html>
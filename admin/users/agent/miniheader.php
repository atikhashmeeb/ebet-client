<style>
.tab-js { display: none; }
.tab-active-js { display: block; }
.tabs-header {
  font-size: 25px;
  text-align: center;
  margin: 3rem auto; }
.centering-layer {
  width: 100%;
  max-width: 800px;
  margin: 2rem auto; }
.tabs-buttons {
  font-size: 14px; }
  .tabs-buttons__btn {
    display: block;
    width: 100%;
    text-decoration: none;
    font-weight: bold;
    border: 2px solid #4286f5;
    border-bottom-width: 0;
    color: #333;
    background-color: #fff;
    outline: none;
    padding: 12px 20px;
    cursor: pointer;
    transition: background-color .3s; }
  .tabs-buttons__btn:hover,
  .tabs-buttons__btn--active {
    color: #fff;
    background-color: #4286f5; }
.tabs-sections {
  padding: 5px 5px;
  border: 2px solid #4286f5; }



  .tabs-buttons {
    font-size: 0; }

  .tabs-buttons__btn {
    width: auto;
    display: inline-block;
    font-size: 14px; }

  .tabs-buttons__btn:not(:last-child) {
    margin-right: 5px; }
.navbar-right {
	display: none;
}
</style>

<?php $iFileName = basename($_SERVER['PHP_SELF']);


$ThisGetUserid=mysqli_query($con, "SELECT * FROM  users WHERE user_id = '".$_GET['user_id']."'");
$iTHisUSer = mysqli_fetch_array($ThisGetUserid);



$iPendingBettingDetails=mysqli_query($con, "SELECT SUM(bettingwin) As Wintotal FROM  bettingdetail WHERE userid = '".$_GET['user_id']."' and status!='0' and result='0' order by id desc");
$iPendBet = mysqli_fetch_array($iPendingBettingDetails);

?>


<table style="width:100%; background:#fff; padding:20px; height:50px;">
	<tr>
    <td style="text-align:center"><b>Agent Name <?php echo $iTHisUSer['user_name']; ?></b></td>
    <td style="text-align:center"><b>Agent Total : <?php echo $iTHisUSer['agenttotal']; ?> </b></td>	
 
    </tr>
 </table>
<br /><br />
<div class="tabs-buttons">
<a href="editagent.php?action=edit&user_id=<?php echo $_GET['user_id']; ?>" class="tabs-buttons__btn  <?php if($iFileName=='editagent.php') { echo 'tabs-buttons__btn--active'; } ?>  btn-js">Edit Profile</a>


<a href="transcation.php?user_id=<?php echo $_GET['user_id']; ?>" class="tabs-buttons__btn  btn-js <?php if($iFileName=='transcation.php') { echo 'tabs-buttons__btn--active'; } ?>" >Transcation</a>

<!--<a href="agentweekly.php?user_id=<?php echo $_GET['user_id']; ?>" class="tabs-buttons__btn  btn-js <?php if($iFileName=='agentweekly.php') { echo 'tabs-buttons__btn--active'; } ?>" >Agent Weekly</a>-->


<a href="internetlog.php?user_id=<?php echo $_GET['user_id']; ?>" class="tabs-buttons__btn  btn-js <?php if($iFileName=='internetlog.php') { echo 'tabs-buttons__btn--active'; } ?>" >Internet Log</a>

<a href="distributionloging.php?user_id=<?php echo $_GET['user_id']; ?>" class="tabs-buttons__btn  btn-js <?php if($iFileName=='distributionloging.php') { echo 'tabs-buttons__btn--active'; } ?>" > Distribution</a>


<a href="deleteagent.php?user_id=<?php echo $_GET['user_id']; ?>" class="tabs-buttons__btn  btn-js <?php if($iFileName=='deleteagent.php') { echo 'tabs-buttons__btn--active'; } ?>" > Delete Agent</a>
</div>
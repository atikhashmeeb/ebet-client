<?php
require_once('../include/config.php');
require_once("../include/functions.php");
require_once('../include/user_functions.php');
if ($_SESSION['user_id']=='') {
    header('Location: ' . $baseurl . '/log.php');
    exit;
}

$settingsmenu = 'active';
$accountmenu='active';

$def_tz = ($_SESSION['user_timezone']) ? $_SESSION['user_timezone'] : 'Asia/Tokyo';
$languages = getLanguages();

$file = $basedir . '/temp/all_users.txt';
$data = json_decode(file_get_contents($file), true);
?>

<!DOCTYPE HTML>
<html>
<?php include $basedir . '/common/header.php'; ?>
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Rubik">

<style>
    body {
        background: #ededed !important;
        font-family: 'Rubik', serif;
    }

    main.mypage {
        background-color: #525252;
        margin: 10px 0 20px;
    }

    main .casino_menu {
        height: 120px;
    }

    .casino_menu ul {
        list-style:none;
        width:100%;
        height: 100%;
        padding: 0;
        margin: 0;
    }

    .casino_menu ul li {
        text-align: center;
        float:left;
        line-height:25px;
        height:100%;
        list-style-type:none;
        margin:0;
        width: 25%;
        overflow:hidden;
        color: #E2E2E2;
    }

    .casino_menu ul li img {
        margin-top: 20px;
        width: 60px;
        height: 60px;
    }

    .casino_menu ul li a{
        text-decoration: none;
    }

    .casino_menu ul li:hover {
        opacity: 0.6;
    }

    .casino_list {
        margin: 30px auto;
        width: 100%;
        display: grid;
        grid-auto-rows: 165px;
        grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
        grid-gap: 1em;
    }

    .card_item {
        text-align: center;
        border-radius: 3px;
        color: #364250;
        height: 165px;
        padding-bottom: 10px;
        background-color: #fff;
        font-size: 14px;
        /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
        box-shadow: 0 0 20px rgba(115,115,115,0.75);
    }

    .card_item:hover {
        box-shadow: 0 0 40px rgba(115,115,115,0.9);
    }

    a.card_item {
        text-decoration: none;
    }

    .card_item img {
        width: 100%;
        height: 85%;
    }
    
    #game_category {
        font-size: 22px;
        font-weight: 600px;
    }


</style>

<body>

<?php include $basedir . '/common/head.php'; ?>


<div class="container row">

    <?php include $basedir . '/common/myheadmenu.php'; ?>

    <main role="main" class="row gutters mypage">

        <article id="myaccount" class="col span_12">

            <div class="box casino_menu">
                <ul>
                    <li>
                        <a href="#" onclick="viewCategory('blackjack')">
                            <img src="../images/i_blackjack.png" /><br>
                            Blackjack Games
                        </a>
                    </li>
                    <li>
                        <a href="#" onclick="viewCategory('table')">
                            <img src="../images/i_casino.png" /><br>
                            Table Games
                        </a>
                    </li>
                    <li>
                        <a href="#" onclick="viewCategory('slot')">
                            <img src="../images/i_slots.png" /><br>
                            Slot Games
                        </a>
                    </li>
                    <li>
                        <a href="#" onclick="viewCategory('keno')">
                            <img src="../images/i_keno.png" /><br>
                            Keno & Zip Tables
                        </a>
                    </li>
                </ul>
            </div>

        </article>

    </main>
    <h1 id="game_category">All Games</h1>
    <div class="casino_list">
        <a href="<?php echo $baseurl; ?>/casino/caribbean_stud_poker.php" class="card_item table" data-category = "table">
            <img src="../images/casino/carribean_stud.png" />
            <br>Caribbean Stud Pocker
        </a>
        <a href="#" class="card_item blackjack" data-category = "blackjack">
            <img src="../images/casino/jacks_or_better.png" />
            <br>Jacks or Better
        </a>
        <a href="#" class="card_item slot" data-category = "slot">
            <img src="../images/casino/slot_rames.png" />
            <br>Slot Ramses
        </a>
        <a href="#" class="card_item slot" data-category = "slot">
            <img src="../images/casino/slot_mr_chicken.png" />
            <br>Slot Mr Chicken
        </a>
        <a href="#" class="card_item slot" data-category = "slot">
            <img src="../images/casino/slot_arabian.png" />
            <br>Slot Arabian
        </a>
        <a href="#" class="card_item slot" data-category = "slot">
            <img src="../images/casino/slot_3d_soccer.png" />
            <br>3D Soccer Slot
        </a>
        <a href="#" class="card_item slot" data-category = "slot">
            <img src="../images/casino/slot_christmas.png" />
            <br>Slot Christmas
        </a>
        <a href="#" class="card_item table" data-category = "table">
            <img src="../images/casino/3card_pocker.png" />
            <br>3Card Poker
        </a>
        <a href="#" class="card_item table" data-category = "table">
            <img src="../images/casino/baccarat.png" />
            <br>Baccarat
        </a>
        <a href="#" class="card_item table" data-category = "table">
            <img src="../images/casino/craps.png" />
            <br>Craps
        </a>
        <a href="#" class="card_item table" data-category = "table">
            <img src="../images/casino/red_dog.png" />
            <br>Red Dog
        </a>
        <a href="#" class="card_item table" data-category = "table">
            <img src="../images/casino/roulette.png" />
            <br>Roulette
        </a>
        <a href="#" class="card_item table" data-category = "table">
            <img src="../images/casino/joker_poker.png" />
            <br>Joker Poker
        </a>
        <a href="#" class="card_item keno" data-category = "keno">
            <img src="../images/casino/horse_racing.png" />
            <br>Horse Racing
        </a>
        <a href="#" class="card_item keno" data-category = "keno">
            <img src="../images/casino/bingo.png" />
            <br>Bingo
        </a>
    </div>

</div>


<script src="<?php echo $baseurl?>/js/jquery-1.11.0.min.js"></script>
<script src="https://ebet21.com/oldwebsite/php/Assets/JS/Index_JS.js"></script>
<script src="<?php echo $baseurl?>/js/jquery.remodal.js"></script>
<script type="text/javascript" src="<?php echo $baseurl?>/js/main_frontend.js"></script>
<script src="<?php echo $baseurl?>/js/jquery.minimalect.min.js"></script>
<?php include $basedir . '/common/foot.php'; ?>

<script>
    function viewCategory(type) {
        switch (type) {
            case 'blackjack':
                $('.blackjack').css('display', 'block');
                $('.slot').css('display', 'none');
                $('.table').css('display', 'none');
                $('.keno').css('display', 'none');
                $('#game_category').html('Blackjack Games');
                break;
            case 'slot':
                $('.blackjack').css('display', 'none');
                $('.slot').css('display', 'block');
                $('.table').css('display', 'none');
                $('.keno').css('display', 'none');
                $('#game_category').html('Slot Games');
                break;
            case 'table':
                $('.blackjack').css('display', 'none');
                $('.slot').css('display', 'none');
                $('.table').css('display', 'block');
                $('.keno').css('display', 'none');
                $('#game_category').html('Table Games');
                break;
            case 'keno':
                $('.blackjack').css('display', 'none');
                $('.slot').css('display', 'none');
                $('.table').css('display', 'none');
                $('.keno').css('display', 'block');
                $('#game_category').html('Keno Games');
                break;
        }
    }
</script>

</body>

</html>
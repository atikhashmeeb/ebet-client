﻿<?php
require_once('include/config.php');
if($_SESSION['language']=='')
	{
			$_SESSION['language']= 'en';	
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>EBET21</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form">
					
					<span class="login100-form-title p-b-48">
						<img border="0" alt="EBET21" height="140" width="190" src="/images/elogo.png">
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Enter Username">
						<input class="input100" type="text" id="login_username" name="login_username" >
						<span class="focus-input100" data-placeholder="Username"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
						<input class="input100" type="password" id="login_password" name="login_password" >
						<span class="focus-input100" data-placeholder="Password"></span>
					</div>
					
					
					
					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<input class="login100-form-btn" type="button" onclick="getlogin();" value="Secure Login">
							
							</button>



						</div>


						<div class="reg33">
						
         
            <span id="reg_login"> </span>
          
        
					</div>


					</div>

					

					
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>



	<?php
$public_key = $config['public_key'];
$time = time();
$games = false;
$hash = md5($public_key . $config['private_key'] . $time);
?>
<script>
var jq = $.noConflict();
 function getlogin()
 {
	   var login_username = document.getElementById('login_username').value;
	   var user_password = document.getElementById('login_password').value;
	   var t = "<?php echo $time;?>";
		
	   var key = "<?php echo $public_key?>";
	  var hash = "<?php echo $hash?>";
	  jq.post("ajax.php",
	  {   
		   action      		:	'makelogin',
	       login_username       		:	login_username,
		   t				: 	t,
		   public			:	key,
		   hash				:	hash,
	       p    			:	user_password,
		   
	  },
        function(data){ 	
			var data = JSON.parse(data);	
			if (data.status === 'success') {
					 
					if (data.user_isadmin === '1') {
						window.location.href='admin/';
					}
					else if (data.user_isadmin === '2') {
						window.location.href='admin/';
					}
					else if (data.user_isadmin === '5') {
						window.location.href='admin/';
					}
					else	{
						window.location.href='straight.php';
						
					}
			}
			else{
				 document.getElementById('reg_login').innerHTML='<p><b>Invalid Details<b><p>';
			}
		});
 }
 </script>



</body>
</html>
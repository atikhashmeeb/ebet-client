<?php


require_once('include/config.php');
require_once($basedir . "/include/functions.php");
require_once($basedir . '/include/user_functions.php');


if ($_SESSION['user_id'] == '') {
    header('Location: ' . $baseurl . '/log.php');
    exit;
}

$iThisLoginUser = mysqli_query($con, "SELECT * FROM users WHERE user_id = '" . $_SESSION['user_id'] . "'");
$iThisLogUs = mysqli_fetch_array($iThisLoginUser);

if ($iThisLogUs['user_status'] == '2') {
    redirect("insufficentfund.php?action=authorized");
    exit;
}

function RandomStringGenerator($n)
{
    $generated_string = "";
    $domain = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    $len = strlen($domain);
    for ($i = 0; $i < $n; $i++) {
        $index = rand(0, $len - 1);
        $generated_string = $generated_string . $domain[$index];
    }

    return $generated_string;
}
function RandomNumberGenerator($n)
{
    $generated_string = "";
    $domain = "1234567890";
    $len = strlen($domain);
    for ($i = 0; $i < $n; $i++) {
        $index = rand(0, $len - 1);
        $generated_string = $generated_string . $domain[$index];
    }

    return $generated_string;
}

function redirect($url = NULL)
{
    if (is_null($url)) $url = curPageURL();
    if (headers_sent()) {
        echo "<script>window.location='" . $url . "'</script>";
    } else {
        header("Location:" . $url);
    }
    exit;
}


function datasumited($is_away, $is_home, $tokenid, $eventid, $oAnDu, $teamid, $SportName, $AbrNam, $iBetingWin, $BettingCondition, $BettingConditionOrginal, $iRiskAMount, $BettingAmount, $EventDate, $wager, $type, $iTicketNumber)
{
    global $con;

    $iThisLoginUser = mysqli_query($con, "SELECT * FROM users WHERE user_id = '" . $_SESSION['user_id'] . "'");
    $iThisLogUs = mysqli_fetch_array($iThisLoginUser);

    $iThisLoginUserCreditLimit = mysqli_query($con, "SELECT * FROM credit_limit WHERE userid = '" . $_SESSION['user_id'] . "' and type_id	 = '1'");
    $iThisLogUsCreditLimit = mysqli_fetch_array($iThisLoginUserCreditLimit);


    if ($_POST['freeplay'] == '') {
        $iFreePlay = '0';
    } else {
        $iFreePlay = $_POST['freeplay'];
    }


    $q = "INSERT INTO bettingdetail (	
										is_away, 
										is_home, 
										tokenid, 
										o_and_u,
										event_id, 
										team_id, 
										sport_name, 
										abrevation_name, 
										bettingwin, 
										bettingcondition,
										bettingcondition_orginal, 
										riskamount, 
										bettingamount,  
										event_date, 
										wager,
										type,  
										ticketid, 
										userid, 
										create_at,
										status,
										user_current_credit_limit,
										user_current_max_limit,
										user_current_balance,
										result,
										freeplay	) 
	
							VALUES ( 	'" . $is_away . "',
										'" . $is_home . "',
										'" . $tokenid . "',
										'" . $oAnDu . "', 
										'" . $eventid . "', 
										'" . $teamid . "', 
										'" . $SportName . "',  
										'" . $AbrNam . "', 
										'" . abs($iBetingWin) . "', 
										'" . $BettingCondition . "', 
										'" . $BettingConditionOrginal . "', 
										'" . abs($iRiskAMount) . "', 
										'" . $BettingAmount . "', 
										'" . $EventDate . "', 
										'" . $wager . "', 
										'" . $type . "', 
										'" . $iTicketNumber . "', 
										'" . $_SESSION['user_id'] . "',
										'" . date("Y-m-d H:i:s") . "',
										'0',
										'" . $iThisLogUs['credit_limit'] . "',
										'" . $iThisLogUsCreditLimit['maxamount'] . "',
										'" . $iThisLogUs['user_available_balance'] . "',
										'0'	,
										'" . $iFreePlay . "'									
										)";


    mysqli_query($con, $q);


}


function BettingEvent($event_id, $score_away, $score_home, $winner_away, $winner_home, $score_away_by_period, $score_home_by_period, $event_status)
{
    global $con;


    $iResultBetEvent = mysqli_query($con, "SELECT * FROM betting_event WHERE event_id = '" . $event_id . "' and event_status = '" . $event_status . "'");
    $iResBetrow = mysqli_fetch_array($iResultBetEvent);

    if ($iResBetrow['id'] == '') {

        $q = "INSERT INTO betting_event (	event_id, 
										score_away,
										score_home, 
										winner_away, 
										winner_home, 
										score_away_by_period, 
										score_home_by_period,
										event_status ,
										create_at ,
										update_at
										) 
	
							VALUES ( 	'" . $event_id . "',
										'" . $score_away . "', 
										'" . $score_home . "', 
										'" . $winner_away . "', 
										'" . $winner_home . "',  
										'" . $score_away_by_period . "', 
										'" . $score_home_by_period . "', 
										'" . $event_status . "',
										'" . date("Y-m-d H:i:s") . "',
										'" . date("Y-m-d H:i:s") . "'									
										)";


        mysqli_query($con, $q);

    } else {

        $qNe = "UPDATE betting_event set  score_away = '" . $score_away . "',  score_home = '" . $score_home . "',  winner_away = '" . $winner_away . "',  winner_home = '" . $winner_home . "',  score_away_by_period = '" . $score_away_by_period . "',  score_home_by_period = '" . $score_home_by_period . "',  event_status = '" . $event_status . "',  update_at = '" . date("Y-m-d H:i:s") . "'   where event_id = '" . $event_id . "' ";
        mysqli_query($con, $qNe);

    }


}



if (isset($_POST['status']) && $_POST['status']=="oka") {
    
    $tokenid = RandomStringGenerator(10);
    $iTicketNumber = RandomNumberGenerator(10);
    foreach ($_POST['items'] as $item) {
           $dd = explode('-',$item['event_date']);
          $evendate = date("Y-m-d H:i:s", $dd[0].''.$dd[1]);
        datasumited($item['is_away']
        ,$item['is_home']
        , $tokenid
        ,$item['even_id']
        ,$item['o_and_u']
        ,$item['team_id']
        ,$item['teamname']
        ,"ABCD"
        ,$item['risk_win_slip']
        ,$item['scores']
        ,$item['original_money']
        ,$item['risk_stake_slip']
        ,$item['risk_stake_slip']
        ,$evendate
        ,"1"
        ,"2"
        ,$iTicketNumber
    );
     
    } 

    $returndata = [
        "status" => true,
        "response" => $tokenid
    ];
    echo json_encode($returndata);

/* 
    $q = "SELECT SUM(riskamount) as totalbettingprice FROM bettingdetail WHERE tokenid = '" . $tokenid . "'";
    $result = mysqli_query($con, $q);
    $row = mysqli_fetch_array($result);

    $iLoginUser = mysqli_query($con, "SELECT * FROM users WHERE user_id = '" . $_SESSION['user_id'] . "'");
    $iLoginUserDetails = mysqli_fetch_assoc($iLoginUser);


    if (($_POST['freeplay'] == '1') && ($iLoginUserDetails['free_pay_balance'] < $row['totalbettingprice'])) {
        $iDeleteDSate = mysqli_query($con, "DELETE FROM bettingdetail WHERE tokenid = '" . $tokenid . "'");
        redirect("insufficentfund.php?action=freepay&tokenid=" . $row['totalbettingprice']);
        exit;

    } elseif ($iLoginUserDetails['user_available_balance'] < $row['totalbettingprice']) {
        $iDeleteDSate = mysqli_query($con, "DELETE FROM bettingdetail WHERE tokenid = '" . $tokenid . "'");
        redirect("insufficentfund.php?tokenid=" . $tokenid);
        exit;
    }

    redirect("WagerTicket.php?tokenid=" . $tokenid);
    exit; */


}
?>




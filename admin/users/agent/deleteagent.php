<?php
require_once("../../../include/config.php");
require_once($basedir . "/admin/include/functions.php");
include $basedir . '/admin/include/isadmin.php';
$agents = getAssignUser();
$usermenu='active';
$datatables='active';

if($_GET['action']=='userdelete')
	{
	
		$qNeUser = "UPDATE users set  user_status =  '10'  where user_id = '".$_GET['user_id']."'";
		mysqli_query($con,$qNeUser);
		
		
		$q = "INSERT INTO  user_deleted (	
			 							userid, 
										usertype,
										deleted_by, 
										create_at 
										) 
	
							VALUES ( 	'".$_GET['user_id']."',
										'2', 
										'".$_SESSION['user_id']."', 
										'".date("Y-m-d H:i:s")."' 
										)";
				mysqli_query($con,$q);
				
				
		
			$qDel = "DELETE FROM assign_customer WHERE agent_id = '".$_GET['user_id']."'";
			$resultqDel = mysqli_query($con,$qDel);	
				
				
				
				
				
		
		
		unset($_POST);
		unset($_POST);
		header('location:index.php');	
		
	}
?>
<!DOCTYPE html>
<html>
<head>
<?php include $basedir . '/admin/include/header.php'; ?>
<script type="text/javascript">
function del(url)
 {  
 	if(	confirm('Really want to delete this.') )
	{
		window.location=url;
	}
}
</script>
</head>
<body class="skin-blue">
<header class="header">
  <?php include $basedir . '/admin/include/header_menu.php'; ?>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <aside class="left-side sidebar-offcanvas">
    <section class="sidebar">
      <?php include $basedir . '/admin/include/sidebar.php'; ?>
    </section>
  </aside>
  <aside class="right-side">
    <section class="content-header">
      <h1><?php echo $lang[17] ?><small><?php echo $lang[31] ?></small></h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo $baseurl ?>/"><i class="fa fa-dashboard"></i><?php echo $lang[27] ?></a></li>
        <li><a href="<?php echo $baseurl ?>/users"><?php echo $lang[14] ?></a></li>
        <li class="active"><?php echo $lang[17] ?></li>
      </ol>
    </section>
    <section class="content">
      <?php include('miniheader.php'); ?>
      <div class="tabs-sections" style="height:-webkit-fill-available">
        <div class="tabs-sections__section  tabs-sections__section--active  tab-js  tab-active-js">
          <div class="box-body box  no-padding">
              <table  class="table table-hover">
            
                <tr>
                  <td><h2>Delete Account</h2></td>
                 
                </tr>
              <tr>
                  <td><a class="btn btn-primary" href="javascript:del('deleteagent.php?user_id=<?php echo $_GET['user_id']; ?>&action=userdelete')" >Delete Account</a></td>
                </tr>
               
            </table>
          </div>
        </div>
      </div>
    </section>
  </aside>
</div>
<?php include $basedir . '/include/javascript.php'; ?>
</body>
</html>

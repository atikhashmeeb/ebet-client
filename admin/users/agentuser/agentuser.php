<?php
require_once("../../../include/config.php");
require_once($basedir . "/admin/include/functions.php");
include $basedir . '/admin/include/isadmin.php';

$iFileName='agentuser.php';
?>
<!DOCTYPE html>
<html>
<head>
<?php include $basedir . '/admin/include/header.php'; ?>
</head>
<body class="skin-blue">
<header class="header">
  <?php include $basedir . '/admin/include/header_menu.php'; ?>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left"> 
  <aside class="left-side sidebar-offcanvas"> 
    <section class="sidebar">
      <?php include $basedir . '/admin/include/sidebar.php'; ?>
    </section>
  </aside>
  <aside class="right-side"> 
    
    <!-- Header Nav (breadcrumb) -->
    <section class="content-header">
      <h1>Agents<small>agent user list page</small></h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo $baseurl ?>/"><i class="fa fa-dashboard"></i><?php echo $lang[27] ?></a></li>
        <li><a href="<?php echo $baseurl ?>/users"><?php echo $lang[14] ?></a></li>
        <li class="active"><?php echo $lang[17] ?></li>
      </ol>
    </section>
    <section class="content">
      <div class="row" style="margin-bottom: 15px;"> </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Agent <a href="#adminregist" class="btn btn-default btn-flat" data-toggle="modal" data-target="#adminregist">
              Add New Sub Agent
              </a></h3>
              <div class="box-tools"></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table id="datatable" class="table table-hover">
              <thead>
                <tr>
                  <th>UserId</th>
                  <th>Username</th>
                    <th>Full name</th>
                 <th>User</th>
                 <th>Active User</th>
                 <th>Last Login</th>
                 <th>Registered</th>
                 <th>Action</th>
              
                </tr>
              </thead>
              <tbody>
                <?php 
				
				
						 $current_start_week = strtotime('monday this week');
$current_end_week 	= strtotime('sunday this week');
$current_start_week = date("Y-m-d",$current_start_week);
$current_end_week = date("Y-m-d",$current_end_week);
$current_start_week.'-------------'.$current_end_week;
				
				
			$iThisParticularUserDetail=mysqli_query($con, "SELECT * FROM    agent_user WHERE agent_id = '".$_SESSION['user_id']."' order by id desc");
			while($iTHisUser = mysqli_fetch_assoc($iThisParticularUserDetail)) {
 					 
			 $agentQuery ="SELECT * FROM  users WHERE user_id = '".$iTHisUser['create_agentid']."'";
			 $agentUsid=mysqli_query($con, $agentQuery);
			 $agent = mysqli_fetch_assoc($agentUsid);
			 
			 ?>
                <tr>
                   			<td>  <?php echo $agent['user_id']?></td>
	                        <td><?php echo $agent['user_name']?></td>
	                         <td><?php echo $agent['user_fullname']  ?></td>
	                        <td>
<?php 
$iTESTAssignCustomerDetails = mysqli_query($con,"SELECT count(*)  as totalAssign FROM assign_agent_customer WHERE agent_id = '".$agent['user_id']."' LIMIT 0, 1");
$iTEstAssignCustomer = mysqli_fetch_array($iTESTAssignCustomerDetails);
echo $iTEstAssignCustomer['totalAssign'];
?>				
							</td>
	                        <td>
<?php 

$iAssignCustomerDetails = mysqli_query($con,"SELECT group_concat(customer_id) as CustomerId FROM assign_agent_customer WHERE agent_id = '".$agent['user_id']."' LIMIT 0, 1");
$iAssignCustomer = mysqli_fetch_array($iAssignCustomerDetails);

$iTotalAmountSpendLastWeek = mysqli_query($con,"SELECT DISTINCT userid FROM bettingdetail WHERE status!='0' and userid In (".$iAssignCustomer['CustomerId'].") and DATE(create_at) between '".$current_start_week."' and '".$current_end_week."'");


echo $iTotalUserCOunt=mysqli_num_rows($iTotalAmountSpendLastWeek);
?>	                            
                            </td>
	                        <td><?php echo date('m-d-Y', strtotime($agent['user_registered']))?></td>
                            <td><a href="<?php echo $baseurl ?>/admin/users/agentuser/editagentedit.php?action=edit&user_id=<?php echo $agent['user_id']?>">Edit</a> | 
                            <a href="<?php echo $baseurl ?>/admin/users/agentuser/editagentedit.php?action=chanpass&user_id=<?php echo $agent['user_id']?>">Change password</a></td>
                </tr>
                <?php }  ?>
              </tbody>
            </table>
            </div>
            <!-- /.box-body --> 
          </div>
          <!-- /.box --> 
          
        </div>
      </div>
      
      <!-- Modal -->
      <div class="modal fade" id="adminregist" tabindex="-1" role="dialog" aria-labelledby="adminregist" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="form-box" id="login-box">
              <div class="header">Register New Sub Agent</div>
              <form id="form">
                <div class="body bg-gray">
                  <div class="form-group">
                    <input type="text" id="name" class="form-control" placeholder="Full name" required>
                  </div>
                  <div class="form-group">
                    <input type="email" id="email" class="form-control" placeholder="E-mail" required>
                  </div>
                  <div class="form-group">
                    <input type="text" id="user_name" class="form-control" placeholder="User Name">
                  </div>
                  <div class="form-group">
                    <input type="password" id="password1" class="form-control" placeholder="Password" required>
                  </div>
                  <div class="form-group">
                    <input type="password" id="password2" class="form-control" placeholder="Retype password" required>
                  </div>
                </div>
                <div class="footer">
                  <button type="submit" id="signupadmin" class="btn bg-olive btn-block">Sign up</button>
                </div>
                <div align="center" id="message"></div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <?php
$public_key = $config['public_key'];
$time = time();
$games = false;
$hash = md5($public_key . $config['private_key'] . $time);

?>
       <script>
$(document).ready(function() {
	$("#form").bind("submit", manualValidate);
	function manualValidate(e) {
		e.preventDefault();
		e.target.checkValidity();
		var name = $('#name').val();
		var email = $('#email').val();
		var user_name = $('#user_name').val();
		var t = "<?php echo $time;?>";
		var p1 = $('#password1').val();
		var p2 = $('#password2').val();
		var url = "<?php echo $baseurl ?>";
		var key = "<?php echo $public_key?>";
		var hash = "<?php echo $hash?>";
		url = url + "/admin/users/agentuser/addnewagent.php";
		var uri = 'action=makeregister&hash=' + hash + '&public=' + key + '&t=' + t;
		uri += '&name=' + name + '&email=' + email + '&p1=' + p1 + '&p2=' + p2 + '&user_name=' + user_name;

		if (p1 !== p2) {
			$('#message').html('Password not matched');
			setTimeout(function() {
				$('#message').html('');
			}, 2000);
		}
		$.ajax({
			type: 'POST',
			url: url,
			beforeSend: function(x) {
				if(x && x.overrideMimeType) {
					x.overrideMimeType("application/json;charset=UTF-8");
				}
			},
			data: uri,
			success: function(data){
				
				if (data.status === 'success') {
					location.reload();
				} else {
					$('#message').html(data.status);
					setTimeout(function() {
						$('#message').html('');
					}, 2000);
				}
			}
			
		});
	}
})
</script> 
    </section>
    <!-- /.content --> 
    
  </aside>
  <!-- /.right-side --> 
  
</div>
<!-- ./wrapper -->

<?php include $basedir . '/include/javascript.php'; ?>
</body>
</html>
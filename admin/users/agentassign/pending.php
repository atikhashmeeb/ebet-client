<?php
require_once("../../../include/config.php");
require_once($basedir . "/admin/include/functions.php");
include $basedir . '/admin/include/isadmin.php';
$agents = getAssignUser();
$usermenu='active';
$datatables='active';


AddWeeklyBalance($_GET['user_id'] , '1');

if($_GET['action']=='delete')
	{
		
		
		$iThisParticularUserDetail=mysqli_query($con, "SELECT * FROM  bettingdetail WHERE userid = '".$_GET['user_id']."' and id='".$_GET['id']."' and result='0' ");
		$iTHisUser = mysqli_fetch_array($iThisParticularUserDetail) ;
		
		
		echo $q = "INSERT INTO bettingdetail_deleted (	
										bettingdetail_id, 
										userid, 
										ticketid, 
										o_and_u, 
										event_id,
										event_date, 
										team_id, 
										sport_name, 
										abrevation_name, 
										is_away, 
										is_home,
										bettingwin, 
										bettingcondition, 
										bettingcondition_orginal,  
										type, 
										wager,
										riskamount,  
										bettingamount, 
										bettingdetail_create_at, 
										tokenid,
										status,
										user_current_credit_limit,
										user_current_max_limit,
										user_current_pending,
										user_current_balance,
										deleted_by,
										create_at	) 
	
							VALUES ( 	'".$iTHisUser['id']."',
										'".$iTHisUser['userid']."',
										'".$iTHisUser['ticketid']."',
										'".$iTHisUser['o_and_u']."',
										'".$iTHisUser['event_id']."', 
										'".$iTHisUser['event_date']."', 
										'".$iTHisUser['team_id']."', 
										'".$iTHisUser['sport_name']."',  
										'".$iTHisUser['abrevation_name']."', 
 										'".$iTHisUser['is_away']."', 
										'".$iTHisUser['is_home']."', 
 										'".$iTHisUser['bettingwin']."', 
										'".$iTHisUser['bettingcondition']."', 
										'".$iTHisUser['bettingcondition_orginal']."', 
										'".$iTHisUser['type']."', 
										'".$iTHisUser['wager']."', 
										'".$iTHisUser['riskamount']."', 
										'".$iTHisUser['bettingamount']."', 
										'".$iTHisUser['create_at']."', 
										'".$iTHisUser['tokenid']."', 
										'".$iTHisUser['status']."', 
										'".$iTHisUser['user_current_credit_limit']."', 
										'".$iTHisUser['user_current_max_limit']."', 
										'".$iTHisUser['user_current_pending']."', 
										'".$iTHisUser['user_current_balance']."', 
 										'".$_SESSION['user_id']."',
										'".date("Y-m-d H:i:s")."'
										)";
										
										mysqli_query($con, $q);	
										
										
										
$q = "INSERT INTO transaction (userid, amount, betting_id, type, status, description, source, agent_id, create_at) VALUES ('".$iTHisUser['userid']."', '".$iTHisUser['riskamount']."', '".$iTHisUser['id']."', '1', '0', 'Wager Deleted By Agent', '2', '".$_SESSION['user_id']."',   '".date("Y-m-d H:i:s")."')";
		mysqli_query($con,$q);	
										
			 $iTOtalBetingPri =	$iTHisUser['riskamount'];		
			
			$qNeUser = "UPDATE users set  user_available_balance = user_available_balance + '".$iTOtalBetingPri."' where user_id = '".$iTHisUser['userid']."'";
		mysqli_query($con,$qNeUser);							
										
		
		$iDeleteDSate = mysqli_query($con, "DELETE FROM bettingdetail WHERE id = '".$_GET['id']."'");	
		
		unset($_POST);
		header('location:pending.php?user_id='.$_GET['user_id']);
		exit;
	}

?>
<!DOCTYPE html>
<html>
<head>
<?php include $basedir . '/admin/include/header.php'; ?>
    <script type="text/javascript">


function del(url)
{  
	if(	confirm('Really want to delete this.') )
	{
		window.location=url;
	}
}
</script>
</head>
<body class="skin-blue">
<header class="header">
  <?php include $basedir . '/admin/include/header_menu.php'; ?>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <aside class="left-side sidebar-offcanvas">
    <section class="sidebar">
      <?php include $basedir . '/admin/include/sidebar.php'; ?>
    </section>
  </aside>
  <aside class="right-side">
    <section class="content-header">
      <h1><?php echo $lang[17] ?><small><?php echo $lang[31] ?></small></h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo $baseurl ?>/"><i class="fa fa-dashboard"></i><?php echo $lang[27] ?></a></li>
        <li><a href="<?php echo $baseurl ?>/users"><?php echo $lang[14] ?></a></li>
        <li class="active"><?php echo $lang[17] ?></li>
      </ol>
    </section>
    <section class="content">
      <?php include('miniheader.php'); ?>
      <div class="tabs-sections" style="height:-webkit-fill-available">
        <div class="tabs-sections__section  tabs-sections__section--active  tab-js  tab-active-js">
         		
         
              <div class="box-body box  no-padding">
             
	          	  <table id="datatable" class="table table-hover">
	                <thead>
	                    <tr>
	                        <th>Description</th>
	                        <th>Risk/Win</th>
                            <th>Date</th>
                             <th>Delete</th>
	                    </tr>
	                </thead>
	                <tbody>
	                <?php 
$iThisParticularUserDetail=mysqli_query($con, "SELECT * FROM  bettingdetail WHERE userid = '".$_GET['user_id']."' and status!='0' and result='0' order by id desc");
					while($iTHisUser = mysqli_fetch_array($iThisParticularUserDetail)) {  ?>
	                    <tr>
	                        <td><?php echo $iTHisUser['abrevation_name']	?> <?php echo $iTHisUser['sport_name']	?> <b>(<?php echo $iTHisUser['bettingcondition_orginal']	?>)</b></td>
                            <td><?php echo $iTHisUser['riskamount']	?> / <?php echo $iTHisUser['bettingwin']	?></td>
	                        <td><?php echo $iTHisUser['create_at']	?> </td>
                            <td> <a title="Delete This Bet" href="javascript:del('pending.php?action=delete&id=<?php echo $iTHisUser['id']; ?>&user_id=<?php echo $_GET['user_id']; ?>')"    class="table-action-btn" > <i class="fa fa-times"></i> </a></td>
	                    </tr>
                      <?php }  ?>    
                        
	                </tbody>
	            </table>
	        </div> 
          
        </div>
      </div>
    </section>
  </aside>
</div>
<?php include $basedir . '/include/javascript.php'; ?>
</body>
</html>

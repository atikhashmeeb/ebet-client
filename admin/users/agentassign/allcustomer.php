<?php
require_once("../../../include/config.php");
require_once($basedir . "/admin/include/functions.php");
include $basedir . '/admin/include/isadmin.php';
$agents = getAssignUser();
$usermenu='active';
$datatables='active';


if($_GET['week']=='last') {
////////////// PREVIOUS WEEEK START AND END DATES
$previous_week 		= strtotime("-1 week +1 day");
$start_week 		= strtotime("tuesday this week",$previous_week);
$end_week 			= strtotime("monday next week",$start_week);
$current_start_week = date("Y-m-d",$start_week);
$current_end_week = date("Y-m-d",$end_week);
 $start_week.'-------------'.$end_week;

////////////// CURRENTLY WEEEK START AND END DATES
} else {
$current_start_week = strtotime('tuesday this week');
$current_end_week 	= strtotime('monday next week');
$current_start_week = date("Y-m-d",$current_start_week);
$current_end_week 	= date("Y-m-d",$current_end_week);
$current_start_week.'-------------'.$current_end_week;

}
if($_GET['weekdays']!='')
	{
	
	
	$str_arr = explode (",", $_GET['weekdays']);  

	
	$current_start_week = $str_arr[0];
	$current_end_week   = $str_arr[1];	
		
	}
?>
<!DOCTYPE html>
<html>
<head>
<?php include $basedir . '/admin/include/header.php'; ?>
<script>
function submformusing()
	{
		document.getElementById("sumbmitofmr").submit();

	}
</script>
<body class="skin-blue">
<header class="header">
  <?php include $basedir . '/admin/include/header_menu.php'; ?>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <aside class="left-side sidebar-offcanvas">
    <section class="sidebar">
      <?php include $basedir . '/admin/include/sidebar.php'; ?>
    </section>
  </aside>
  <aside class="right-side">
    <section class="content-header">
      <h1><?php echo $lang[17] ?><small><?php echo $lang[31] ?></small></h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo $baseurl ?>/"><i class="fa fa-dashboard"></i><?php echo $lang[27] ?></a></li>
        <li><a href="<?php echo $baseurl ?>/users"><?php echo $lang[14] ?></a></li>
        <li class="active"><?php echo $lang[17] ?></li>
      </ol>
    </section>
    <section class="content">
      <div class="tabs-sections" style="height:-webkit-fill-available">
        <div class="tabs-sections__section  tabs-sections__section--active  tab-js  tab-active-js">
          <div class="box-body box  no-padding"> <br>
            <br>
            <form action="" id="sumbmitofmr" method="get" >
            <table>
            <tr><td><select class="form-control" name="weekdays" onChange="submformusing()">
            		<option>Current Week</option>
                    <?php
						  for($i=0; $i<6; $i++) {
							$previous_week = strtotime("-$i week +$i day");
							$start_week = strtotime("tuesday this week",$previous_week);
							$end_week = strtotime("monday next week",$start_week);
							$start_weekNew = date("Y-m-d",$start_week);
							$end_weekNew = date("Y-m-d",$end_week); ?>
<option <?php if($_GET['weekdays']==$start_weekNew.','.$end_weekNew) { echo 'selected'; } ?> value="<?php echo $start_weekNew.','.$end_weekNew; ?>"><?php echo date("m/d/Y",$start_week); ?></option>
                    <?php } ?>
                    </select></td>
            <td><a href="allcustomer.php" class="btn btn-default">Current Week</a></td>
            <td><a href="allcustomer.php?week=last" class="btn btn-primary">Last Week</a> </td>
            </tr>
            </table>
            </form>
            <table id="datatable" class="table table-hover">
              <thead>
                <tr>
                  <th>UserId</th>
                  <th>Name</th>
                  <?php
 				  	for ($iOms =	strtotime($current_start_week); $iOms<=	strtotime($current_end_week); $iOms+=86400) {    ?>
                  <th class="thHeaderPeriod" style="width:100px;"><?php  echo date("D", $iOms);  /// date("Y-m-d", $iOms); // This is Date  ?>
                  </th>
                  <?php } ?>
                  <th>Weekly</th>
                  <th>Pending</th>
                  <th>Payments</th>
                  <th>Balance</th>
                </tr>
              </thead>
              <tbody>
                <?php 
$iAssignCustomerDetails = mysqli_query($con,"SELECT group_concat(customer_id) as CustomerId FROM assign_customer WHERE agent_id = '".$_SESSION['user_id']."' LIMIT 0, 1");
$iAssignCustomer = mysqli_fetch_array($iAssignCustomerDetails);

$iTotalAmountSpendLastWeek = mysqli_query($con,"SELECT DISTINCT userid FROM bettingdetail WHERE status!='0' and userid In (".$iAssignCustomer['CustomerId'].") and DATE(create_at) between '".$current_start_week."' and '".$current_end_week."'");

$iCommaSepUser = '';
while($iTotAmtSpdLstWek = mysqli_fetch_array($iTotalAmountSpendLastWeek))
	{
		$iCommaSepUser .= $iTotAmtSpdLstWek['userid'].' ,';	
	}
$iCommaSepUser =	rtrim($iCommaSepUser,',');
				
				
					$iThisParticularUserDetail=mysqli_query($con, "SELECT * FROM   assign_customer WHERE agent_id = '".$_SESSION['user_id']."' order by id desc");
					while($iTHisUser = mysqli_fetch_assoc($iThisParticularUserDetail)) {
						$iFinalAmount =0;
						$iTotalFinalAamou = 0;
					 $qrny ="SELECT * FROM  users WHERE user_id = '".$iTHisUser['customer_id']."'";
					 $ThisGetUsid=mysqli_query($con, $qrny);
					 $iTssUser = mysqli_fetch_assoc($ThisGetUsid);
	                ?>
                <tr>
                  <td><a href="index.php?action=edit&user_id=<?php echo $iTHisUser['customer_id']	?>">USER <?php echo $iTHisUser['customer_id']	?></a></td>
                  <td><?php echo $iTssUser['user_name']	?></td>
                  <?php
 				  	for ($iOms =	strtotime($current_start_week); $iOms<=	strtotime($current_end_week); $iOms+=86400) {
						
$q1 = "SELECT SUM(bettingwin) as TotalBettingWin FROM bettingdetail WHERE userid = '".$iTssUser['user_id']."' and status = '2' and result='1' and DATE(create_at) = '".date("Y-m-d", $iOms)."'";
$result1 = mysqli_query($con,$q1);
$iBetDet1 = mysqli_fetch_array($result1);	

$q2 = "SELECT SUM(riskamount) as TotalRiskAmount FROM bettingdetail WHERE userid = '".$iTssUser['user_id']."'  and status = '2' and result='2' and DATE(create_at) = '".date("Y-m-d", $iOms)."'";
$result2 = mysqli_query($con,$q2);
$iBetDet2 = mysqli_fetch_array($result2);	

$iFinalAmount	= $iBetDet1['TotalBettingWin']+$iBetDet2['TotalRiskAmount'];	 ?>
                  <td><?php  echo $iFinalAmount;	 $iTotalFinalAamou += $iFinalAmount;	  ?></td>
                  <?php } ?>
                  <td style="width:20px;"><?php echo $iTotalFinalAamou; ?></td>
                  <td><?php
$q3 = "SELECT SUM(riskamount) as iWeekPendingAmount FROM bettingdetail WHERE userid = '".$iTssUser['user_id']."'  and status = '2' and result='0' and DATE(create_at) BETWEEN  '".$current_start_week."' and  '".$current_end_week."'";
					$result3 = mysqli_query($con,$q3);
					$iBetDet3 = mysqli_fetch_array($result3);	
		if($iBetDet3 ['iWeekPendingAmount']=='') { echo '0'; } else {	echo $iBetDet3 ['iWeekPendingAmount'];	}
					?></td>
                  <td style="width: 30px;"><?php
$q3Dep = "SELECT SUM(amount) as DepositeAmount FROM transaction WHERE userid = '".$iTssUser['user_id']."'  and type = '0' and DATE(create_at) BETWEEN  '".$current_start_week."' and  '".$current_end_week."'";
$result3Dep = mysqli_query($con,$q3Dep);
$iBetDet3Dep = mysqli_fetch_array($result3Dep);	

 $iBetDet3Dep['DepositeAmount'];


$q3Widt= "SELECT SUM(amount) as WidthrawlAmount FROM transaction WHERE userid = '".$iTssUser['user_id']."'  and type = '1' and DATE(create_at) BETWEEN  '".$current_start_week."' and  '".$current_end_week."'";
$result3Widt = mysqli_query($con,$q3Widt);
$iBetDet3Widt = mysqli_fetch_array($result3Widt);	

 $iBetDet3Widt['WidthrawlAmount'];


echo $iBetDet3Dep['DepositeAmount']-$iBetDet3Widt['WidthrawlAmount'];

					?></td>
                  <td style="width: 30px;"><?php
					 $q4 = "SELECT * FROM users WHERE user_id = '".$iTssUser['user_id']."'";
					$result4 = mysqli_query($con,$q4);
					$iBetDet4 = mysqli_fetch_array($result4);	
					 echo $iBetDet4['user_available_balance'];
					?></td>
                </tr>
                <?php }  ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </section>
  </aside>
</div>
<?php include $basedir . '/include/javascript.php'; ?>
</body>
</html>

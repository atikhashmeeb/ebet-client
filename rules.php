<?php 
require_once('include/config.php');
require_once($basedir . "/include/functions.php");
require_once($basedir . '/include/user_functions.php');
if ($_SESSION['user_id']=='') { 
	header('Location: ' . $baseurl . '/log.php');
	exit;
}

$settingsmenu = 'active';
$accountmenu='active';

$def_tz = ($_SESSION['user_timezone']) ? $_SESSION['user_timezone'] : 'Asia/Tokyo';
$languages = getLanguages();

$file = $basedir . '/temp/all_users.txt';
$data = json_decode(file_get_contents($file), true);
?>

<!DOCTYPE HTML>
<html>
<?php include $basedir . '/common/header.php'; ?>

<style>
 
 h1 {
  color: black;
  text-align: center !important;
  text-decoration: underline;
  font-size: 35px !important;
  text-style: bold;
}

	h2 {
  color: black !important;
  text-align: left;
  text-decoration: underline;
 font-size: 20px !important;
  text-style: bold !important;

}

strong { font-weight: bold; }

</style>

<body>

	<?php include $basedir . '/common/head.php'; ?>


	<div class="container row">
	
		<?php include $basedir . '/common/myheadmenu.php'; ?>

		<main role="main" class="row gutters mypage">

			<article id="myaccount" class="col span_12">

				<div class="box">
		
		<center> <h1> Rules </h1> </center>

 <h2> Live Betting Rules </h2>
 
 

 <p>
                        <strong>THIS SITE is fully licensed and authorized to operate on the Internet.</strong> To ensure complete
                        legality, all transactions and wagers will be considered as originating from and
                        governed by the laws of Costa Rica.
                    </p>
                    <p>
                        Member's winnings will not be reported to any government agency. It is the sole
                        responsibility of each individual to report income for tax purposes.</p>
                    <p>
                        All members must be at least 21 years old.</p>
                    <p>
                        Personal identity and details of all members; including name, address, phone number,
                        email address and wagering history, will remain completely confidential, and never
                        given, reported or sold to any third party.</p>
                    <p>
                        Members are solely responsible for their account transactions and should keep all
                        account information confidential. The member is solely responsible for any unauthorized
                        use of the account either through the phone or the website.</p>
                    <p>
                        Management reserves the right to refuse or limit any wager.</p>
                    <p>
                        All Internet wagers are final once accepted by THIS SITE and verified by the member.</p>
					<p>
						All Internet wagers must be placed through our user interface provided on our Web pages. 
						Any Internet wagering through other means, including the use of a "robot" service, is strictly forbidden. 
						In the event the use of non-approved client software is detected, 
						Management reserves the right to invalidate all such wagers retroactively, cancel the player's account, 
						and/or take any other action it deems appropriate.
					</p>
                    <p>
                        <p>
                            <p>
                                THIS SITE follows Las Vegas no bet regulations which state: No bet +/- 2 � points or
                                40 � from the consensus line shall be honored regardless of whether the error is
                                made by the sportsbook or the customer in either manual or electronic transaction.</p>
                            <p>
                                <p>
                                    In Baseball, a wager on a Run Line or a Total will always be with the Listed Pitchers.
                                    Therefore, if there is a Pitcher change, the wager will be NO ACTION.</p>
                            </p>
						</p>
					</p>


					<br></br>


					<h2>
				Dead Heat Rules Explained
			</h2>			<p>
					A dead heat is when two or more selections in an event tie. 
					You could have a two-way dead-heat in a horserace or even a five-way 
					dead-heat in golf (especially for the places in a golf tournament, 
					say a five-way dead-heat for third).
				</p>
				<p>
					So what are the bookmaker and sportsbook rules when there is a dead-heat for betting purpose?
				</p>
				<p>
					<b>The rule is that if two selections dead-heat for any placing, half the stake is applied to the selection at full betting odds and the other half of the stake is lost.</b>
				</p>
				<p>
					If more than two dead-heat, the stake is proportionally reduced accordingly.
				</p>			
				<br></br>

				<h2>
                Horses Rules
            </h2>
			<p>Please contact your agent for horse payouts.</p>
                <p>We do not assume liability for wagers that are unsuccessfully entered before post time.</p>
                <p>Horses are identified by saddlecloth number not by name.</p>
                <p>When you wager on a horse that is coupled with other horses, your wager includes all horses running under that number. If a horse is coupled with others, you receive all horses running as 
				   part of the entry. If part of the entry is scratched and the other part of the entry runs, all wagers have action. There is no �must go� action as part of the entry. If one of the entry 
				   horses runs, all wagers have action.</p>
                <p>If there are no track payoffs for a certain type of wager, all wagers will be refunded.</p>              
                <p>NO HOUSE QUINELLAS!!</p>
                      <br></br>

					   <h2>
                Betting Rules
            </h2>
			<ol class="blue_num">
                    <li><b>THIS SITE reserves</b> the right to limit or refuse any wager prior to its acceptance.
                    </li>
                    <li>Minimum wager $50 by phone, $1 by Internet.</li>
                    <li>BAD LINE RULE � THIS SITE will not accept wagers on events posted with obvious or gross
                        line errors. Management also reserves the right to cancel any wager made on an obvious
                        "bad" line whether due to software or human error.</li>
                    <li>Members should check their account balance on a weekly basis at the minimum.</li>
                    <li>Members must state their User ID and Password to the THIS SITE consultant prior to
                        obtaining a line or placing a wager.</li>
                    <li>Account balances are verified by the THIS SITE consultant. Any discrepancies will be
                        corrected by the customer service department.</li>
                    <li>THIS SITE keeps track of all wagers by telephone. Any discrepancies with regard to
                        wagers will be resolved by the tape recording of the transaction, and that recording
                        shall serve as the final determination of the dispute and all wagers will be adjusted
                        accordingly.</li>
                    <li>READ BACK IS FINAL" when the THIS SITE consultant reads back the wager and says "If
                        all of the plays just stated are correct, please state your PIN and Password". It
                        is the customer's responsibility to correct any errors during the readback. Once
                        the plays are confirmed they are final. In the event that the customer does not
                        hear the read back due to technical difficulties, it is the customer's responsibility
                        to call back immediately to confirm the plays. Otherwise the wagers will be voided.</li>
                    <li>All disputes will be settled by management in a timely manner.</li>
                    <li>THIS SITE reserves the right to add, delete or change house wagering limits, rules
                        or payoff odds at any time.</li>
                    <li>The placing of wagers by anyone under 21 is prohibited.</li>
                    <li>Las Vegas Rules apply to any wager or discrepancy not covered by the rules and regulations
                        here specified.</li>
                    <li>In order to have action, the game must go:
                        <br>
                        <br>
                        <ul class="blue_dots">
                            <li>Football (College - Pro) 55 min. of play</li>
                            <li>Basketball (Pro) 43 min. of play</li>
                            <li>Basketball (College) 35 min. of play</li>
                            <li>Hockey (Pro) 55 min. of play</li>
                            <li>Boxing (Pro - amateur) 1st round bell</li>
                            <li>Other sports (W/time-limits) 5 min. of play remaining</li>
                            <li>Baseball (Home Team Winning) 4 1/2 innings complete</li>
                            <li>Baseball (Visitor Winning) 5 innings complete</li>
                            <li>Baseball (Total Runs-Home team winning) 8 1/2 innings complete</li>
                            <li>Baseball (Total Runs-visitor winning) 9 innings complete</li>
                        </ul>
                    </li>
                    <li>All sporting events must be played on the date as scheduled unless otherwise specified.
                        Any event postponed or rescheduled will automatically constitute " No Action".</li>
                    <li>When wagering on totals, overtime periods are counted in the final score, unless
                        otherwise specified.</li>
                    <li>The home team is always listed as the bottom team unless otherwise specified.</li>
                </ol>

                    

				</div>

			</article>
			 
		</main>

	</div>



	<script src="<?php echo $baseurl?>/js/jquery-1.11.0.min.js"></script>
    <script src="http://ebet21.com/oldwebsite/php/Assets/JS/Index_JS.js"></script>
    <?php include $basedir . '/common/foot.php'; ?>

    <script src="<?php echo $baseurl?>/js/jquery.remodal.js"></script>
	<script type="text/javascript" src="<?php echo $baseurl?>/js/main_frontend.js"></script>

	<script src="<?php echo $baseurl?>/js/jquery.minimalect.min.js"></script>
	<script type="text/javascript">
	  $(document).ready(function(){
	    $("#myaccount form.inputform select").minimalect();
	  });
	</script>


</body>

</html>
<script>
$(document).ready(function () {
	$("form").bind("submit", submitForm);
	function submitForm(e) {
	    e.preventDefault();
	    e.target.checkValidity();
	    var lang = $('select[name="language"]').val();
	    var old_lang = $('input[name="old_lang"]').val();
	    var tz = $('select[name="timezone"]').val();
	    var t = "<?php echo $time;?>";
	    var url = "<?php echo $baseurl ?>";
	    var key = "<?php echo $public_key?>";
	    var hash = "<?php echo $hash?>";
	    url = url + "/ajax/changelangtz.php";
	    var uri = 'hash=' + hash + '&public=' + key + '&t=' + t;
	    uri += '&lang=' + lang + '&tz=' + tz;
	
	    $.ajax({
	        type: 'POST',
	        url: url,
	        beforeSend: function(x) {
	            if(x && x.overrideMimeType) {
	                x.overrideMimeType("application/json;charset=UTF-8");
	            }
	        },
	        data: uri,
	        success: function(data){
	        	if (lang && lang !== old_lang) {
		        	window.location.reload(true);
	        	}
                $('#message').html(data.message);
                setTimeout(function() {
	                $('#message').html('');
                }, 3000)
	        }
	        
	    });
	}
})
</script>


</body>

</html>
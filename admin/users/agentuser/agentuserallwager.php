<?php
require_once("../../../include/config.php");
require_once($basedir . "/admin/include/functions.php");
include $basedir . '/admin/include/isadmin.php';
$agents = getAssignUser();
$usermenu='active';
$datatables='active';
?>
<!DOCTYPE html>
<html>
<head>
<?php include $basedir . '/admin/include/header.php'; ?>
<script type="text/javascript">


function del(url)
{  
	if(	confirm('Really want to delete this.') )
	{
		window.location=url;
	}
}
</script>
</head>
<body class="skin-blue">
<header class="header">
  <?php include $basedir . '/admin/include/header_menu.php'; ?>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <aside class="left-side sidebar-offcanvas">
    <section class="sidebar">
      <?php include $basedir . '/admin/include/sidebar.php'; ?>
    </section>
  </aside>
  <aside class="right-side">
    <section class="content-header">
      <h1>Wagers<small>All Wagers</small></h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo $baseurl ?>/"><i class="fa fa-dashboard"></i><?php echo $lang[27] ?></a></li>
        <li><a href="<?php echo $baseurl ?>/users"><?php echo $lang[14] ?></a></li>
        <li class="active"><?php echo $lang[17] ?></li>
      </ol>
    </section>
    <section class="content">
      <div class="tabs-sections" style="height:-webkit-fill-available">
        <div class="tabs-sections__section  tabs-sections__section--active  tab-js  tab-active-js">
          <div class="box-body box  no-padding">
            <table id="datatable" class="table table-hover">
              <thead>
                <tr>
                  <th>User Id</th>
                  <th>Username</th>
				  <th>Date Placed</th>
                  <th>Description</th>
                  <th>Risk/Win</th>
                  <th>Ticket ID</th>
                </tr>
              </thead>
              <tbody>
                <?php
				
 $iAssignCustomerDetails = mysqli_query($con,"SELECT group_concat(customer_id) as CustomerId FROM assign_agent_customer WHERE agent_id = '".$_SESSION['user_id']."' LIMIT 0, 1");
$iAssignCustomer = mysqli_fetch_array($iAssignCustomerDetails);
	 
					
$iThisParticularUserDetail=mysqli_query($con, "SELECT * FROM  bettingdetail WHERE userid In (".$iAssignCustomer['CustomerId'].") order by id DESC");
					
			 

					while($iTHisUser = mysqli_fetch_array($iThisParticularUserDetail)) { 
					
					
					$qrny ="SELECT * FROM  users WHERE user_id = '".$iTHisUser['userid']."'";
					 $ThisGetUsid=mysqli_query($con, $qrny);
					 $iTssUser = mysqli_fetch_assoc($ThisGetUsid);
					
					 ?>
                <tr>
                  <td><a href="agentusercustomeredit.php?action=edit&user_id=<?php echo $iTssUser['user_id']	?>">USER <?php echo $iTssUser['user_id']	?></a></td>
                  <td><?php echo $iTssUser['user_name']	?></td>
				  <td><?php echo date("m/d/Y", strtotime($iTHisUser['create_at'])); ?> <?php echo date("h:i a", strtotime($iTHisUser['create_at'])); ?> </td>
                  <td><?php echo $iTHisUser['abrevation_name']	?> <?php echo $iTHisUser['sport_name']	?> <b>(<?php echo $iTHisUser['bettingcondition_orginal']	?>)</b></td>
                  <td><?php echo $iTHisUser['riskamount']	?> / <?php echo $iTHisUser['bettingwin']	?></td>
                  <td><?php echo $iTHisUser['ticketid']	?></td>
                </tr>
                <?php }  ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </section>
  </aside>
</div>
<?php include $basedir . '/include/javascript.php'; ?>
</body>
</html>

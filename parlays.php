<?php 
require_once('include/config.php');
require_once($basedir . "/include/functions.php");
require_once($basedir . '/include/user_functions.php');
if ($_SESSION['user_id']=='') { 
	header('Location: ' . $baseurl . '/log.php');
	exit;
}

$settingsmenu = 'active';
$accountmenu='active';

$def_tz = ($_SESSION['user_timezone']) ? $_SESSION['user_timezone'] : 'Asia/Tokyo';
$languages = getLanguages();

$file = $basedir . '/temp/all_users.txt';
$data = json_decode(file_get_contents($file), true);
?>

<!DOCTYPE HTML>
<html>
<?php include $basedir . '/common/header.php'; ?>


<style>
 

 
 .btn-group a {
    background: white;
    border: 0;
    border-radius: 0;
    color: #1c3845;
    font-weight: 600;
    text-decoration: none;
    font-family: sans-serif;
    font-size: 14px;
}
 
 
 
 
 .bg-light {
        background-color: #f8f9fa!important;
    padding: 20px;}
 
 
.box h1 {
    padding: 0px 0px 0;
       font-size: 16px;
	    font-family: sans-serif;
   
}

thead.bg-primary{
border-bottom: 3px solid #D1202A;
    background: linear-gradient(#000000, #343333);}

.btn-group, .btn-group-vertical {
    position: relative;
    display: -webkit-inline-box;
    display: -ms-inline-flexbox;
    display: inline-flex;
    vertical-align: middle;
}

 .float-right {
    float: right!important;

	background: linear-gradient(#1d2d35, #D1202A);
    color: white;
    text-decoration: none;
    padding: 3px 11px;
	    font-family: sans-serif;


}
.col-md-6 {
    -webkit-box-flex: 0;
    -ms-flex: 0 0 50%;
    flex: 0 0 50%;
    width: 470px;
	float: left;
	padding-left: 20px;
}
.col-md-9 {
    -webkit-box-flex: 0;
    -ms-flex: 0 0 50%;
    flex: 0 0 50%;
    width: 470px;
	float: right;
	padding-right: 20px;
}

.btn {
    display: inline-block;
    font-weight: 400;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    border: 1px solid transparent;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    border-radius: .25rem;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}
.btn-primary {
    color: #fff;
    background-color: #007bff;
    border-color: #007bff;
}
.btn-success {
    color: #fff;
    background-color: #28a745;
    border-color: #28a745;
}


.box .table {
    margin: 0px auto 20px;
}

.box .status, .box .table {
       width: 100%;
    margin: 0 auto 10px;
    border-top: 0px;
    padding: 4px 0;
    height: inherit;
    border: 2px solid #060f42;
}

.box .status, .box .table tbody tr td{
text-align:left;
}

.bg-primary {
    background-color: #007bff!important;
    padding: 6px 15px 6px 13px;
    color: white;
    border: 0;
}

#myaccount .box, #dashboard .box {
    margin: 0px 0px 0px 0px;
	
}


.selectRow:hover {
      background-color: #ffeb3b61;
    color: black;
}

.selectRow span.float-right {
       color: #f8f9fa;
}





@media only screen and (max-width: 991px) {

#myaccount .box, #dashboard .box {
    margin: 0px 10px 0px 10px;
}





.selectRow:hover {
      background-color: #ffeb3b61;
    color: black;
}

.selectRow span.float-right {
       color: #f8f9fa;
}

.clicked {
     background-color: #ffeb3b61;
    color: black;
}

.btn-group a {
    border-radius: 0;
}
.btn-group.col-xs-12 {
	    width: 100%;
	    background: #f8f9fa;
}


.gutters .col:first-child {
    padding: 0px 0;
}
.box .status, .box .table {
       width: 100%;
    margin: 0 auto 10px;
    border-top: 0px;
    padding: 4px 0;
    height: inherit;
    border: 2px solid #060f42;
}

.box .status, .box .table tbody tr td{
text-align:left;
}



.btn {
    display: inline-block;
    font-weight: 400;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    border: 1px solid transparent;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    border-radius: .25rem;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}
.btn-primary {
    color: #fff;
    background-color: #007bff;
    border-color: #007bff;
}
.btn-success {
    color: #fff;
    background-color: #28a745;
    border-color: #28a745;
}
strong { font-weight: bold; }

h3 {
	font-weight: bold; 
	font-size: 2rem;


}

</style>



<body>

	<?php include $basedir . '/common/head.php'; ?>


	<div class="container row">
	
		<?php include $basedir . '/common/myheadmenu.php'; ?>

		<main role="main" class="row gutters mypage">

			<article id="myaccount" class="col span_12">

				<div class="box">
				
 



  <br></br>
  <br></br>
   <br></br>
    <br></br>




                      
                    

				</div>

			</article>
			 
		</main>

	</div>



	<script src="<?php echo $baseurl?>/js/jquery-1.11.0.min.js"></script>
    <script src="http://ebet21.com/oldwebsite/php/Assets/JS/Index_JS.js"></script>
    <?php include $basedir . '/common/foot.php'; ?>

    <script src="<?php echo $baseurl?>/js/jquery.remodal.js"></script>
	<script type="text/javascript" src="<?php echo $baseurl?>/js/main_frontend.js"></script>

	<script src="<?php echo $baseurl?>/js/jquery.minimalect.min.js"></script>
	<script type="text/javascript">
	  $(document).ready(function(){
	    $("#myaccount form.inputform select").minimalect();
	  });
	</script>


</body>

</html>
<script>
$(document).ready(function () {
	$("form").bind("submit", submitForm);
	function submitForm(e) {
	    e.preventDefault();
	    e.target.checkValidity();
	    var lang = $('select[name="language"]').val();
	    var old_lang = $('input[name="old_lang"]').val();
	    var tz = $('select[name="timezone"]').val();
	    var t = "<?php echo $time;?>";
	    var url = "<?php echo $baseurl ?>";
	    var key = "<?php echo $public_key?>";
	    var hash = "<?php echo $hash?>";
	    url = url + "/ajax/changelangtz.php";
	    var uri = 'hash=' + hash + '&public=' + key + '&t=' + t;
	    uri += '&lang=' + lang + '&tz=' + tz;
	
	    $.ajax({
	        type: 'POST',
	        url: url,
	        beforeSend: function(x) {
	            if(x && x.overrideMimeType) {
	                x.overrideMimeType("application/json;charset=UTF-8");
	            }
	        },
	        data: uri,
	        success: function(data){
	        	if (lang && lang !== old_lang) {
		        	window.location.reload(true);
	        	}
                $('#message').html(data.message);
                setTimeout(function() {
	                $('#message').html('');
                }, 3000)
	        }
	        
	    });
	}
})
</script>


</body>

</html>
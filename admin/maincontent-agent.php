<?php 
$iAssignCustomerDetails = mysqli_query($con,"SELECT group_concat(customer_id) as CustomerId FROM assign_agent_customer WHERE agent_id = '".$_SESSION['user_id']."' LIMIT 0, 1");
$iAssignCustomer = mysqli_fetch_array($iAssignCustomerDetails);


////////////// PREVIOUS WEEEK START AND END DATES
$previous_week = strtotime("-1 week +1 day");
$start_week = strtotime("last sunday midnight",$previous_week);
$end_week = strtotime("next saturday",$start_week);
$start_week = date("Y-m-d",$start_week);
$end_week = date("Y-m-d",$end_week);
 $start_week.'-------------'.$end_week;

////////////// CURRENTLY WEEEK START AND END DATES
 
$current_start_week = strtotime('monday this week');
$current_end_week 	= strtotime('sunday this week');
$current_start_week = date("Y-m-d",$current_start_week);
$current_end_week = date("Y-m-d",$current_end_week);
$current_start_week.'-------------'.$current_end_week;


 
$iTotalAmountSpendLastWeek = mysqli_query($con,"SELECT DISTINCT userid FROM bettingdetail WHERE status!='0' and userid In (".$iAssignCustomer['CustomerId'].") and DATE(create_at) between '".$current_start_week."' and '".$current_end_week."'");

$iTotalUserCOunt=mysqli_num_rows($iTotalAmountSpendLastWeek);
$iTotAmtSpdLstWek = mysqli_fetch_array($iTotalAmountSpendLastWeek);

?>

<div class="row">
  <div class="col-lg-3 col-xs-6">
    <div class="small-box bg-aqua">
      <div class="inner">
        <h3><?php echo $iTotalUserCOunt ; ?></h3>
        <p>Active Users</p>
      </div>
      <div class="icon"><i class="ion ion-game-controller-a"></i> </div>
      <a href="<?php echo $baseurl ?>/admin/users/agentuser/agentuseractiveuser.php?lang=en&active=player" class="small-box-footer"><?php echo $lang[50] ?> <i class="fa fa-arrow-circle-right"></i></a> </div>
  </div>
  <!-- ./col -->
  
  <?php
 $q1 = "SELECT SUM(bettingwin) as TotalBettingWin FROM bettingdetail WHERE userid In (".$iAssignCustomer['CustomerId'].") and  result='1' and DATE(create_at) = '".date("Y-m-d")."'";
$result1 = mysqli_query($con,$q1);
$iBetDet1 = mysqli_fetch_array($result1);	


 $q2 = "SELECT SUM(riskamount) as TotalRiskAmount FROM bettingdetail WHERE userid In (".$iAssignCustomer['CustomerId'].") and  result='2' and DATE(create_at) = '".date("Y-m-d")."'";
$result2 = mysqli_query($con,$q2);
$iBetDet2 = mysqli_fetch_array($result2);	

$iFinalAmount= $iBetDet1['TotalBettingWin']+$iBetDet2['TotalRiskAmount'];
?>
  <div class="col-lg-3 col-xs-6">
    <div class="small-box bg-green">
      <div class="inner">
        <h3><?php echo number_format($iFinalAmount, 2)?> </h3>
        <p>Today Figure</p>
      </div>
      <div class="icon"><i class="ion ion-stats-bars"></i></div>
      <a href="<?php echo $baseurl ?>/admin/users/agentuser/agentusercustomer.php?lang=en" class="small-box-footer"><?php echo $lang[50] ?> <i class="fa fa-arrow-circle-right"></i></a> </div>
  </div>
  <!-- ./col -->
  
  <?php
$q1 = "SELECT SUM(bettingwin) as TotalBettingWin FROM bettingdetail WHERE userid In (".$iAssignCustomer['CustomerId'].") and result='1' and DATE(create_at) between '".$current_start_week."' and '".$current_end_week."'";
$result1 = mysqli_query($con,$q1);
$iBetDet1 = mysqli_fetch_array($result1);	


$q2 = "SELECT SUM(riskamount) as TotalRiskAmount FROM bettingdetail WHERE userid In (".$iAssignCustomer['CustomerId'].")  and result='2' and DATE(create_at) between '".$current_start_week."' and '".$current_end_week."'";
$result2 = mysqli_query($con,$q2);
$iBetDet2 = mysqli_fetch_array($result2);	

$iFinalAmount= $iBetDet1['TotalBettingWin']+$iBetDet2['TotalRiskAmount'];
?>
  <div class="col-lg-3 col-xs-6">
    <div class="small-box bg-red">
      <div class="inner">
        <h3><?php echo number_format($iFinalAmount, 2)?></h3>
        <p>Weekly Figure</p>
      </div>
      <div class="icon"><i class="ion ion-pie-graph"></i></div>
      <a href="<?php echo $baseurl ?>/admin/users/agentuser/agentusercustomer.php?lang=en" class="small-box-footer"><?php echo $lang[50] ?> <i class="fa fa-arrow-circle-right"></i></a> </div>
  </div>
  <!-- ./col -->
  <?php
$qDEL2 = "SELECT count(*) as TotalDelete FROM bettingdetail_deleted WHERE deleted_by = '".$_SESSION['user_id']."'";
$resultDEL2 = mysqli_query($con,$qDEL2);
$iBetDetDEL2 = mysqli_fetch_array($resultDEL2);	
?>
  <div class="col-lg-3 col-xs-6"> 
    <!-- small box -->
    <div class="small-box bg-yellow">
      <div class="inner">
        <h3><?php echo $iBetDetDEL2['TotalDelete']; ?></h3>
        <p>Deleted Wagers</p>
      </div>
      <div class="icon"><i class="ion ion-person-add"></i></div>
      <a href="<?php echo $baseurl ?>/admin/users/agentuser/agentuserdeleted_wager.php?lang=en" class="small-box-footer"><?php echo $lang[50] ?> <i class="fa fa-arrow-circle-right"></i></a> </div>
  </div>
  <!-- ./col -->
  
  <div class="col-lg-3 col-xs-6" style="visibility:hidden">
    <div class="small-box bg-teal">
      <div class="inner">
        <h3></h3>
        <p></p>
      </div>
      <div class="icon"><i class="ion ion-person-add"></i></div>
    </div>
  </div>
</div>
<div class="row"   style="margin-top: 50px;">
  <div class="col-lg-3 col-xs-3 col-md-3"></div>
  <div class="col-lg-6 col-xs-6 col-md-6">
    <table style="width:100%" class="table">
      <thead>
        <tr class="HeaderColumnsName">
          <th>Date Time</th>
          <th>IP Address</th>
        </tr>
      </thead>
      <tbody>
        <?php 	$iLoginIpDetailsNew = mysqli_query($con, "SELECT * FROM login_log WHERE userid = '".$_SESSION['user_id']."'");
					  		while($iLogIp = mysqli_fetch_array($iLoginIpDetailsNew)) { ?>
        <tr>
          <td><?php echo $iLogIp['create_at']; ?></td>
          <td><?php echo $iLogIp['ip_address']; ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>
